# TODO

- [x] favicon
- [x] .htaccess
- [x] EN : ajouter les articles français avec (fr)
- [x] i18n: ajouter les terms manquant (voir thèmes)
- [x] documenter les classes pour les images dans le README.md
- [x] hugo deploy 
- [x] connecter deuxffleurs au nom de domaine www.vivienphilizot.fr
- [x] redirection permanante de vivienphilizot.fr et vivienphilizot.com vers www.vivienphilizot.fr
- [x] ajouter données structurées schema.org
	- [x] AR = article > ScholarlyArticle
	- [x] CH = chapitre d’ouvrage > ScholarlyArticle
	- [x] LI = livre > Book
- [x] activer les ligatures "liga" + "dlig"
