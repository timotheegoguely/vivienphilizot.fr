# TODO

- [x] revoir les scripts de génération des articles avec Vivien pour Hugo
- [x] favicon
- [x] .htaccess
- [x] EN : ajouter les articles français avec (fr)
- [x] i18n: ajouter les terms manquant (voir thèmes)
- [x] documenter les classes pour les images dans le README.md
- [x] hugo deploy 
- [ ] connecter le nom de domaine vivienphilizot.fr
