document.addEventListener("DOMContentLoaded", () => {
  const body = document.querySelector('body');
  const kind = body.dataset.kind;
  if (kind === "term") {
    const targetElement = document.querySelector(`#${location.pathname.split("/")[2]}`);
    targetElement.scrollIntoView({
      behavior: 'auto',
      block: 'center'
    });
  }
});
