---
title: "Legal information"
translationKey: "legal"
description: "Legal information about vivienphilizot.fr website"
template: "legal"
---

# Legal information

<small>In accordance with the provisions of Articles 6-III and 19 of Law No. 2004-575 of June 21, 2004 for Confidence in the Digital Economy, called L.C.E.N., we bring to the attention of users and visitors to the site vivienphilizot.fr the following information.</small>

## Publisher

The vivienphilizot.fr website is edited by Vivien Philizot, 9 rue Jacques Peirotes, Strasbourg, France.
<philizot@unistra.fr>

## Hosting

[Deuxfleurs](https://deuxfleurs.fr), association loi 1901 declared in the prefecture of Ille-et-Vilaine on January 29, 2020.

RNA number: W353020804
SIRET number: 89961256800019
Head office: RDC Appt. 1, 16 rue de la Convention, 59800 Lille, France

## Design & web development

Design: Vivien Philizot
Web development: [Timothée Goguely](https://timothee.goguely.com)
Generated with [Hugo](https://gohugo.io).
Font: [Immortel Infra G1](https://www.205.tf/immortel) Roman + _Italic_, designed by Clément Le Tulle-Neyret and published by 205TF.
