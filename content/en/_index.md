---
title: "Home"
description: "PhD. Teacher and researcher. <br>Graphic design and visual culture studies"
image: "/images/montagne.jpg"
---

Vivien Philizot is Senior Lecturer (HDR) at the [Faculty of Arts](https://arts.unistra.fr/), University of Strasbourg, where he is Head of the Design Department, and Co-Director of the [MA Degre in Graphic Design and Visual Culture](https://formations.unistra.fr/fr/formations/master-MAS/master-design-ME184/design-graphique-et-cultures-visuelles-PR372.html).

He has taught at the Visual Didactics Department of the [Haute école des arts du Rhin](https://www.hear.fr) in Strasbourg and at the [Haute école d’art et de design (HEAD)](https://www.hesge.ch/head/) in Geneva, and worked for several years at the graphic design studio [Poste 4](https://www.atelierposte4.com).

His teaching and research activities lie at the intersection of graphic design, visual cultures and epistemology. He is a member of the research team [Approches contemporaines de la création et de la réflexion artistique](https://accra-recherche.unistra.fr/) - UR 3402 - University of Strasbourg and of the research program [Visual Cultures](https://www.culturesvisuelles.org/), which he co-founded in 2018.

He is the director of the [“Milieux” collection](https://www.editions205.fr/collections/milieux) published by Deux-cent-cinq. He is the author of several books on images and graphic design, including <cite>Images premières. Aux origines de la représentation visuelle</cite>, (Metis Presses, Geneva, 2023. 275 p.), <cite>Qu’est-ce qu’une image dans l’espace public ?</cite> (Éditions Deux-cent-cinq, Lyon, 2022. 84 p.), <cite>Technique et design graphique. Outils, médias, savoirs</cite> (with Jérôme Saint-Loubert, Éditions B42, Paris, 2020. 288 p.).
