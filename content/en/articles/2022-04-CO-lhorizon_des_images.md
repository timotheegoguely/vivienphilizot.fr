---
title: "“The horizon of images”"
description: "Lecture at the Conference « Horizons contemporains : ouverture, partition, profondeur d’espaces dans les arts de la scène et de l’écran », organised by Aurélie Coulon and Benjamin Thomas, Université de Strasbourg, 28 juin 2022 "
author: "Vivien Philizot"
date: 2022-01-01
years: "2022"
places: "Strasbourg"
objects: "communication"
themes: ["Horizon", "images", "perception", "représentation", "remédiation", "critique de la représentation télévisuelle"]
persons: ["Aurélie Coulon", "Benjamin Thomas"]
---

# “The horizon of images”, Lecture at the Conference « Horizons contemporains : ouverture, partition, profondeur d’espaces dans les arts de la scène et de l’écran », organised by Aurélie Coulon and Benjamin Thomas, Université de Strasbourg, 28 juin 2022 

Peter Weir’s film <cite>The Truman Show</cite>, released in 1998, sets the scene for a dystopian horizon by offering a critique of the media representation that characterised several American film productions of the late 1990s. The end of the decade was also marked in the United States by the height of the debate on visual culture, which invited us to observe the horizon of these film and television screens in the light of the reflections on the image developed in the fields of Anglo-Saxon philosophy and visual studies. If television’s remediation of cinema implies a distinction between the image of the horizon and the horizon of images, the aim will be to put this distinction to work by attempting to identify what the horizon, as an object and theoretical tool, can contribute to our understanding of images.
