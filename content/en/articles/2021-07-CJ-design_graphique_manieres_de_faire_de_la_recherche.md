---
title: "Graphic Design. Ways of doing research."
description: "Conference, Centre Georges Pompidou, and Bibliothèque Kandinsky. Paris, 17th December 2021. Co-produced by Université of Strasbourg and Centre national des arts plastiques. Co-organized with Nolwenn Maudet (UR 3402) and Véronique Marrier."
author: "Nolwenn Maudet, Vivien Philizot, Véronique Marrier"
date: 2021-01-01
years: "2021"
places: ["Centre Georges Pompidou", "Bibliothèque Kandinsky", "Paris"]
objects: "colloque et journée d’études"
themes: ["design graphique", "recherche", "réflexivité"]
persons: ["Véronique Marrier", "Nolwenn Maudet", "Yann Aucompte", "Julie Blanc", "Max Bonhomme", "Corentin Brulé", "Léonore Conte", "Jérémy De Barros", "Sébastien Dégeilh", "Catherine de Smet", "Pierre Fournier", "Catherine Geel", "Thomas Huot-Marchand", "Olivier Huz", "Annick Lantenois", "Vincent Maillard", "Nolwenn Maudet", "Manon Ménard", "Éloïsa Pérez", "Vivien Philizot", "Lorène Picard", "Gilles Rouffineau", "Fabrice Sabatier"]
---

# Graphic Design. Ways of doing research, Conference, Centre Georges Pompidou, and Bibliothèque Kandinsky. Paris, 17th December 2021. Co-produced by Université of Strasbourg and Centre national des arts plastiques. Co-organized with Nolwenn Maudet (UR 3402) and Véronique Marrier.

For some years now, the field of graphic design has been characterised by a growing interest in research. This interest can be seen at several levels: at the level of teaching in art schools and universities (which is increasingly combining theory and projects), at the level of practice (which is becoming more reflexive and critical), and at the level of research itself (which is being enriched by an ever wider range of knowledge). The aim of this study day is to explore the existence and nature of specific modes of knowledge, or specific ways of doing research in graphic design. How have these developed in recent years, while maintaining points of intersection and openness to neighbouring disciplines? Organised by: Véronique Marrier, Nolwenn Maudet, Vivien Philizot, in collaboration with Mica Gherghescu and Victor Guégan. Speakers: Yann Aucompte, Julie Blanc, Max Bonhomme, Corentin Brulé, Léonore Conte, Jérémy De Barros, Sébastien Dégeilh, Catherine de Smet, Pierre Fournier, Catherine Geel, Thomas Huot-Marchand, Olivier Huz, Annick Lantenois, Vincent Maillard, Nolwenn Maudet, Manon Ménard, Éloïsa Pérez, Vivien Philizot, Lorène Picard, Gilles Rouffineau, Fabrice Sabatier. 

[Link to the presentation](https://www.culturesvisuelles.org/champs-de-recherche/epistemologie-des-images-et-du-design-graphique/design-graphique-manieres-de-faire-de-la-recherche)
