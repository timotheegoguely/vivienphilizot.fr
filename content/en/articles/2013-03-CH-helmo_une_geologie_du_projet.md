---
title: "“Helmo, a project geology”"
description: "in Thomas Couderc, Jochen Gerner, Vivien Philizot (dir.), Clément Vauchez (dir.). <cite>Helmo. Design graphique, ping-pong et géologie</cite>, Université de Strasbourg, 2013, (Cahiers Design ; 3), p.6-10. "
author: "Vivien Philizot"
date: 2013-01-01
years: "2013"
places: "Strasbourg"
objects: "chapitre d’ouvrage"
themes: ["Helmo", "contrainte", "identité visuelle", "affiche", "design graphique"]
persons: ["Thomas Couderc", "Jochen Gerner", "Clément Vauchez"]
---

# “Helmo, a project geology”, in Thomas Couderc, Jochen Gerner, Vivien Philizot (dir.), Clément Vauchez (dir.). <cite>Helmo. Design graphique, ping-pong et géologie</cite>, Université de Strasbourg, 2013, (Cahiers Design ; 3), p.6-10. 

This text is the introduction to the third issue of Cahiers design, which focuses on the work of the Helmo collective. While we often hear that design is an operation aimed at transforming constraints into opportunities, the graphic determinism that underlies Helmo’s work, borrowed from Georges Perec or Jorge Luis Borges, seems primarily aimed at reducing the arbitrariness of the creative act, motivated less by the subjectivity of its authors than by the necessity of the project’s life contexts.

[Download (French)](/pdf/009-PHILIZOT-2013-03-CH-helmo_une_geologie_du_projet.fr.sh-2013.pdf)
