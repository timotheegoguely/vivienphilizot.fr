---
title: "Exhibition “A Cartography of Research in Graphic Design”"
description: "Co-curated with Malte Martin. The Signe, Centre national du graphisme, Chaumont, du 13th May - 6th August 2017."
author: "Vivien Philizot, Malte Martin"
date: 2017-01-01
years: "2017"
places: ["Centre national du graphisme", "Chaumont", "Galerie NaMiMa", "Nancy"]
objects: "exposition"
themes: ["recherche en design graphique", "cartographie", "signes"]
persons: ["Malte Martin", "Philippe Riehling", "Michaël Mouyal", "Julienne Richard"]
---

# Exhibition “A Cartography of Research in Graphic Design”, Co-curated with Malte Martin. The Signe, Centre national du graphisme, Chaumont, du 13th May - 6th August 2017.

In recent years, there has been a growing interest in graphic design research, as measured by the topics and projects developed in art schools, the increasing number of publications, and the consolidation work done by various institutional actors. What might the landscape of graphic design research look like? This is the question that this exhibition, which can be read and thought of as a cartography, seeks to answer. This exhibition has been presented at The Signe, Centre national du graphisme, Chaumont, from 13 May to 6 August 2017. It has also been presented at the NaMiMa gallery at the École supérieure d‘art de Nancy, from 17 October to 30 November 2017.

<figure>
<img src="/images/038_Cartographie-recherche-design-graphique-0_1200x849.jpg" alt="" width="1200" height="849" loading="lazy">
<img src="/images/038_Cartographie-recherche-design-graphique-1_1200x800.jpg" alt="" width="1200" height="849" loading="lazy">
<img src="/images/038_Cartographie-recherche-design-graphique-2_1200x800.jpg" alt="" width="1200" height="849" loading="lazy">
<img src="/images/038_Cartographie-recherche-design-graphique-3_1200x800.jpg" alt="" width="1200" height="849" loading="lazy">
<figcaption>Exposition « Une cartographie de la recherche en design graphique », 2017</figcaption>
</figure>

[Download booklet](/pdf/038-PHILIZOT-A-cartography-of-research-2017.pdf)
