---
title: "Visual cultures digital platform"
description: "ACCRA - UR3402, University of Strasbourg."
author: "Vivien Philizot, Sophie Suma"
date: 2021-01-01
years: "2021"
places: ""
objects: "projet de recherche"
themes: ["cultures visuelles", "recherche"]
persons: ["Sophie Suma", "Nolwenn Maudet", "Simon Zara", "Sarah Calba", "Timothée Goguely", "Adrien Payet"]
---

# Visual cultures digital platform, ACCRA - UR3402, University of Strasbourg.

The Visual cultures digital platform is a digital space for the exchange, production and dissemination of research on knowledge of images and visual culture. The platform combines a collective scientific approach with reflections on form and visual and textual tools, drawing on contributions from digital humanities, graphic design and visual studies. The platform is conceived as a space for reflection and experimentation on new collaborative working tools and ways of disseminating and publishing research. It is conceived as a unique writing tool that allow several geographically distant researchers to collaborate on the same project and disseminate their content online. The development of this platform is also an opportunity to offer the public a didactic cultural object, offering anyone interested an exploratory experience to understand the role of images and visual cultures in our environment. Design: Sophie Suma, Nolwenn Maudet, Simon Zara, Sarah Calba. Development: Timothée Goguely and Adrien Payet.

<figure>
<img src="/images/069-Cultures-visuelles_1200x953.jpg" alt="" width="1200" height="953" loading="lazy">
</figure>

[Link to the website](https://www.culturesvisuelles.org/)
