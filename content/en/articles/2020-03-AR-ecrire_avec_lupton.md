---
title: "“Writing with Lupton”"
description: "(with Nolwenn Maudet) in <cite>Graphisme en France</cite> 2020, Centre national des Arts plastiques, 2020, p.28-31."
author: "Nolwenn Maudet, Vivien Philizot"
date: 2020-01-01
years: "2020"
places: ""
objects: "article"
themes: ["Ellen Lupton", "Michel Foucault", "design", "disciplines", "épistémologie du design", "Archéologie du savoir"]
persons: ["Nolwenn Maudet"]
---

# “Writing with Lupton”, (with Nolwenn Maudet) in <cite>Graphisme en France</cite> 2020, Centre national des Arts plastiques, 2020, p.28-31.

This text is an introduction to our French translation of “Disciplines of Design. Writing with Foucault”, a text published by researcher Ellen Lupton in 1996 in <cite>Design Writing Research</cite>. The author's device is both surprising and daring: in two columns, she scatters excerpts from Michel Foucault’s <cite>L’Archéologie du savoir</cite>, in which Lupton has taken care to replace the author’s references to madness and medicine with the term “design” and other related notions (visual production, newspapers, printers, etc.). The title and introductory text announce the aim: to examine the coherence of design as a discipline, borrowing Foucault’s “archaeological” method. 

<figure>
<img src="/images/057_Maudet-Philizot-Lupton_1200x864_a.jpg" alt="" width="1200" height=« 864" loading="lazy">
<img src="/images/057_Maudet-Philizot-Lupton_1200x864_b.jpg" alt="" width="1200" height=« 864" loading="lazy">
<img src="/images/057_Maudet-Philizot-Lupton_1200x864_c.jpg" alt="" width="1200" height=« 864" loading="lazy">
<img src="/images/057_Maudet-Philizot-Lupton_1200x864_d.jpg" alt="" width="1200" height=« 864" loading="lazy">
<figcaption>Nolwenn Maudet, Vivien Philizot, « Écrire avec Lupton », <cite>Graphisme en France</cite>, 2020</figcaption>
</figure>

[Link to the online version](https://www.cnap.fr/en/graphisme-en-france-issue-26)
[Download (French)](/pdf/057-PHILIZOT-2020-03-AR-ecrire_avec_lupton.fr.sh-2020.pdf)
