---
title: "“Forma, Media. Some remarks on the modes of existence of graphic design”"
description: "Lecture in opening of the Graphic Design Festival « Format(s) », Haute École des Arts du Rhin (HEAR), 6th October 2022"
author: "Vivien Philizot"
date: 2022-01-01
years: "2022"
places: ["Haute École des Arts du Rhin", "Strasbourg"]
objects: "conférence"
themes: ["Format", "design graphique", "médiation"]
persons: ""
---

# “Forma, Media. Some remarks on the modes of existence of graphic design”, Lecture in opening of the Graphic Design Festival « Format(s) », Haute École des Arts du Rhin (HEAR), 6th October 2022

The notion of form and its derivatives occupy a privileged place in the history and culture of design, and graphic design in particular. After all, graphic designers create forms or give shape to ideas. On a daily basis, they manipulate formats, whether files or paper, while experiencing the tension that is so often discussed between ’giving form’ and giving shape. ’Form’ is one of the commonplaces of graphic design. However, it does not always allow us to understand the way in which graphic objects function, which may be less like representations than mediations. We will look at this problem from the point of view of two fundamental concepts: forma and media.

<figure>
<img src="/images/077_John_Berger-Voir_le_voir_4e-1972_1200x1628.jpg" alt="" width="1200" height="1628" loading="lazy">
<figcaption>Seing John Berger’s <cite>Ways of Seeing</cite> (1972 - 2014 - 2022)</figcaption>
</figure>
