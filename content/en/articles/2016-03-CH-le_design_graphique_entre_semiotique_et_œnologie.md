---
title: "“Graphic design, between semiotics and oenology”"
description: "in Ruedi Baur, Pierre Litzler, Vivien Philizot (dir.), <cite>Langages visuels et systèmes complexes</cite>, Presses universitaires de l’Université de Strasbourg, 2016, p.44-49."
author: "Vivien Philizot"
date: 2016-01-01
years: "2016"
places: "Strasbourg"
objects: "chapitre d’ouvrage"
themes: ["objets graphiques", "iconoclasme", "histoire", "identités visuelles", "Frederick R. Barnard"]
persons: ""
---

# “Graphic design, between semiotics and oenology”, in Ruedi Baur, Pierre Litzler, Vivien Philizot (dir.), <cite>Langages visuels et systèmes complexes</cite>, Presses universitaires de l’Université de Strasbourg, 2016, p.44-49.

This article examines the reception of graphic objects, starting from the historical tradition of images. While an image may be 'worth a thousand words', as the phrase coined by the advertising executive Frederick R. Barnard in the 1920s teaches us, the polysemy of which it is suspected often relegates it to the shadow of text. Acknowledging the historical alternation between iconoclasm and fetishism, our aim here is to follow W.J.T. Mitchell in arguing for the reintroduction of a relationship to images under the aegis of the totem.

[Download (French)](/pdf/026-PHILIZOT-2016-03-CH-le_design_graphique_entre_semiotique_et_œnologie.fr.sh-2016.pdf)
