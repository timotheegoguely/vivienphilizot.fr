---
title: "“Translating ‘graphic design’. Reflections on the epistemology of a discipline”"
description: "in <cite>Appareil</cite>, [online], 24/2022. https://doi-org.scd-rproxy.u-strasbg.fr/10.4000/appareil.4380"
author: "Vivien Philizot"
date: 2022-01-01
years: "2022"
places: ""
objects: "article"
themes: ["design graphique", "sémiotique", "théorie des médias", "études visuelles", "épistémologie", "traduction"]
persons: ""
---

# “Translating ‘graphic design’. Reflections on the epistemology of a discipline”, in <cite>Appareil</cite>, [online], 24/2022. https://doi-org.scd-rproxy.u-strasbg.fr/10.4000/appareil.4380

In some respects, “graphic design” is an incomprehensible notion, covering a diffuse set of practices, situations, and heterogeneous objects, which seem to dissuade any attempt to circumscribe its ever-changing contours. In order to better grasp the tricky form that this expression draws at the surface of the world, this paper proposes to follow its multiple translations, as closely as possible. From which paradigms has, what we agree to call “graphic design” been constructed, considered, theorised under other names in recent history? At a time when one is searching everywhere without finding the scientific models of design and graphic design research, it is very useful to note that the objects that commonly fall into these categories have been apprehended for years by neighbouring fields of knowledge. By discussing three translations of “graphic design”, this paper aims to give some clarity to this set of things with such diffuse contours and to determine more precisely the scientific perspectives through which research in graphic design is possible today.

<figure>
<img src="/images/075-Georges-Nelson_1977_1200x684.jpg" alt="" width="1200" height=« 684" loading="lazy">
<figcaption>Georges Nelson, <cite>How to See</cite>, 1977</figcaption>
</figure>

[Lien vers la version en ligne](https://doi-org.scd-rproxy.u-strasbg.fr/10.4000/appareil.4380)
[Download (French)](/pdf/075-PHILIZOT-2022-03-AR-traduire_design_graphique_reflexions_sur_lepistemologie_dune_discipline.fr.sh-2022.pdf)
