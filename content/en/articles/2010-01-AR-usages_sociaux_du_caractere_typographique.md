---
title: "“Social uses of Typographic Character”"
description: "in <cite>Livraison</cite> – revue d’art contemporain, 13/2010, p.102-113."
author: "Vivien Philizot"
date: 2010-01-01
years: "2010"
places: ""
objects: "article"
themes: ["typographie", "usages", "lecture", "contexte social", "affiches de théâtre"]
persons: ""
---

# “Social uses of Typographic Character”, in <cite>Livraison</cite> – revue d’art contemporain, 13/2010, p.102-113.

To what extent does the reading situation and context influence what is read? This paper examines the visual form of typefaces by relating it to the different meanings it can have for the reader. The social uses of the typeface are closely linked to what the semiological tradition has called the ‘connotations’ of the letter. This paper examines the connotations from a pragmatic perspective, free from any form of essentialism.

[Download](/pdf/086-PHILIZOT-Social_Use_of_Typographic_Character-2010.pdf)
