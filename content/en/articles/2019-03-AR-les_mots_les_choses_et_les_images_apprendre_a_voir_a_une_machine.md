---
title: "“Words, things and images. Teaching a machine to see”"
description: "in <cite>Revue Radar</cite> [en ligne], 4/2019. https://revue-radar.fr/livraison/4/article/28/les-mots-les-choses-et-les-images-apprendre-a-voir-a-une-machine"
author: "Vivien Philizot"
date: 2019-01-01
years: "2019"
places: ""
objects: "article"
themes: ["intelligence artificielle", "vision", "réseaux de neurones", "images", "interaction humain-machine"]
persons: ""
---

# “Words, things and images. Teaching a machine to see”, in <cite>Revue Radar</cite> [en ligne], 4/2019. https://revue-radar.fr/livraison/4/article/28/les-mots-les-choses-et-les-images-apprendre-a-voir-a-une-machine

In recent years, artificial intelligence research has made spectacular advances in the field of vision. Deep neural networks now seem capable of seeing for us and making decisions based on their observations. However, the centuries-old human resistance to images and their interpretation does not seem to have been overcome. Teaching a machine to see and draw requires us to redefine what ”seeing”, as a process necessarily imbued with knowledge, actually means. Irreducible to a specific field of knowledge, the problem of machine vision is fundamentally a problem of visual culture. In an attempt to understand what “seeing” might mean for a machine, this text questions the way in which deep neural networks learn to connect language, the world and thought, by examining the details from which machine vision seems to be built.

[Download (French)](/pdf/048-PHILIZOT-2019-03-AR-les_mots_les_choses_et_les_images_apprendre_a_voir_a_une_machine.fr.sh-2019.pdf)
