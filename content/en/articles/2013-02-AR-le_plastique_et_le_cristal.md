---
title: "“Plastic and crystal. The visual identity of the Stedelijk Museum Amsterdam”"
description: "in <cite>Après/Avant</cite>, Revue annuelle de culture graphique, Rencontres de Lure, 1/2013, p.59-63."
author: "Vivien Philizot"
date: 2013-01-01
years: "2013"
places: ""
objects: "article"
themes: ["design graphique", "Stedelijk Museum Amsterdam", "identité visuelle", "transgression", "Armand Mevis", "Linda van Deursen"]
persons: ""
---

# “Plastic and crystal. The visual identity of the Stedelijk Museum Amsterdam”, in <cite>Après/Avant</cite>, Revue annuelle de culture graphique, Rencontres de Lure, 1/2013, p.59-63.

The history of the visual identity of the Stedelijk Museum in Amsterdam, initiated by Sandberg, graphic designer and director from 1945 to 1963, is certainly one of the richest a museum has ever known. This article examines the identity designed by Armand Mevis and Linda van Deursen in 2011. The seemingly childlike openness of this project shakes up notions of harmony, balance and proportion, and raises questions at the heart of the discipline.

<figure>
<img src="/images/008_Stedelijk-Museum_1000x1500.jpg" alt="" width="1000" height="1500" loading="lazy">
<figcaption>Stedelijk Museum Amsterdam, Armand Mevis, Linda Van Deursen, 2012</figcaption>
</figure>

[Link to the online version](https://www.delure.org/les-a-cotes/bibliotheque/revue-apres-avant/apres-avant-1)
[Download (French)](/pdf/008-PHILIZOT-2013-02-AR-le_plastique_et_le_cristal.fr.sh-2013.pdf)
