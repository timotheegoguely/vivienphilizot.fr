---
title: "“Kitsch, bad taste, Scheiße. An aesthetic of dissonance”"
description: "in <cite>Azimuts</cite>, École supérieure d’Art et Design de Saint-Étienne, 42-2015, p.109-136."
author: "Vivien Philizot"
date: 2015-01-01
years: "2015"
places: ""
objects: "article"
themes: ["kitsch", "vernaculaire", "camp", "design graphique", "détournement", "citation", "dissonance"]
persons: ""
---

# “Kitsch, bad taste, Scheiße. An aesthetic of dissonance”, in <cite>Azimuts</cite>, École supérieure d’Art et Design de Saint-Étienne, 42/2015, p.109-136.

Since at least the end of the twentieth century, the fields of design, graphics and architecture seem to have been affected by a form of aesthetics of deception, which, by literally repeating the opposition between the pure and the impure, High and Low, returns us to a unique and vertical conception of values that remains controversial. The aim here is to re-examine these practices in the light of a musical metaphor that can help us to understand the way in which "camp", vernacular and kitsch operate within design. A critical moment in a system in equilibrium, dissonance comes from deviation, disagreement and a certain logic of distancing that can only be relative.

<figure>
<img src="/images/019_Bambi_560x500.jpg" alt="" width="560" height="500" loading="lazy">
</figure>

[Link to the online version](https://revue-azimuts.fr/numeros/42/kitsch-bad-taste-scheisse-une-esthetique-de-la-dissonance)
[Download](/pdf/019-PHILIZOT-Aesthetics_of_Dissonance_Azimuts-2015.pdf)
