---
title: "“Design forms and the subjective construction of the social world”"
description: "in <cite>Figures de l’Art</cite>, Presses universitaires de Pau, 25/2013, p.365-381."
author: "Vivien Philizot"
date: 2013-01-01
years: "2013"
places: ""
objects: "article"
themes: ["design", "expérience", "pratique", "usages", "langage"]
persons: ""
---

# “Design forms and the subjective construction of the social world”, in <cite>Figures de l’Art</cite>, Presses universitaires de Pau, 25/2013, p.365-381.

Based on a back and forth between project and experience, design is a fundamentally contextual activity and as such it seems to share certain characteristics with language. It is therefore possible to make connections between the linguistic environment and our relationship to the world, insofar as this relationship is mediated not by the objects themselves, but by the practice of the objects that design is responsible for bringing into existence. For it is this notion of practice, in its linguistic and anthropological sense, that seems to determine the object’s ability to make sense, and thus its very membership of that category of ‘design object’ that is so difficult to circumscribe.

[Lien vers la version en ligne](https://www.persee.fr/doc/fdart_1265-0692_2013_num_25_1_1616)
[Download (French)](/pdf/007-PHILIZOT-2013-01-AR-formes_du_design_et_construction_subjective_du_monde_social.fr.sh-2013.pdf)
