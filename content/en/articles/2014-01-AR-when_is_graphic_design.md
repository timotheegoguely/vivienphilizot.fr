---
title: "“When is Graphic design? Some nominalist remarks on the definition of a discipline”"
description: "in <cite>Tombolo</cite>, [online] 2014. /www.t-o-m-b-o-l-o.eu/meta/when-is-grahic-design-quelques-remarques-nominalistes-sur-la-definition-dune-discipline-2/ reprint in Collectif, <cite>Graphisme et transmission</cite>, Alliance graphique internationale, 2015, p.28-31."
author: "Vivien Philizot"
date: 2014-01-01
years: "2014"
places: ""
objects: "article"
themes: ["design graphique", "définitions", "pragmatisme", "Nelson Goodman", "contexte"]
persons: ""
---

# “When is Graphic design? Some nominalist remarks on the definition of a discipline”, in <cite>Tombolo</cite>, [online] 2014. /www.t-o-m-b-o-l-o.eu/meta/when-is-grahic-design-quelques-remarques-nominalistes-sur-la-definition-dune-discipline-2/ reprint in Collectif, <cite>Graphisme et transmission</cite>, Alliance graphique internationale, 2015, p.28-31.

The definition of graphic design as an autonomous field of practice has been the subject of much debate since 2010, when the Festival international de graphisme de Chaumont proposed the question “Le graphisme qu’est-ce que c’est?” (“Graphic design, what is it?”) as the subject of its annual competition. The aim here is to study, on behalf of graphic design, the consequences of the paradigm shift that occurred in the art world in the 20<sup>th</sup> century, and which Nelson Goodman conceptualized in a much-publicized article, by proposing to replace “What is art?” with the question “When is art?”

[Download (French)](/pdf/011-PHILIZOT-2014-01-AR-when_is_graphic_design.fr.sh-2014.pdf)
