---
title: "<cite>Images premières. At the origins of visual representation</cite>"
description: "Mētis Presses, Genève, 2023. 275 p. ISBN : 9782940711222"
author: "Vivien Philizot"
date: 2023-01-01
years: "2023"
places: "Genève"
objects: "livre"
themes: ["images", "études visuelles", "origines", "représentation", "sciences", "épistémologie", "Ludwig Wittgenstein", "Nelson Goodman", "Ernst Gombrich", "Susan Sontag", "WJT Mitchell", "Emmanuel Alloa"]
persons: ""
---

# <cite>Images premières. At the origins of visual representation</cite>, Mētis Presses, Genève, 2023. 275 p. ISBN : 9782940711222

In contrast to the perfection of images promised by today’s technological overkill, these ‘First images’ are ‘just images’ and derive their fascination from their ability to place us at the very limits and origins of visual representation. This essay looks at the original sites of image production, such as the subterranean walls of the Paleolithic, Plato's cave, and the crumbling walls described by Leonardo da Vinci, before turning to other accounts of beginnings, other inaugural images, and then to other stories of beginnings, other inaugural images, such as the first photograph, the first monochromes, the first images produced by artificial neural networks, or certain scientific visualisations, all of which offer opportunities to reinvent the notion of ‘image’ itself, which is as familiar as it is elusive. The book engages in a rich dialogue with the philosophy of Ludwig Wittgenstein and Nelson Goodman, while also questioning the notions of representation developed by Ernst Gombrich, Susan Sontag and, more recently, WJT Mitchell and Emmanuel Alloa.

<figure>
<img src="/images/081_Philizot-Images-premieres_1045x1200_a.jpg" alt="" width="1045" height="1200" loading="lazy">
<img src="/images/081_Philizot-Images-premieres_1024x621_b.jpg" alt="" width="1024" height="621" loading="lazy">
<img src="/images/081_Philizot-Images-premieres_1024x621_c.jpg" alt="" width="1024" height="621" loading="lazy">
<img src="/images/081_Philizot-Images-premieres_1024x621_d.jpg" alt="" width="1024" height="621" loading="lazy">
<figcaption>Vivien Philizot, <cite>Images premières. Aux origines de la représentation visuelle</cite></figcaption>
</figure>

[Link to the publisher’s website](https://www.metispresses.ch/fr/images-premieres)
