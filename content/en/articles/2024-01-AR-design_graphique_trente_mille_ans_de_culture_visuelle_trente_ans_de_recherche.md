---
title: "“Graphic design. Thirty thousand years of visual culture, thirty years of research”"
description: "<cite>Graphisme en France</cite> 2024, Centre national des Arts plastiques, 2024, p.39-61."
author: "Vivien Philizot"
date: 2024-01-01
years: "2024"
places: ""
objects: "article"
themes: ["design graphique", "recherche", "épistémologie", "histoire"]
persons: ""
---

# “Graphic design. Thirty thousand years of visual culture, thirty years of research”, <cite>Graphisme en France</cite> 2024, Centre national des Arts plastiques, 2024, p.39-61.

Since the end of the twentieth century, graphic design has seen the emergence of a research activity that has developed in a variety of forms and contexts: in the teaching programmes of art schools and universities, in projects initiated by public and private institutions, and in the practice of graphic designers themselves, when combined with theoretical, historical and critical knowledge. Over the last thirty years, research has profoundly changed the landscape of graphic design in France, while at the same time helping to question its identity, its history and its relationship with many fields of humanities research (art history, media studies, semiotics and visual culture studies, etc.). This article looks back at this recent development and at the challenges facing graphic design research in the coming years.

<figure class="max-h-screen align-left">
<img src="/images/083_Muller-Brockmann_1200x929.jpg" alt="" width="1200" height="929" loading="lazy">
<img src="/images/083_Yann-Aucompte-these_1200x853.jpg" alt="" width="1200" height="853" loading="lazy">
<figcaption><cite>A history of visual communication. Geschichte der visuellen Kommunikation. Histoire de la communication visuelle.</cite> (Niederteufen : Niggli, 1971) ; Yann Aucompte. « La diversité éthique des pratiques de graphisme en France au tournant du XXI<sup>e</sup> siècle » (These de doctorat, Paris 8, 2022).</figcaption>
</figure>

[Link to the online version](https://www.cnap.fr/en/publishing/graphisme-en-france-issue-30)
[Download](/pdf/083-PHILIZOT-XXXXXXXXXXX-2024.pdf)
