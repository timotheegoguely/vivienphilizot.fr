---
title: "“Graphic Design and Metamorphoses of the Spectacle A few footnotes to <cite>Ten Footnotes to a Manifesto</cite>”"
description: "in <cite>Graphisme en France</cite> 2014, Centre national des Arts plastiques, 2014, p.27-45. "
author: "Vivien Philizot"
date: 2014-01-01
years: "2014"
places: ""
objects: "article"
themes: ["manifeste", "design graphique", "politique", "graphisme commercial", "valeur", "capital", "graphisme dissensuel", "Ken Garland", "Michael Bierut"]
persons: ""
---

# “Graphic Design and Metamorphoses of the Spectacle A few footnotes to <cite>Ten Footnotes to a Manifesto</cite>”, in <cite>Graphisme en France</cite> 2014, Centre national des Arts plastiques, 2014, p.27-45. 

In 1964, the manifesto <cite>First Things First</cite> was published in England, denouncing the excesses of a nascent profession - graphic design - at the service of the Western capitalist way of life and the market economy. Punctuated by several other manifestos and counter-proposals, the ensuing history invites us to question the legitimacy of the distinction between “cultural” and “commercial”, the “division of labour of meaning” as the modus operandi of design since the Bauhaus, the metamorphoses of advertising processes and the extension of the field of design into the realm of existence (prophesied by László Moholy-Nagy) proper to postmodernism.

<figure>
<img src="/images/013_Ten-Footnotes_M_Bierut_1200x1078.jpg" alt="" width="1200" height="1078" loading="lazy">
<figcaption>Bierut, Michael. « Ten Footnotes to a Manifesto ». In <cite>Seventy-Nine Short Essays on design</cite>, Michael Bierut, 52‑60. New York: Princeton Architectural Press, 2007.</figcaption>
</figure>

[Link to the online version](https://www.cnap.fr/en/node/74828)
[Download](/pdf/013-PHILIZOT-Graphic_Design_and_Metamorphoses_of_the_Spectacle-2014.pdf)
