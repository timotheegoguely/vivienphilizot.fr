---
title: "Ecology of “graphic” media"
description: "Lectures. Master 2 Design, Public Environment Design course, Faculty of Arts, University of Strasbourg, from 2024"
author: "Vivien Philizot"
date: 2024-01-01
years: "2024"
places: "Université de Strasbourg"
objects: "cours"
themes: ["design graphique", "médias", "textes et images", "culture visuelle"]
persons: ""
---

# Ecology of “graphic” media, Lectures. Master 2 Design, Public Environment Design course, Faculty of Arts, University of Strasbourg, from 2024

This course lays the methodological and epistemological foundations for the study of graphic objects as images, text-image combinations and, more broadly, as media. It aims to build the epistemological culture of graphic design students, not around a science of design, but around a set of interdisciplinary approaches that converge towards an ecological approach to graphic media. The aim is to provide keys to understanding the different theoretical systems or paradigms that have developed since the beginning of the 20<sup>th</sup> century in disciplinary fields concerned with images and media: semiotics, art history, media theory, visual studies, media ecology and archaeology, iconology, etc.
