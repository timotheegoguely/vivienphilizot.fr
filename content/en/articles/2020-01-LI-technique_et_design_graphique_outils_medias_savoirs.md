---
title: "<cite>Technique and graphic design. Tools, media, knowledge</cite>"
description: "Éditions B42, Paris, 2020. 288p. ISBN : 9782490077212. Co-direction Jérôme Saint-Loubert-Bié"
author: "Vivien Philizot et Jérôme Saint-Loubert Bié (dir.)"
date: 2020-01-01
years: "2020"
places: ""
objects: "livre"
themes: ["design graphique", "technique", "typographie", "impression", "objets", "médias"]
persons: ["Jérôme Saint-Loubert-Bié", "Bruno Bachimont", "Max Bonhomme", "Loup Cellard", "Kevin Donnot", "Pierre-Damien Huyghe", "Indra Kupferschmid", "Jean-Noël Lafargue", "Anthony Masure", "Vivien Philizot", "Fabrice Sabatier", "Nicolas Thély"]
---

# <cite>Technique and graphic design. Tools, media, knowledge</cite>, Éditions B42, Paris, 2020. 288p. ISBN : 9782490077212. Co-direction Jérôme Saint-Loubert-Bié

Our visual environment is permeated by technology. Yet these operations are often overshadowed by more noble questions about graphic designers, their approaches and the forms they produce. The various contributions in this book demonstrate, in a number of complementary ways, that technology is not reducible to quantified operations or functional objects, but that it has a much older and deeper anthropological dimension than our technological environments would have us imagine. This book is a testament to the multiplicity of possible approaches to the subject, as well as to the fertility of a subject that has yet to be fully explored at the intersection of design, visual studies and the digital humanities. With contributions from Bruno Bachimont, Max Bonhomme, Loup Cellard, Kevin Donnot, Pierre-Damien Huyghe, Indra Kupferschmid, Jean-Noël Lafargue, Anthony Masure, Vivien Philizot, Fabrice Sabatier, Nicolas Thély.

<figure class="max-h-screen align-left">
<img src="/images/054_Technique-et-design-graphique_960x640.jpg" alt="" width="960" height="640" loading="lazy">
<figcaption>Vivien Philizot, Jérôme Saint-Loubert Bié (dir.), <cite>Technique et design graphique</cite>, Éditions B42, 2020</figcaption>
</figure>
