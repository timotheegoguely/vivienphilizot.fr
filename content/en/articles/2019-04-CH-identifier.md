---
title: "“Identify”"
description: "in <cite>Le Ludographe, connaître et pratiquer le design graphique à l’école élémentaire</cite>, Collection Série graphique, Centre national des arts plastiques, Paris, 2019, p.10-15."
author: "Vivien Philizot"
date: 2019-01-01
years: "2019"
places: "Paris"
objects: "chapitre d’ouvrage"
themes: ["identité visuelle", "mots et images", "logotypes", "langages graphiques", "systèmes visuels"]
persons: ""
---

# “Identify”, in <cite>Le Ludographe, connaître et pratiquer le design graphique à l’école élémentaire</cite>, Collection Série graphique, Centre national des arts plastiques, Paris, 2019, p.10-15.

When it comes to identification, graphic design makes a series of contextual choices, using the visual tools of word, image and word-image. This text presents graphic design from this perspective, through a series of examples at different scales. From pictograms to corporate identities, our entire universe of signs operates according to the regime of identification.

[Download (French)](/pdf/049-PHILIZOT-2019-04-CH-identifier.fr.sh-2019.pdf)
