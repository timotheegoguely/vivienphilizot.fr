---
title: "Collection « Milieux »"
description: "Éditions Deux cent cinq. Direction of the collection."
author: "Vivien Philizot"
date: 2021-01-01
years: "2021"
places: "Lyon"
objects: "collection de livres"
themes: ["médias", "design graphique", "études visuelles", "théorie des médias"]
persons: ["Éloïsa Pérez", "Sophie Suma", "Vanina Pinter", "Jean-Marc Besse", "André Gunthert", "Florence Roller", "Damien Gautier", "Lize Braat"]
---

# Collection « Milieux », Éditions Deux cent cinq. Direction of the collection.

The Milieux collection explores and surveys our contemporary visual culture. It examines the materiality and visuality of our social practices as they are mediated, embraced and displaced in many ways by visual and textual forms. These forms, which occupy our immediate environment, constitute real environments to be read and seen, understood and interpreted.

<figure>
<img src="/images/063_Vivien-Philizot_collection-Milieux_1200x1200.jpg" alt="" width="1200" height="1200" loading="lazy">
<figcaption>Collection Milieux, Éditions Deux-cent-cinq</figcaption>
</figure>

[Link to the publisher’s website](https://www.editions205.fr/collections/milieux)
