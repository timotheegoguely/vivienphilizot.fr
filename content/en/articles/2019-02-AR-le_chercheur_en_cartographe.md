---
title: "“The researcher as cartographer. What relations between science and graphic design?”"
description: "in <cite>Étapes</cite>, 252/2019, p.76-79."
author: "Vivien Philizot"
date: 2019-01-01
years: "2019"
places: ""
objects: "article"
themes: ["design graphique", "théorie", "recherche", "sciences"]
persons: ""
---

# “The researcher as cartographer. What relations between science and graphic design?”, in <cite>Étapes</cite>, 252/2019, p.76-79.

To do science is to draw maps. To circumscribe a specific territory, to locate discontinuities in the apparent fabric of things, to delineate regions, provinces and their boundaries, to make connections or ruptures in the texture of the world. Can this familiar metaphor tell us something about the scientific side of graphic design, the reflexive side of the practice that goes by the various names of research, theory and science? What are the specific characteristics of the graphic design research we hear so much about? What approaches, methods and concepts are likely to be mobilised in such research? Or, to put it another way, if graphic design is an identifiable territory, what maps can be drawn to capture it? 

[Download (French)](/pdf/047-PHILIZOT-2019-02-AR-le_chercheur_en_cartographe.fr.sh-2019.pdf)
