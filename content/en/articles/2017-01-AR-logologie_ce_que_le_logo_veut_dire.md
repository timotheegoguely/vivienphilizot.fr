---
title: "“Logology. What a logo means”"
description: "in <cite>Graphisme en France</cite> 2017, Centre national des Arts plastiques, 2017, p.15-23."
author: "Vivien Philizot"
date: 2017-01-01
years: "2017"
places: "Paris"
objects: "article"
themes: ["logotype", "identités visuelles", "discours", "langages graphiques", "idéologie", "Paul Rand"]
persons: ""
---

# “Logology. What a logo means”, in <cite>Graphisme en France</cite> 2017, Centre national des Arts plastiques, 2017, p.15-23.

The logotype is an object that derives its specific functions from its visual properties, from its fundamentally iconic nature. The place of these signs in our history, their role in our economy and culture, and their political function invite us to unravel the two threads of discourse and image, <cite>logos</cite> and <cite>tupos</cite>, to understand how their entanglement has gradually woven the forms we know them as. What role do our visual systems actually play in our immediate environment? How do we use them in our everyday lives? How do they contribute to the social and cultural construction of our visual environment? Logology, literally “the science of discourse”, ironically doubling its Greek root, can also be understood as a science of the logotype, at least if we remember that the entirely visual way in which the latter presents itself to us is fundamentally doubled, countered or reinforced, as the case may be, by the logic of discourse itself. In the double sense of logotype and discourse, what could <cite>logos</cite> possibly mean?

<figure>
<img src="/images/030_Vivien_Philizot_Logologie-1200x773.jpg" alt="" width="1200" height="773" loading="lazy">
<figcaption>Vivien Philizot, « Logologie. Ce que le logo veut dire » - « Logology. What a logo means »</figcaption>
</figure>

[Link to the online version](https://www.cnap.fr/en/graphisme-en-france-issue-23)
[Download](/pdf/030-PHILIZOT-Logology-2017.pdf)
