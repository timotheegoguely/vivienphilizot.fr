---
title: "“Kodak’s Colorama and the Construction of the Gaze in Public Space”"
description: "in <cite>Articulo</cite>, Journal of Urban Research [en ligne], 19/2019. https://journals.openedition.org/articulo/3975"
author: "Vivien Philizot"
date: 2019-01-01
years: "2019"
places: "New York"
objects: "article"
themes: ["images", "études visuelles", "photographie", "paysage urbain", "représentation", "dispositif", "selfie", "idéologie", "Colorama", "gare de New York"]
persons: ""
---

# “Kodak’s Colorama and the Construction of the Gaze in Public Space”, in <cite>Articulo</cite>, Journal of Urban Research [en ligne], 19/2019. https://journals.openedition.org/articulo/3975

Between the 1950s and 1989, Kodak continuously occupied a surface of approximately one hundred square metres in New York’s Grand Central Station with a display of panoramic photographs that were changed regularly. These giant “Coloramas” presented photographed scenes that were taken specifically for the project, with the hundreds of images displayed during this period all showing an idealised representation of the American family. These photographs would, however, have remained ordinary had their defining feature not been the placement of the photographer within the frame of the image. By connecting photography to the point of view by which it is constructed, these panoramas seem to prescribe to their public a certain relation to the images, and to initiate a reflexive process which seems to have now become general. The huge area covered by the Coloramas has today given way to the more discreet surface of our smartphones, laptops and other digital tablets, which govern our various visual practices and reorganise the notions of image, point of view and reflexivity in new ways. Between the Coloramas of the 1960s and today’s touch screens, the question arises as to whether the multiplication and spread of visual surface has changed something in the way that image – and thus the gaze – is built in public space.

<figure>
<img src="/images/046_Colorama_New-York-Central-Station.jpg" alt="" width="1200" height="783" loading="lazy">
<figcaption>Colorama, New York Central Station, non datée © George Eastman House</figcaption>
</figure>

[Download](/pdf/046-PHILIZOT-kodaks_colorama_and_the_construction_of_the_gaze_in_public_space-2019.pdf)
