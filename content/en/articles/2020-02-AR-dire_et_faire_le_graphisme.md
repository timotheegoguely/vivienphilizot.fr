---
title: "“Saying and doing graphic design. Where we learn that no one agrees on the words of graphic design and that terminological distinctions have consequences for practice”"
description: "(with Nolwenn Maudet) in <cite>Graphisme en France</cite> 2020, Centre national des Arts plastiques, 2020, p.38-54."
author: "Nolwenn Maudet, Vivien Philizot"
date: 2020-01-01
years: "2020"
places: ""
objects: "article"
themes: ["design graphique", "texte et image", "histoire", "métiers", "disciplines"]
persons: ["Nolwenn Maudet"]
---

# “Saying and doing graphic design. Where we learn that no one agrees on the words of graphic design and that terminological distinctions have consequences for practice”, (with Nolwenn Maudet) in <cite>Graphisme en France</cite> 2020, Centre national des Arts plastiques, 2020, p.38-54.

"Graphic design", "Graphisme". In the form of a dialogue, this text explores a delicate and much debated terminological distinction. How have these terms gradually come to be used in France, and what different realities do they cover in contexts as specific as art schools, public institutions and companies, or in the ways in which graphic designers themselves present themselves? How have these terms been used by professions with their own histories? How have they been given different meanings to describe the variety of relationships to commissions that we observe in practice? How do terms whose meanings are systematically shifted and redefined enable the public to understand or misunderstand what they designate as an activity, a field of practice, a profession? 

<figure>
<img src="/images/055_Revue-BAT-N107_1000x1329.jpg" alt="" width="1000" height="1329" loading="lazy">
<figcaption>Revue BAT numéro 107</figcaption>
</figure>

[Link to the online version](https://www.cnap.fr/en/graphisme-en-france-issue-26)
[Download](/pdf/055-PHILIZOT-XXXXXXXXXXX-2020.pdf)
