---
title: "“Visualization in Humanities and Social Sciences: Epistemic perspectives”"
description: "Introductory speaker at the 11<sup>th</sup> Japanese French Frontiers of Science Symposium (JFFoS2024), University of Strasbourg, May 24-27, 2024 (CNRS - JSPS)"
author: "Vivien Philizot"
date: 2024-01-01
years: "2024"
places: "Strasbourg"
objects: "conférence"
themes: ["images", "visualization", "science studies", "information studies"]
persons: ""
---

# “Visualization in Humanities and Social Sciences: Epistemic perspectives”, Introductory speaker at the 11th Japanese French Frontiers of Science Symposium (JFFoS2024), University of Strasbourg, May 24-27, 2024 (CNRS - JSPS)

As the American historian of science Matthew Norton Wise puts it, « much of the history of science could be written in terms of making new things visible – or familiar things visible in a new way. » (Wise 2006, 75). If much of science is about making the invisible visible, it is because there is a close connection between knowledge and the forms in which it is expressed. Such connections have been the subject of much research over the past forty years in the fields of laboratory studies and anthropological approaches to laboratory work (Latour and Woolgar 1979; Woolgar 1982; Lynch 1982), science studies (Lynch 1985, Hacking 1985), media studies (Bredekamp, Dünkel, and Schneider 2015) and visualization and information studies (Tilling 1975; Tufte 1983, Friendly et al. 2006). These contributions attest to a growing interest in visualization in science and in the materiality of the visual representations that mediate knowledge. This lecture introduces the epistemological issues raised by visualization as they arise at the intersection of knowledge and materiality. It will also highlight the contribution of the humanities to practices of visualization, using an approach drawn from visual culture studies, science studies, and information studies.

<figure>
<img src="/images/084_Francis_Galton_Weather_Map_1861_1200x1672.jpg" alt="" width="1200" height="1672" loading="lazy">
<figcaption>Francis Galton, Weather Map, 1861</figcaption>
</figure>
