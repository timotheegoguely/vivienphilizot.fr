---
title: "Teaching graphic design at university. How does research change pedagogy?"
description: "Conference, Maison Interuniverstaire des Sciences de l'Homme – Alsace (Misha).
Strasbourg, 23-24 January 2024"
author: "Vivien Philizot"
date: 2024-01-01
years: "2024"
places: "Université de Strasbourg"
objects: "colloque et journée d’études"
themes: ["design graphique", "recherche", "enseignement", "pédagogie"]
persons: ["Yann Aucompte", "Ruedi Baur", "Karen Brunel", "Éloïse Cariou", "Boris du Boullay", "Catherine de Smet", "Damien Laverdunt", "Ron Fillon-Mallette", "Manon Ménard", "Éloïsa Pérez", "Christina Poth", "Magali Roumy Akue", "Silvia Sfligiotti", "Alice Twemlow", "Stéphane Vial", "Alix Gesnel", "Julia Coffre", "Marie Secher", "Louise Wambergue"]
---

# Teaching graphic design at university. How does research change pedagogy?, Conference, Maison Interuniverstaire des Sciences de l'Homme – Alsace (Misha).
Strasbourg, 23-24 January 2024

What does a research-based approach mean for a graphic design course? The aim of this study day is to extend the questions raised by the exhibition 'Une cartographie de la recherche en design graphique', presented at the Signe, centre national du graphisme, in 2017, and the study day 'Design graphique, manières de faire de la recherche', organised at the Centre Pompidou (CNAP, Université de Strasbourg, Bibliothèque Kandinsky) in 2021, to the field of teaching. The aim is to examine the ways in which research changes and transforms the teaching and pedagogy of graphic design along three main lines: What skills and knowledge can research, history and theory bring to graphic design courses? What theoretical frameworks and disciplinary cross-fertilisation are needed for university courses in graphic design? How can graphic design education, through the prism of history and theory, encourage a critical view of the visual world?

<figure>
<img src="/images/085-JE-Enseigner-le-design-graphique_1200x1697.jpg" alt="" width="1200" height="1697" loading="lazy">
<figcaption>Journée d’études « Enseigner le design graphique à l’université. Qu’est-ce que la recherche fait à la pédagogie ? », 2024</figcaption>
</figure>

[Link](https://www.culturesvisuelles.org/champs-de-recherche/epistemologie-des-images-et-du-design-graphique/enseigner-le-design-graphique-a-l-universite)
[Download programm (french)](/pdf/085-PHILIZOT-2024-03-CJ-enseigner_le_design_graphique_a_luniversite.fr.sh-2024.pdf)
[Dowload Call for participation](/pdf/085-PHILIZOT-Teaching-Graphic-Design-Call.pdf)
