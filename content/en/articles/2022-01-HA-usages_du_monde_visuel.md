---
title: "“Usages du monde visuel”"
description: "Habilitation to supervise rechearch. Catherine de Smet. Defense at Université Paris VIII Vincennes - Saint-Denis, 4th February 2022. Jury : Catherine Chomarat-Ruiz, Yves Citton, Catherine de Smet, Nathalie Delbard, Valérie Devillard"
author: "Vivien Philizot"
date: 2022-01-01
years: "2022"
places: ["Université Paris VIII", "Vincennes Saint-Denis"]
objects: "habilitation"
themes: ["design graphique", "épistémologie", "images", "études visuelles"]
persons: ["Catherine Chomarat-Ruiz", "Yves Citton", "Catherine de Smet", "Nathalie Delbard", "Valérie Devillard"]
---

# “Usages du monde visuel”, Habilitation to supervise rechearch.</cite> Catherine de Smet. Defense at Université Paris VIII Vincennes - Saint-Denis, 4th February 2022. Jury : Catherine Chomarat-Ruiz, Yves Citton, Catherine de Smet, Nathalie Delbard, Valérie Devillard

This habilitation to supervise research consists of three volumes: a summary dissertation (138 p.), an unpublished work entitled <cite>Images premières. Aux origines de la représentation visuelle</cite> (266 p.), and a collection of essays (522 p.). At the crossroads of graphic design and visual culture studies, this research focuses on graphic objects from the perspective opened up by media studies and, beyond that, by works on images which, from Louis Marin to Emmanuel Alloa, have sought to highlight the paradox of visual mediation in its various forms. This habilitation proposes new ways of looking at the study of images and contemporary visual culture, to enrich our understanding of graphic practices by going beyond the polysemic horizon of design, to engage in a broader reflection on the visual world.

<figure>
<img src="/images/073_Philizot-Habilitation_1200x1200_a.jpg" alt="" width="1200" height="1200" loading="lazy">
<img src="/images/073_Philizot-Habilitation_1200x1200_b.jpg" alt="" width="1200" height="1200" loading="lazy">
<img src="/images/073_Philizot-Habilitation_1200x1200_c.jpg" alt="" width="1200" height="1200" loading="lazy">
<img src="/images/073_Philizot-Habilitation_1200x1200_d.jpg" alt="" width="1200" height="1200" loading="lazy">
<img src="/images/073_Philizot-Habilitation_1200x1200_e.jpg" alt="" width="1200" height="1200" loading="lazy">
<img src="/images/073_Philizot-Habilitation_1200x1200_f.jpg" alt="" width="1200" height="1200" loading="lazy">
<img src="/images/073_Philizot-Habilitation_1200x1200_g.jpg" alt="" width="1200" height="1200" loading="lazy">
<figcaption>Vivien Philizot, Habilitation à diriger des recherches, 2022</figcaption>
</figure>

