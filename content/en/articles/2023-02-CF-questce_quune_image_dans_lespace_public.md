---
title: "“What is an image in the public space?”"
description: "Lecture as part of the ‘Revoir le voir: comment créer, lire et consommer l'image aujourd'hui’ day, organised by Stéphanie Gygax, Anette Lenz, Rob van Leijsen, Haute école d'art et de design de Genève (HEAD), 30 January 2023"
author: "Vivien Philizot"
date: 2023-01-01
years: "2023"
places: ["Haute école d’art et de design de Genève", "Genève"]
objects: "conférence"
themes: ["images", "espace public", "graphisme d’utilité publique "]
persons: ["Stéphanie Gygax", "Anette Lenz", "Rob van Leijsen"]
---

# “What is an image in the public space?”, Lecture as part of the ‘Revoir le voir: comment créer, lire et consommer l'image aujourd'hui’ day, organised by Stéphanie Gygax, Anette Lenz, Rob van Leijsen, Haute école d'art et de design de Genève (HEAD), 30 January 2023

The main question guiding the reflections developed in this conference is located precisely at the intersection of these two classic definitions of public space (Thierry Paquot): in the singular, public space is the place of political debate, the space of democracy; in the plural, it is the physical and symbolic spaces in and through which the public circulates and communicates. It goes hand in hand with the hypothesis that at this intersection are images, which play a leading role in the social practices and visual situations that make up our daily lives.

<figure>
<img src="/images/082_They-live-carpenter_1200x1789.jpg" alt="" width="1200" height="1789" loading="lazy">
<figcaption>John Carpenter, <cite>They Live</cite>, 1988</figcaption>
</figure>
