---
title: "<cite>What is an image in the public space?</cite>"
description: "Éditions Deux-cent-cinq, Lyon, 2022. 84 p. ISBN : 9782919380626"
author: "Vivien Philizot"
date: 2022-01-01
years: "2022"
places: "Lyon"
objects: "livre"
themes: ["design graphique", "espace public", "affiche"]
persons: ""
---

# <cite>What is an image in the public space?</cite>, Éditions Deux-cent-cinq, Lyon, 2022. 84 p. ISBN : 9782919380626

As Thierry Paquot points out, the conceptualisation of public space in the twentieth century has revealed the richness and diversity of this polysemous term: in the singular, public space is the place of political debate, the space of democracy; in the plural, it is the physical and symbolic spaces in and through which the public circulates and communicates. The main question that guides the reflections in this book lies precisely at the intersection of these two definitions. It is accompanied by the hypothesis that it is at this intersection that images play a leading role in the social practices and visual situations that make up our daily lives. A work of art in an urban space, an event covered with posters, an advertisement, a graphic or an institutional poster… What are the consequences for the public space and for the democratic debate of these visual forms that are publicly addressed to the eye?

<figure>
<img src="/images/074_Philizot_Image-dans-espace-public_06a.jpg" alt="" width="900" height="900" loading="lazy">
<img src="/images/074_Philizot_Image-dans-espace-public_06b.jpg" alt="" width="900" height="900" loading="lazy">
<img src="/images/074_Philizot_Image-dans-espace-public_06c.jpg" alt="" width="900" height="900" loading="lazy">
<img src="/images/074_Philizot_Image-dans-espace-public_06d.jpg" alt="" width="900" height="900" loading="lazy">
<img src="/images/074_Philizot_Image-dans-espace-public_06e.jpg" alt="" width="900" height="900" loading="lazy">
</figure>

[Link to the publisher’s website](https://www.editions205.fr/collections/milieux/products/qu-est-ce-qu-une-image-dans-l-espace-public)
