---
title: "Accueil"
description: "Enseignant-chercheur. <br>Design graphique et cultures visuelles"
image: "/images/montagne.jpg"
---

Vivien Philizot est maître de conférences HDR à la [Faculté des Arts](https://arts.unistra.fr/), Université de Strasbourg, où il est directeur du département Design et co-responsable du [Master Design graphique et cultures visuelles](https://formations.unistra.fr/fr/formations/master-MAS/master-design-ME184/design-graphique-et-cultures-visuelles-PR372.html).

Il a enseigné au sein du département de Didactique Visuelle de la [Haute école des arts du Rhin](https://www.hear.fr) à Strasbourg et à la [Haute école d’art et de design (HEAD)](https://www.hesge.ch/head/) à Genève, et a travaillé plusieurs années au studio de création graphique [Poste 4](https://www.atelierposte4.com).

Ses activités d’enseignement et de recherche se situent à l’intersection du graphisme, des études visuelles et de l’épistémologie. Il est membre de l’équipe de recherche [Approches contemporaines de la création et de la réflexion artistique](https://accra-recherche.unistra.fr/) - UR 3402 - Université de Strasbourg et du programme de recherche [Cultures visuelles](https://www.culturesvisuelles.org/) qu’il a co-fondé en 2018.

Il est directeur de la [collection « Milieux »](https://www.editions205.fr/collections/milieux) aux éditions Deux-cent-cinq. Il est l’auteur de plusieurs ouvrages sur l’image et le graphisme, dont <cite>Images premières. Aux origines de la représentation visuelle</cite>, (Metis Presses, Genève, 2023. 275 p.), <cite>Qu’est-ce qu’une image dans l’espace public ?</cite> (Éditions Deux-cent-cinq, Lyon, 2022. 84 p.), <cite>Technique et design graphique. Outils, médias, savoirs</cite> (avec Jérôme Saint-Loubert, Éditions B42, Paris, 2020. 288 p.).
