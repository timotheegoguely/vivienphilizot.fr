---
title: "Mentions légales"
translationKey: "legal"
description: "Mentions légales du site vivienphilizot.fr"
template: "legal"
---

# Mentions légales

<small>Conformément aux dispositions des articles 6-III et 19 de la Loi n° 2004-575 du 21 juin 2004 pour la Confiance dans l’économie numérique, dite L.C.E.N., nous portons à la connaissance des utilisateurs et visiteurs du site vivienphilizot.fr les informations suivantes.</small>

## Éditeur

Les site vivienphilizot.fr est édité par Vivien Philizot, 9 rue Jacques Peirotes, Strasbourg, France.
<philizot@unistra.fr>

## Hébergement

[Deuxfleurs](https://deuxfleurs.fr), association loi 1901 déclarée en préfecture d’Ille-et-Vilaine le 29 janvier 2020.

Numéro RNA : W353020804
Numéro SIRET : 89961256800019
Siège social : RDC Appt. 1, 16 rue de la Convention, 59800 Lille, France

## Design & développement web

Design : Vivien Philizot
Développement web : [Timothée Goguely](https://timothee.goguely.com)
Généré avec [Hugo](https://gohugo.io).
Police de caractères : [Immortel Infra G1](https://www.205.tf/immortel) Roman + _Italic_, dessinée par Clément Le Tulle-Neyret et publié par 205TF.
