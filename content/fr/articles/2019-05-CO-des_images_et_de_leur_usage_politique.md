---
title: "« Des images et de leur usage politique »"
description: "Communication dans le cadre de la journée d’études « L’art traversé par le politique : discours, représentations, pratiques », organisée par Katrin Gattinger, (ACCRA - UR 3402), Faculté des Arts, 16 décembre 2019."
author: "Vivien Philizot"
date: 2019-01-01
years: "2019"
places: "Université de Strasbourg"
objects: "communication"
themes: ["images", "sciences", "vision", "intelligence artificielle"]
persons: ["Katrin Gattinger"]
---

# « Des images et de leur usage politique », Communication dans le cadre de la journée d’études « L’art traversé par le politique : discours, représentations, pratiques », organisée par Katrin Gattinger, (ACCRA - UR 3402), Faculté des Arts, 16 décembre 2019.

Les images, comme le suggère WJT Mitchell, sont la devise principale des échanges médiatiques. Or de tels échanges impliquent désormais des acteurs humains et non-humains. Des réseaux de neurones sont entraînés à voir à notre place et délibérément insérés dans d’innombrables contextes de notre vie quotidienne (réseaux sociaux, conduite automatique, diagnostique médical, procédés de contrôle industriels, surveillance, etc.). Mais qu’est-ce que « voir » peut bien signifier pour une machine ? Il s’agit d’aborder cette question à partir de récentes controverses sur les usages politiques de Facebook, pour développer ensuite une réflexion plus large sur les rapports entre voir et savoir.
