---
title: "« L’image et ses régimes de véridiction »"
description: "Communication dans le cadre du séminaire « Faits, <cite>fakes</cite>, fictions. Épistémologie visuelle du vrai et du faux », organisé par Vivien Philizot, Sophie Suma, Sarah Calba, Université de Strasbourg, ACCRA - UR3402, 30 mars 2021."
author: "Vivien Philizot"
date: 2021-01-01
years: "2021"
places: "Strasbourg"
objects: "conférence"
themes: ["images", "vérité", "épistémologie des images", "sciences", "médiations"]
persons: ["Sophie Suma", "Sarah Calba"]
---

# « L’image et ses régimes de véridiction », Communication dans le cadre du séminaire « Faits, <cite>fakes</cite>, fictions. Épistémologie visuelle du vrai et du faux », organisé par Vivien Philizot, Sophie Suma, Sarah Calba, Université de Strasbourg, ACCRA - UR3402, 30 mars 2021.

Les images ne sont que de petites parties d’ensembles bien plus vastes que l’on appelle notre environnement, notre société, notre monde, qui sont largement traversés par des signes graphiques et autres objets visuels de toutes sortes. Ces objets visuels ont de véritables fonctions – scientifique, légale, politique, esthétique – qui les rendent indispensables au quotidien. Cette manière pragmatique de voir les images a-t-elle des effets sur les valeurs de vérité dont nous pouvons les créditer ? Qu’est-ce que peut bien signifier le fait qu’une image soit « vraie » ou « fausse » ? Peut-on imaginer que d’une situation à une autre, le mode de véridiction d’une image puisse changer ? Que l’on tourne notre regard vers les arts, vers les sciences, ou vers les religions, les images ont toujours été prises et enrôlées dans des dispositifs qui impliquaient de répondre à cette question par la négative : l’image, ou, dans un sens plus large, la représentation, serait une « correspondance » entre le monde et le langage. Sa valeur vérité tiendrait toute entière dans cette adéquation. En partant de cas d’études, nous verrons comment décrire différemment ce qui se passe dans les images, ou ce qui « passe » par les images. 
