---
title: "« Épistémologie des images et du design graphique »"
description: "Communication dans le cadre du séminaire « Disciplines et interdisciplinarité », École doctorale des humanités, 19 mars 2021."
author: "Vivien Philizot"
date: 2021-01-01
years: "2021"
places: "Strasbourg"
objects: "conférence"
themes: ["recherche", "épistémologie", "design graphique", "disciplines"]
persons: ""
---

# « Épistémologie des images et du design graphique », Communication dans le cadre du séminaire « Disciplines et interdisciplinarité », École doctorale des humanités, 19 mars 2021.

Comment regarder (scientifiquement) les objets qui constituent notre environnement visuel ? Un recueil de poèmes constructivistes, un collage dadaïste, un plan du métro de Londres, un photomontage des années 1930, un caractère typographique, un timbre poste, un panneau de signalisation autoroutière, une encyclopédie, un schéma, une application numérique. Les différents objets de cette liste hétérogène peuvent-il se donner à voir comme un ensemble cohérent, susceptible de faire l’objet d’un programme de recherche ? La cohérence de cette liste tient tout d’abord à l’expression communément employée pour la résumer : ces objets sont du « design graphique ». Et les quelques entreprises récentes de recherche en design graphique semblent bien confirmer l’intérêt scientifique de cet ensemble. Or ces objets sont par ailleurs étudiés de longue date par plusieurs autres disciplines sous d’autres noms : les signes, les médias, la culture visuelle. Entre design graphique, sémiologie, théorie des médias et culture visuelle, comment étudier notre environnement visuel ?
