---
title: "Collection « Milieux »"
description: "Éditions Deux cent cinq. Direction de la collection."
author: "Vivien Philizot"
date: 2021-01-01
years: "2021"
places: "Lyon"
objects: "collection de livres"
themes: ["médias", "design graphique", "études visuelles", "théorie des médias"]
persons: ["Éloïsa Pérez", "Sophie Suma", "Vanina Pinter", "Jean-Marc Besse", "André Gunthert", "Florence Roller", "Damien Gautier", "Lize Braat"]
---

# Collection « Milieux », Éditions Deux cent cinq. Direction de la collection.

La collection Milieux explore et arpente notre culture visuelle contemporaine. Elle interroge la matérialité et la visualité de nos pratiques sociales, en tant qu’elles sont, de mille et une manière, médiatisées, embarquées, déplacées, par des formes visuelles et textuelles. Ces formes qui occupent notre environnement proche constituent de véritables milieux, à lire et à voir, à comprendre et à interpréter.

<figure>
<img src="/images/063_Vivien-Philizot_collection-Milieux_1200x1200.jpg" alt="" width="1200" height="1200" loading="lazy">
<figcaption>Collection Milieux, Éditions Deux-cent-cinq</figcaption>
</figure>

[Lien vers la version en ligne](https://www.editions205.fr/collections/milieux)
