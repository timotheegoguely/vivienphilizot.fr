---
title: "« Design graphique. Trente mille ans de culture visuelle, trente ans de recherche »"
description: "<cite>Graphisme en France</cite> 2024, Centre national des Arts plastiques, 2024, p.39-61."
author: "Vivien Philizot"
date: 2024-01-01
years: "2024"
places: ""
objects: "article"
themes: ["design graphique", "recherche", "épistémologie", "histoire"]
persons: ""
---

# « Design graphique. Trente mille ans de culture visuelle, trente ans de recherche », <cite>Graphisme en France</cite> 2024, Centre national des Arts plastiques, 2024, p.39-61.

Le design graphique a vu émerger depuis la fin du XX<sup>e</sup> siècle une activité de recherche qui s’est largement développée sous des formes et dans des contextes très divers : dans les programmes pédagogiques des écoles d’art et des universités, dans des projets initiés par des institutions publiques et privées, dans la pratique même des graphistes, lorsqu’elle s’articule à des connaissances théoriques, historiques et critiques. Dans les trente dernières années, la recherche a profondément transformé le paysage du design graphique en France, tout en contribuant à interroger son identité, son histoire, ainsi que ses relations de voisinage avec de nombreux champs de recherche en sciences humaines (histoire de l’art, media studies, sémiotiques et études de culture visuelle…). Cet article consistera à revenir sur cette évolution récente tout en posant des enjeux pour la recherche en design graphique dans les années à venir.

<figure class="max-h-screen align-left">
<img src="/images/083_Muller-Brockmann_1200x929.jpg" alt="" width="1200" height="929" loading="lazy">
<img src="/images/083_Yann-Aucompte-these_1200x853.jpg" alt="" width="1200" height="853" loading="lazy">
<figcaption><cite>A history of visual communication. Geschichte der visuellen Kommunikation. Histoire de la
communication visuelle.</cite> (Niederteufen : Niggli, 1971) ; Yann Aucompte. « La diversité éthique des pratiques de graphisme en France au tournant du XXIe siècle » (These de doctorat, Paris 8, 2022).</figcaption>
</figure>

[Lien vers la version en ligne](https://www.cnap.fr/actualites/graphisme-en-france/revues/ndeg30-graphisme-en-france-30e-anniversaire-2024)
[Télécharger l'article](/pdf/083-PHILIZOT-design_graphique_trente_mille_ans_de_culture_visuelle_trente_ans_de_recherche-2024.pdf)
