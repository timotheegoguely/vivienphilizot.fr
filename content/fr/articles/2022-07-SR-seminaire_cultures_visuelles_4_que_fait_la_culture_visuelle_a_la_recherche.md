---
title: "Séminaire Cultures visuelles #4 : Que fait la culture visuelle à la recherche ?"
description: "Master 1 et 2, ACCRA - UR3402, Faculté des Arts, Université de Strasbourg. Co-organisation avec Sophie Suma et Simon Zara. 9 séances entre janvier et avril 2022."
author: "Vivien Philizot, Sophie Suma, Simon Zara"
date: 2022-01-01
years: "2022"
places: ""
objects: "séminaire de recherche"
themes: ["recherche", "culture visuelle", "montage", "enquête"]
persons: ["Sophie Suma", "Maxime Boidy", "Sarah Calba", "Simon Zara"]
---

# Séminaire Cultures visuelles #4 : Que fait la culture visuelle à la recherche ?, Master 1 et 2, ACCRA - UR3402, Faculté des Arts, Université de Strasbourg. Co-organisation avec Sophie Suma et Simon Zara. 9 séances entre janvier et avril 2022.

Que fait l’image au sein d’une recherche scientifique, notamment dans le champ des études visuelles ? Comment installe-t-elle un mode discursif, voire un mode d’existence des objets de recherche singulier ? Comment permet-elle de mettre en évidence ce que par ailleurs les textes ne permettent pas toujours de faire passer ou d’exprimer ? Qu’est-ce que ces deux régimes sémiotiques (la description et la dépiction) ont-ils à se dire ? Il s’agira moins de valoriser les images comme moyens de mener des études alternatives que de réévaluer leur statut. Plus que des objets d’étude, les images peuvent être conçues aussi bien comme moyens que comme fins de la recherche. Du moins si l’on s’accorde à penser que les images sont autant d’agents avec lesquels nous pouvons dialoguer. D’Aby Warburg à Forensic Architecture, de nombreux exemples issus de traditions de recherche très différentes sont abordés au cours des séances. Invité·e·s et intervenant·e·s : Maxime Boidy (LISAA EA 4120), Simon Zara (ACCRA Université de Strasbourg), Vivien Philizot (ACCRA Université de Strasbourg), Sarah Calba (Archives Henri Poincaré), Sophie Suma (AMUP - ACCRA Université de Strasbourg). 
