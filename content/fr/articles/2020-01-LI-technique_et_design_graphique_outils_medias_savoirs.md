---
title: "<cite>Technique et design graphique. Outils, médias, savoirs</cite>"
description: "Éditions B42, Paris, 2020. 288p. ISBN : 9782490077212. Co-direction Jérôme Saint-Loubert-Bié"
author: "Vivien Philizot et Jérôme Saint-Loubert Bié (dir.)"
date: 2020-01-01
years: "2020"
places: ""
objects: "livre"
themes: ["design graphique", "technique", "typographie", "impression", "objets", "médias"]
persons: ["Jérôme Saint-Loubert-Bié", "Bruno Bachimont", "Max Bonhomme", "Loup Cellard", "Kevin Donnot", "Pierre-Damien Huyghe", "Indra Kupferschmid", "Jean-Noël Lafargue", "Anthony Masure", "Vivien Philizot", "Fabrice Sabatier", "Nicolas Thély"]
---

# <cite>Technique et design graphique. Outils, médias, savoirs</cite>, Éditions B42, Paris, 2020. 288p. ISBN : 9782490077212. Co-direction Jérôme Saint-Loubert-Bié

Notre environnement visuel est traversé de part en part par la technique. Pourtant, ces opérations sont bien souvent maintenues dans l’ombre de questions plus nobles portant sur les graphistes, leurs démarches et les formes qu’ils produisent. Les différentes contributions de cet ouvrage montrent, selon plusieurs éclairages complémentaires, que la technique n’est pas réductible à des opérations quantifiées ou à des objets fonctionnels, mais qu’elle revêt plus largement une dimension anthropologique beaucoup plus ancienne et profonde que ce que nos environnements technologiques ne laissent imaginer. Cet ouvrage témoigne de la multiplicité des approches possibles sur le sujet ainsi que de la fertilité d’une thématique qui reste encore largement à défricher, au croisement du design, des études visuelles, et des humanités numériques. Avec les contributions de Bruno Bachimont, Max Bonhomme, Loup Cellard, Kevin Donnot, Pierre-Damien Huyghe, Indra Kupferschmid, Jean-Noël Lafargue, Anthony Masure, Vivien Philizot, Fabrice Sabatier, Nicolas Thély.

<figure class="max-h-screen align-left">
<img src="/images/054_Technique-et-design-graphique_960x640.jpg" alt="" width="960" height="640" loading="lazy">
<figcaption>Vivien Philizot, Jérôme Saint-Loubert Bié (dir.), <cite>Technique et design graphique</cite>, Éditions B42, 2020</figcaption>
</figure>
