---
title: "Exposition « Une cartographie de la recherche en design graphique »"
description: "Co-commissariat avec Malte Martin. Exposition présentée au Signe, Centre national du graphisme, Chaumont, du 13 mai au 6 août 2017."
author: "Vivien Philizot, Malte Martin"
date: 2017-01-01
years: "2017"
places: ["Centre national du graphisme", "Chaumont", "Galerie NaMiMa", "Nancy"]
objects: "exposition"
themes: ["recherche en design graphique", "cartographie", "signes"]
persons: ["Malte Martin", "Philippe Riehling", "Michaël Mouyal", "Julienne Richard"]
---

# Exposition « Une cartographie de la recherche en design graphique », Co-commissariat avec Malte Martin. Exposition présentée au Signe, Centre national du graphisme, Chaumont, du 13 mai au 6 août 2017.

Ces dernières années ont vu l’émergence d’un intérêt grandissant pour la recherche en design graphique, que l’on mesure à l’aune des axes et des projets développés dans les écoles d’art, du nombre croissant de thèses en préparation, du travail de consolidation fait par différents acteurs institutionnels. À quoi peut bien ressembler le paysage de la recherche en design graphique ? C’est à cette question que tente de répondre cette exposition, qui en retour, peut se lire et se penser comme une cartographie. Exposition présentée au Signe, Centre national du graphisme, Chaumont, du 13 mai au 6 août 2017. Exposition représentée à la Galerie NaMiMa de l’École supérieure d’art de Nancy, du 17 octobre au 30 novembre 2017.

<figure>
<img src="/images/038_Cartographie-recherche-design-graphique-0_1200x849.jpg" alt="" width="1200" height="849" loading="lazy">
<img src="/images/038_Cartographie-recherche-design-graphique-1_1200x800.jpg" alt="" width="1200" height="849" loading="lazy">
<img src="/images/038_Cartographie-recherche-design-graphique-2_1200x800.jpg" alt="" width="1200" height="849" loading="lazy">
<img src="/images/038_Cartographie-recherche-design-graphique-3_1200x800.jpg" alt="" width="1200" height="849" loading="lazy">
<figcaption>Exposition « Une cartographie de la recherche en design graphique », 2017</figcaption>
</figure>

[Télécharger le document d'exposition](/pdf/038-PHILIZOT-exposition_une_cartographie_de_la_recherche_en_design_graphique-2017.pdf)
