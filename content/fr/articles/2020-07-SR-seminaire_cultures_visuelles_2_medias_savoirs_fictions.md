---
title: "Séminaire Cultures visuelles #2 : Médias Savoirs Fictions"
description: "Master 2, ACCRA - UR3402, Faculté des Arts, Université de Strasbourg. Co-organisation avec Sophie Suma - 9 séances entre janvier et avril 2020. (dernières séances annulées à cause de la crise sanitaire)."
author: "Vivien Philizot, Sophie Suma"
date: 2020-01-01
years: "2020"
places: ""
objects: "séminaire de recherche"
themes: ["cultures visuelles", "arts plastiques", "architecture", "histoire"]
persons: ["Sophie Suma", "Nathalie Delbard", "Anke Vrijs", "Katrin Gattinger", "Sarah Calba", "Nolwenn Maudet", "Simon Zara", "Chiara Palermo"]
---

# Séminaire Cultures visuelles #2 : Médias Savoirs Fictions, Master 2, ACCRA - UR3402, Faculté des Arts, Université de Strasbourg. Co-organisation avec Sophie Suma - 9 séances entre janvier et avril 2020. (dernières séances annulées à cause de la crise sanitaire).

Dans la perspective récemment ouverte par les études culturelles et visuelles, ce séminaire vise à déployer un ensemble de réflexions sur l’image, entendue ici non pas comme un objet physique (une surface plane présentant une configuration de formes signifiantes), mais comme un processus dynamique, une relation, un rapport social embarqué ou incarné par des objets visuels. L’image est un point d’entrée, une médiation privilégiée de quelque chose qui passe par le visible ou qui est rendu visible (ou invisible) : rapports de pouvoir, connaissance, information, etc. Elle est délibérément envisagée ici comme une forme interdisciplinaire, par laquelle sont appréhendées des problématiques qui ressortissent à différentes pratiques et champs du savoir, (cinéma, arts du spectacle, danse, design, arts plastiques, architecture, histoire de l’art, et plus généralement les sciences humaines et sociales). Invité·e·s : (Nathalie Delbard (CEAC, Université de Lille), Anke Vrijs (ACCRA, INSA Strasbourg), Katrin Gattinger (ACCRA, Université de Strasbourg), Sarah Calba (Archives Henri Poincaré), Nolwenn Maudet (ACCRA, Université de Strasbourg), Simon Zara (CEAC, Université de Lille), Chiara Palermo (CREPhAC, Université de Strasbourg).
