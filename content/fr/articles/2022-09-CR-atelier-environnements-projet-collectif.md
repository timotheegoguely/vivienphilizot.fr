---
title: "Atelier Environnements - Projet collectif"
description: "Master 1 et 2, parcours Design Environnements publics, Faculté des Arts de l’Université de Strasbourg, 2022-2024"
author: "Vivien Philizot"
date: 2022-01-01
years: "2022"
places: "Université de Strasbourg"
objects: "cours"
themes: ["design graphique", "projet", "environnements"]
persons: ""
---

# Atelier Environnements - Projet collectif, Master 1 et 2, parcours Design Environnements publics, Faculté des Arts de l’Université de Strasbourg, 2022-2024

L’atelier « Projet collectif » consiste à aborder le sujet à travers la pratique et la recherche de terrain. Pour l’année 2023-2024, il s’agira d’enquêter sur les signes et les images dans l’espace urbain, par l’observation, la récolte de documents, la prise de vue, le croquis, afin d’alimenter une base de données partagée avec les étudiants de l’université de Potsdam. Ces matériaux ont vocation à être remobilisés, régorganisés et étudiés, notamment dans le cadre de l’atelier « Organiser un événement scientifique ».
