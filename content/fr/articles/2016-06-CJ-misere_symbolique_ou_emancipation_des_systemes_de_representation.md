---
title: "Misère symbolique ou émancipation des systèmes de représentation"
description: "Colloque. Université de Strasbourg, Maison Interuniversitaire des Sciences de l’Homme-Alsace Université, 31 mai et 1er juin 2016. Co-organisation avec Ruedi Baur et Pierre Litzler."
author: null
date: 2016-01-01
years: "2016"
places: "Université de Strasbourg"
objects: "colloque et journée d’études"
themes: ["design graphique", "identité visuelle", "savoirs", "systèmes de représentation"]
persons: ["Violaine Appel", "Vera Baur", "Alain Beretz", "Nawal Bakouri", "Laurie Chapotte", "Xavier Crouan", "Julien Defait", "Olivier Deloignon", "Michel Deneken", "Bernard Emsellem", "Martin Grandjean", "Etienne Guidat", "Pierre-Damien Huyghe", "Jean-François Lanneluc", "Claire Laval", "Malte Martin", "Dominique Mégard", "Faustine Najman", "Daniel Payot", "Christina Poth", "Franck Tallon", "Armelle Tanvez", "Fabrice Papy", "Philippe Portelli", "Armelle Tanvez", "Cécilia Zanni-Merk", "Ruedi Baur", "Pierre Litzler"]
---

# Misère symbolique ou émancipation des systèmes de représentation, Colloque. Université de Strasbourg, Maison Interuniversitaire des Sciences de l’Homme-Alsace Université, 31 mai et 1er juin 2016. Co-organisation avec Ruedi Baur et Pierre Litzler.

Ce colloque porte sur les systèmes de représentation d’institutions publiques à grandes échelle : collectivités, états, institutions culturelles, hôpitaux, universités, etc. Les identités visuelles de ces institutions empruntent bien souvent les logiques de la marque, sur le modèle des sociétés commerciales. Mais le logotype est-il pour les institutions la seule manière d’exister dans l’espace public ? Ces systèmes de représentation permettent-ils l’exercice de la citoyenneté ? Comment le design graphique peut-il représenter la complexité de ces ensembles ? Intervenant·e·s : Violaine Appel, Vera Baur-Kockot, Alain Beretz, Nawal Bakouri, Laurie Chapotte, Xavier Crouan, Julien Defait, Olivier Deloignon, Michel Deneken, Bernard Emsellem, Martin Grandjean, Etienne Guidat, Pierre-Damien Huyghe, Jean-François Lanneluc, Claire Laval, Malte Martin, Dominique Mégard, Faustine Najman, Daniel Payot, Christina Poth, Franck Tallon, Armelle Tanvez, Fabrice Papy, Philippe Portelli, Armelle Tanvez, Cécilia Zanni-Merk
