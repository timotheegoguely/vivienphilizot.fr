---
title: "« Langages graphiques »"
description: "Communication dans le cadre de la journée d’étude « Histoire du design graphique », Patricia Falguières (EHESS), Véronique Marrier, Centre national des arts plastiques (CNAP), La Défense, Paris, 18 septembre 2014."
author: "Vivien Philizot"
date: 2014-01-01
years: "2014"
places: ["Centre national des arts plastiques", "Paris"]
objects: "communication"
themes: ["langages graphiques", "avant-gardes", "modernité"]
persons: ["Véronique Marrier", "Patricia Falguières"]
---

# « Langages graphiques », Communication dans le cadre de la journée d’étude « Histoire du design graphique », Patricia Falguières (EHESS), Véronique Marrier, Centre national des arts plastiques (CNAP), La Défense, Paris, 18 septembre 2014.

Si la modernité est le terrain d’élaboration des « langages graphiques », pensés par les Modernes comme des formes universelles d’expression visuelle, le « langage » est ici métaphorique. Les langages graphiques retiennent du langage ses propriétés systématiques – le fait de s’organiser en système. Ils s’en distinguent en ayant recours au “visuel” comme catégorie singulière. Car ce que semble avoir développé la modernité graphique, c’est bien une conception transculturelle du visuel, adossée au projet universaliste et idéaliste des avant-gardes. Théorisée dans les années 1920, cette idée trouve ses prolongements en design graphique dans les écoles d’art après guerre, comme le Nouveau Bauhaus à Chicago ou l’école d’Ulm, abondamment nourries des théories issues du mouvement moderne, des avant-gardes, de la peinture abstraite et de la Gestalt.
