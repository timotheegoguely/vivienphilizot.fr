---
title: "Exposition « 20000 exemplaires »"
description: "co-commissariat avec Sandra Chamaret. Exposition présentée dans le cadre de la Biennale de l’édition 2017, Palais universitaire de Strasbourg, du 30 mars au 14 avril 2017."
author: "Sandra Chamaret, Vivien Philizot"
date: 2017-01-01
years: "2017"
places: ["Palais universitaire de Strasbourg", "Strasbourg"]
objects: "exposition"
themes: ["édition à grand tirage", "fabrication", "livre"]
persons: ["Sandra Chamaret"]
---

# Exposition « 20000 exemplaires », co-commissariat avec Sandra Chamaret. Exposition présentée dans le cadre de la Biennale de l’édition 2017, Palais universitaire de Strasbourg, du 30 mars au 14 avril 2017.

« Certains livres sont faits pour être lus, d’autres sont faits pour être vus. » Partant de cette affirmation, cette exposition présente l’économie du livre et plus précisément l’édition à grand tirage – diffusée et vendue au plus grand nombre. Sont présentés ses choix éditoriaux, ses mécanismes de diffusion et de vente, ses techniques de promotion, ses voies de légitimation ainsi que les motivations des lecteurs/acheteurs, à travers plusieurs synthèses visuelles et en volumes, construites d’après des récoltes de chiffres analysés.
