---
title: "Que font les images de l’IA ? Imaginaires, fictions et représentations"
description: "Journée d’étude, Université de Strasbourg et Université de Lille. En ligne, 15 et 16 avril 2021. Co-organisation avec Nolwenn Maudet (UR 3402) et Simon Zara (ULR 3587)."
author: "Nolwenn Maudet, Vivien Philizot, Simon Zara"
date: 2021-01-01
years: "2021"
places: "Université de Strasbourg"
objects: "colloque et journée d’études"
themes: ["intelligences artificielles", "imaginaires", "médias", "technologies"]
persons: ["Nolwenn Maudet", "Simon Zara", "Florian Harmand", "David Pucheu", "Lutz Robbers", "Marie Bourget-Mauger", "Loïc Horellou", "Marion Balac", "Ludivine Allienne-Diss", "Portelli Aurélien", "Guarnieri Franck", "Alberto Romele", "Dario Rodighiero", "Marta Severo"]
---

# Que font les images de l’IA ? Imaginaires, fictions et représentations, Journée d’étude, Université de Strasbourg et Université de Lille. En ligne, 15 et 16 avril 2021. Co-organisation avec Nolwenn Maudet (UR 3402) et Simon Zara (ULR 3587).

Cette dernière décennie a vu de nombreuses avancées technologiques remettre sur le devant des scènes scientifique et médiatique le terme d’intelligence artificielle. Cette journée sera l’occasion d’interroger les multiples images et représentations de l’intelligence artificielle, selon trois axes : généalogie des représentations de l’intelligence artificielle, les représentations de l’intelligence artificielle comme analogies, métaphores ou figures de l’altérité, les représentations de l’intelligence artificielle comme formes prospectives ou critiques. La réflexion portera sur ce que font les représentations de l’intelligence artificielle, sur la manière dont elles produisent des imaginaires et anticipent des discussions sur les finalités de nos environnements techniques, sur la socialisation des machines, sur les contradictions sociales dont la technologie n’est bien souvent qu’une expression sublimée. Organisation : Nolwenn Maudet, Vivien Philizot, Simon Zara. Intervenant·e·s : Florian Harmand, David Pucheu, Lutz Robbers, Marie Bourget-Mauger, Loïc Horellou, Marion Balac, Ludivine Allienne-Diss, Portelli Aurélien, Guarnieri Franck, Alberto Romele, Dario Rodighiero, Marta Severo
