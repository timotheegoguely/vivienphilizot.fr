---
title: "« Temps dispersés. L’épistémologie visuelle à l’épreuve de Here, de Richard McGuire »"
description: "Communication dans le cadre du séminaire « Que fait la culture visuelle à la recherche ? », organisé par Vivien Philizot, Sophie Suma, Simon Zara, Université de Strasbourg, ACCRA - UR3402, 8 mars 2022"
author: "Vivien Philizot"
date: 2022-01-01
years: "2022"
places: "Strasbourg"
objects: "conférence"
themes: ["photographie", "chronophotographie", "Richard McGuire", "roman graphique", "images", "temps", "sciences"]
persons: ""
---

# « Temps dispersés. L’épistémologie visuelle à l’épreuve de Here, de Richard McGuire », Communication dans le cadre du séminaire « Que fait la culture visuelle à la recherche ? », organisé par Vivien Philizot, Sophie Suma, Simon Zara, Université de Strasbourg, ACCRA - UR3402, 8 mars 2022

L’image, et plus précisément la photographie, a souvent été définie par sa capacité à fixer un temps révolu, à mettre en présence une absence, à attester de ce qui « a été », selon la formule consacrée par Barthes dans son célèbre essai sur la photographie, <cite>La Chambre claire</cite>. Cette propriété des photographies est certainement renforcée par la métaphore de la fenêtre, depuis longtemps chargée d’exprimer tout à la fois le cadrage sélectif et l’altérité constitutive des images, lorsqu’elles ouvrent sur un morceau du monde, sur un ailleurs, sur quelque chose qui n’est pas ou plus là. C’est précisément cette altérité que met en scène <cite>Here</cite>, « Ici », une bande dessinée de Richard McGuire, publiée à l’origine en 1989 dans la revue <cite>Raw</cite>, sous forme de six planches qui offrent un même point de vue sur l’intérieur d’une maison américaine saisie à différents moments de son histoire. La visualisation est employée de longue date en sciences pour donner à comprendre des phénomènes dans leur durée, pour mettre en évidence des changements d’états, pour montrer l’évolution d’un objet, d’un lieu ou d’une situation. Mais de quelle épistémologie visuelle procède cet usage des images, que pousse dans ses retranchements l’ingénieux dispositif visuel conçu par McGuire ?
