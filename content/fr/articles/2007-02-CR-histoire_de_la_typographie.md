---
title: "Histoire de la typographie"
description: "Cours magistral. Année 2 et 3 Arts, Tronc commun, Haute école des arts du Rhin (Hear), Licence 3 Arts, parcours Design, Faculté des Arts de l’Université de Strasbourg, de 2007 à 2014"
author: "Vivien Philizot"
date: 2007-01-01
years: "2007"
places: "Université de Strasbourg"
objects: "cours"
themes: ["histoire", "théorie", "typographie"]
persons: ""
---

# Histoire de la typographie, Cours magistral. Année 2 et 3 Arts, Tronc commun, Haute école des arts du Rhin (Hear), Licence 3 Arts, parcours Design, Faculté des Arts de l’Université de Strasbourg, de 2007 à 2014

Ce cours porte sur l’histoire de la lettre et sur les transformations sociales et politiques dont s’accompagne l’histoire de l’écrit et de la diffusion des savoirs depuis l’invention de l’imprimerie. Il s’agit d’examiner les différentes traditions historiques qui ont mené de la culture classique de l’imprimerie à nos définitions moderne et contemporaine de la typographie. Le cours passe par les étapes importantes de cette histoire, en s’attardant sur les enjeux épistémologiques posés par les différents seuils techniques dont elle est jalonnée. De l’invention de l’imprimerie à la typographie numérique, cette histoire croise des approches techniques, matérielles et sociales, pour éclairer un objet étroitement lié au langage, à l’écriture et à la diffusion des savoirs.
