---
title: "« Le chercheur en cartographe. Quelles relations entre sciences et design graphique ? »"
description: "in <cite>Étapes</cite>, 252/2019, p.76-79."
author: "Vivien Philizot"
date: 2019-01-01
years: "2019"
places: ""
objects: "article"
themes: ["design graphique", "théorie", "recherche", "sciences"]
persons: ""
---

# « Le chercheur en cartographe. Quelles relations entre sciences et design graphique ? », in <cite>Étapes</cite>, 252/2019, p.76-79.

Faire de la science, c’est tracer des cartes. Circonscrire un territoire spécifique, repérer des discontinuités dans la trame apparente des choses, délimiter des régions, des provinces et leurs frontières, opérer des rapprochements ou instaurer des ruptures dans la texture du monde. Cette métaphore connue peut-elle nous apprendre quelque chose sur la part scientifique du design graphique, ce versant réflexif de la pratique, qui prend les différents noms de « recherche », « théorie », « science » ? Quelles sont les spécificités de la recherche au sujet du design graphique, dont nous entendons autant parler ? Quels sont les approches, les méthodes, les concepts susceptibles d’être mobilisés dans une recherche de ce genre ? Ou pour le dire en d’autres termes, si le design graphique est un territoire identifiable, quelles sont les cartes susceptibles d’être tracées pour l’appréhender ? 

[Télécharger l'article](/pdf/047-PHILIZOT-le_chercheur_en_cartographe-2019.pdf)
