---
title: "« Kitsch, bad taste, Scheiße. Une esthétique de la dissonance »"
description: "in <cite>Azimuts</cite>, École supérieure d’Art et Design de Saint-Étienne, 42-2015, p.109-136."
author: "Vivien Philizot"
date: 2015-01-01
years: "2015"
places: ""
objects: "article"
themes: ["kitsch", "vernaculaire", "camp", "design graphique", "détournement", "citation", "dissonance"]
persons: ""
---

# « Kitsch, bad taste, Scheiße. Une esthétique de la dissonance », in <cite>Azimuts</cite>, École supérieure d’Art et Design de Saint-Étienne, 42/2015, p.109-136.

Les champ du design, du graphisme et de l’architecture semblent travaillés depuis au moins la fin du XX<sup>e</sup> siècle par une forme d’esthétique de la déception, qui, en rejouant littéralement l’opposition du pur et de l’impur, du savant et du populaire, nous renvoie à une conception unique et verticale des valeurs qui reste discutable. Il s’agit ici de réexaminer ces pratiques à la lumière d’une métaphore musicale, qui peut permettre de comprendre dans un même mouvement la manière dont le « camp », le vernaculaire et le kitsch opèrent au sein du design. Moment critique dans un système en équilibre, la dissonance procède de l’écart, du désaccord, d’une certaine logique de la mise à distance qui ne peut qu’être relative.

<figure>
<img src="/images/019_Bambi_560x500.jpg" alt="" width="560" height="500" loading="lazy">
</figure>

[Lien vers la version en ligne](https://revue-azimuts.fr/numeros/42/kitsch-bad-taste-scheisse-une-esthetique-de-la-dissonance)
[Télécharger l'article](/pdf/019-PHILIZOT-kitsch_bad_taste_scheisse_une_esthetique_de_la_dissonance-2015.pdf)
