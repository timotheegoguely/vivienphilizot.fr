---
title: "Atlas. Le graphiste en traducteur"
description: "Exposition. Palais universitaire de Strasbourg, du 18 février au 15 mars 2013."
author: "Vivien Philizot"
date: 2013-01-01
years: "2013"
places: ["Palais universitaire de Strasbourg", "Strasbourg"]
objects: "exposition"
themes: ["atlas", "design graphique", "traduction"]
persons: ""
---

# Atlas. Le graphiste en traducteur, Exposition. Palais universitaire de Strasbourg, du 18 février au 15 mars 2013.

L’atlas a pu représenter, dans la culture moderne européenne, un moyen graphique de parcourir le monde du regard, d’en inventorier la diversité, d’organiser des objets qui pouvaient alors se distribuer dans un espace graphique offert au regard. L’atlas est au croisement du visuel et du discursif. Cette exposition présente des atlas conçu par les étudiants du Master design autour de thématiques diverses, interrogeant les rapports entre les connaissances scientifiques et les formes visuelles qui peuvent les transmettre et les donner à comprendre.
