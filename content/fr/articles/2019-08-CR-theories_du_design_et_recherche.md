---
title: "Introduction aux théories du design et à la recherche"
description: "Cours magistral. Licence 3 Arts, parcours Design, Faculté des Arts de l’Université de Strasbourg 2019-2020"
author: "Vivien Philizot"
date: 2019-01-01
years: "2019"
places: "Université de Strasbourg"
objects: "cours"
themes: ["histoire et théorie", "design", "recherche"]
persons: ""
---

# Introduction aux théories du design et à la recherche, Cours magistral. Licence 3 Arts, parcours Design, Faculté des Arts de l’Université de Strasbourg 2019-2020

Cet enseignement porte sur les relations qui se sont établies au XX<sup>e</sup> siècle entre science et design. Il s’agit de comprendre comme comment le design a pu se penser dans l’histoire récente comme une discipline et une activité réflexive. En étudiant différents projets théoriques émanant de designers, mais aussi de scientifiques, sociologues, philosophes, anthropologues, nous verrons comment la théorie en design émerge tantôt d’un faisceau nourri de disciplines hétérogènes, tantôt de tentatives d’en faire une science autonome. Si la recherche en design continue de s’inventer au début du XXI<sup>e</sup> siècle, c’est parce qu’elle soulève des problématiques sociales, politiques et critiques qui débordent largement du cadre initial de la pratique. L’apport des études culturelles et visuelles nous permettra de replacer ces enjeux dans le large spectre des sciences humaines et sociales.
