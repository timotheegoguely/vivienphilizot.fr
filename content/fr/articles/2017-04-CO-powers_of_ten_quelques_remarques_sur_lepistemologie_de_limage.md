---
title: "« <cite>Powers of Ten</cite>. Quelques remarques sur l’épistémologie de l’image »"
description: "Communication dans le cadre du colloque « L’image », organisé par Catherine Florentz, Valérie Simonneaux-Eve (INCI), Charles Hirlimann (IPCMS), Vincente Fortier (DRES), Jay Rowell (SAGE) Dino Moras (IGBMC), CNRS et Université de Strasbourg, 1er et 2 juin 2017."
author: "Vivien Philizot"
date: 2017-01-01
years: "2017"
places: "Université de Strasbourg"
objects: "communication"
themes: ["Ray Eames", "Charles Eames", "Powers of Ten", "images", "art et sciences"]
persons: ""
---

# « <cite>Powers of Ten</cite>. Quelques remarques sur l’épistémologie de l’image », Communication dans le cadre du colloque « L’image », organisé par Catherine Florentz, Valérie Simonneaux-Eve (INCI), Charles Hirlimann (IPCMS), Vincente Fortier (DRES), Jay Rowell (SAGE) Dino Moras (IGBMC), CNRS et Université de Strasbourg, 1er et 2 juin 2017.

<cite>Powers of Ten</cite> (Puissances de dix) est un court métrage réalisé en 1977 par Charles et Ray Eames, qui se présente sous la forme d’un travelling faisant voyager le spectateur à travers différents ordres de grandeur, de l’infiniment petit à l’infiniment grand. En présentant ainsi dans un même continuum des images d’objets infiniment éloignés et infiniment proches, <cite>Powers of Ten</cite> invite à s’interroger sur le point aveugle de la représentation scientifique, sur le rôle de premier plan que nous faisons tenir aux images, sur la place qu’elles prennent dans notre rapport à la connaissance, et sur les pouvoirs démesurés que nous leur attribuons.
