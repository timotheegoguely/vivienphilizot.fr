---
title: "« Simon Patterson, The Great Bear »"
description: "in Collectif, <cite>48°34’52” – 7°45’33” – une cartographie illustrée de la didactique visuelle</cite>, catalogue d’exposition, Haute école des arts du Rhin, Strasbourg, 2010, p.10."
author: "Vivien Philizot"
date: 2010-01-01
years: "2010"
places: ["Haute école des arts du Rhin", "Strasbourg"]
objects: "chapitre d’ouvrage"
themes: ["Simon Patterson", "Harry Beck", "cartographie", "plan de métro"]
persons: ""
---

# « Simon Patterson, The Great Bear », in Collectif, <cite>48°34’52” – 7°45’33” – une cartographie illustrée de la didactique visuelle</cite>, catalogue d’exposition, Haute école des arts du Rhin, Strasbourg, 2010, p.10.

Ce texte présente le travail de l’artiste Simon Patterson, plus précisément du détournement de la cartographie du métro de Londres réalisé en 2008. Cette carte est une citation du plan conçu en 1933 par Harry Beck, et devenu un modèle d’organisation pour les plans des réseaux de transports de nombreux pays. Patterson remplace les stations par des noms d’artistes, philosophes, acteurs, sportifs, musiciens, etc., donnant lieu à des associations surprenantes.
