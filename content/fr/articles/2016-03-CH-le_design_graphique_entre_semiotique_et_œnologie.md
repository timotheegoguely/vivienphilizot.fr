---
title: "« Le design graphique, entre sémiotique et œnologie »"
description: "in Ruedi Baur, Pierre Litzler, Vivien Philizot (dir.), <cite>Langages visuels et systèmes complexes</cite>, Presses universitaires de l’Université de Strasbourg, 2016, p.44-49."
author: "Vivien Philizot"
date: 2016-01-01
years: "2016"
places: "Strasbourg"
objects: "chapitre d’ouvrage"
themes: ["objets graphiques", "iconoclasme", "histoire", "identités visuelles"]
persons: ["Frederick R. Barnard"]
---

# « Le design graphique, entre sémiotique et œnologie », in Ruedi Baur, Pierre Litzler, Vivien Philizot (dir.), <cite>Langages visuels et systèmes complexes</cite>, Presses universitaires de l’Université de Strasbourg, 2016, p.44-49.

Cet article aborde les questions de réception des objets graphiques, à partir de la tradition historique de celle des images. Si l’image peut bien “valoir mille mots” comme nous l’enseigne l’expression lancée par le publicitaire Frederick R. Barnard dans les années 1920, la polysémie dont elle est suspectée la reconduit bien souvent à l’ombre du texte, de ce qui s’écrit sans équivoque ni ambiguité en “noir sur blanc”, quand ce n’est pas “gravé dans le marbre”. En prenant acte de l’alternance enregistrée par l’histoire entre iconoclasme et fétichisme, il s’agit ici de plaider, à la suite de W.J.T. Mitchell, pour la réintroduction d’un rapport aux images sous les auspices du totem.

[Télécharger l'article](/pdf/026-PHILIZOT-le_design_graphique_entre_semiotique_et_œnologie-2016.pdf)
