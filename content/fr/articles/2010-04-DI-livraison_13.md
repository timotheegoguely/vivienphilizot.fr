---
title: "Langage et typographie"
description: "<cite>Livraison 13</cite>, revue d’art contemporain et de design graphique, 13/2010. 172 p. "
author: "Vivien Philizot"
date: 2010-01-01
years: "2010"
places: "Strasbourg"
objects: "revue"
themes: ["langage", "typographie", "design graphique"]
persons: ["Nicolas Simonin", "Claude Gretillat"]
---

# Langage et typographie, <cite>Livraison 13</cite>, revue d’art contemporain et de design graphique, 13/2010. 172 p. 

Matière physique de l’écriture et de la pensée qu’elle matérialise, la typographie est le lieu d’une rencontre entre un contenu linguistique et une forme plastique, entre une idée et une mise en forme destinée à la fixer. Le caractère typographique cependant, de par sa forme, son origine et son style, engage dans cette rencontre sa propre histoire, se faisant ainsi le vecteur d’une signification concurrente impossible à négliger. En ouvrant des pistes de réflexion et des itinéraires croisés, ce numéro treize se propose d’explorer les rapports que la typographie entretient avec le langage.
