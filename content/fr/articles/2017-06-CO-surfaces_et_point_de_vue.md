---
title: "« Surfaces et point de vue. Du Colorama aux selfies, métamorphoses du regard dans l’espace public »"
description: "Communication dans le cadre du colloque « Que font les images dans l’espace public ? », organisé par Allison Huetz, Clémence Lehec, Thierry Maeder, Raphaël Pieroni, Estelle Sohier, Jean-François Staszak (Laboratoire InfoGéo), Université de Genève, 18-20 janvier 2017."
author: "Vivien Philizot"
date: 2017-01-01
years: "2017"
places: "Université de Genève"
objects: "communication"
themes: ["Colorama", "Kodak", "photographie familiale", "études visuelles", "gare de New York"]
persons: ""
---

# « Surfaces et point de vue. Du Colorama aux selfies, métamorphoses du regard dans l’espace public », Communication dans le cadre du colloque « Que font les images dans l’espace public ? », organisé par Allison Huetz, Clémence Lehec, Thierry Maeder, Raphaël Pieroni, Estelle Sohier, Jean-François Staszak (Laboratoire InfoGéo), Université de Genève, 18-20 janvier 2017.

Entre les années 1950 et 1989, la société Kodak occupe en continu un espace de près d’une centaine de mètres carrés dans la gare centrale de New York sous la forme de photographies panoramiques changées régulièrement. En étudiant la généalogie de ce projet, il s’agit de comprendre ce que la mise en scène du point de vue dans l’espace de la ville nous enseigne sur la construction sociale du regard. L’héritage critique des visual studies peut nous aider à reconsidérer cette discipline consacrée au travail de la représentation : le design graphique.
