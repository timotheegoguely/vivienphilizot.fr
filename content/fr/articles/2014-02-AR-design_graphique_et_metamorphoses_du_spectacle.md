---
title: "« Design graphique et métamorphoses du spectacle. Quelques notes de bas de page à <cite>Dix notes de bas de page à un manifeste</cite> »"
description: "in <cite>Graphisme en France</cite> 2014, Centre national des Arts plastiques, 2014, p.27-45. "
author: "Vivien Philizot"
date: 2014-01-01
years: "2014"
places: ""
objects: "article"
themes: ["manifeste", "design graphique", "politique", "graphisme commercial", "valeur", "capital", "graphisme dissensuel"]
persons: ["Ken Garland", "Michael Bierut"]
---

# « Design graphique et métamorphoses du spectacle. Quelques notes de bas de page à <cite>Dix notes de bas de page à un manifeste</cite> », in <cite>Graphisme en France</cite> 2014, Centre national des Arts plastiques, 2014, p.27-45. 

En 1964 parait en Angleterre le manifeste <cite>First Things First</cite>, qui dénonce alors les dérives d’une profession encore naissante – le design graphique – au service du mode de vie occidental capitaliste et de l’économie de marché. Jalonnée par plusieurs autres manifestes ou contre propositions, l’histoire qui s’en suit nous invite à nous interroger sur la légitimité de la distinction entre « culturel » et « commercial », sur la « division du travail du sens » comme mode opératoire du design depuis le Bauhaus, sur les métamorphoses des procédés publicitaires et sur l’extension du champ du design au domaine de l’existence (prophétisé par László Moholy-Nagy) propre à la postmodernité.

<figure>
<img src="/images/013_Ten-Footnotes_M_Bierut_1200x1078.jpg" alt="" width="1200" height="1078" loading="lazy">
<figcaption>Bierut, Michael. « Ten Footnotes to a Manifesto ». In <cite>Seventy-Nine Short Essays on design</cite>, Michael Bierut, 52‑60. New York: Princeton Architectural Press, 2007.</figcaption>
</figure>

[Lien vers la version en ligne](https://www.cnap.fr/n%C2%B020-graphisme-en-france-2014)
[Télécharger l'article](/pdf/013-PHILIZOT-design_graphique_et_metamorphoses_du_spectacle-2014.pdf)
