---
title: "« Visualization in Humanities and Social Sciences: Epistemic perspectives »"
description: "Introductory speaker at the 11<sup>th</sup> Japanese French Frontiers of Science Symposium (JFFoS2024), University of Strasbourg, May 24-27, 2024 (CNRS - JSPS)"
author: "Vivien Philizot"
date: 2024-01-01
years: "2024"
places: "Strasbourg"
objects: "conférence"
themes: ["images", "visualization", "science studies", "information studies"]
persons: ""
---

# « Visualization in Humanities and Social Sciences: Epistemic perspectives », Introductory speaker at the 11th Japanese French Frontiers of Science Symposium (JFFoS2024), University of Strasbourg, May 24-27, 2024 (CNRS - JSPS)

Comme le dit l’historien des sciences américain Matthew Norton Wise, « much of the history of science could be written in terms of making new things visible – or familiar things visible in a new way. » (Wise 2006, 75) Si une bonne partie de la science consiste à rendre visible ce qui est invisible, c’est parce qu’il existe un lien étroit entre la connaissance et les formes par lesquelles celle-ci est exprimée. Les liens de ce genre ont été beaucoup étudiés ces dernières années dans les domaines de l’histoire des sciences et des sciences studies. Ces contributions attestent d’un intérêt croissant pour la visualisation en science et la matérialité des représentations, souvent visuelles, qui médiatisent la connaissance. Cette communication vise à présenter les enjeux épistémologiques posés par la visualisation, lorsqu’ils émergent au croisement de la connaissance et de la matérialité. Il s’agira aussi de mettre en évidence ce que les humanités font à la visualisation, selon une approche qui emprunte aux visual culture studies, aux science studies et aux information studies.

<figure>
<img src="/images/084_Francis_Galton_Weather_Map_1861_1200x1672.jpg" alt="" width="1200" height="1672" loading="lazy">
<figcaption>Francis Galton, Weather Map, 1861</figcaption>
</figure>
