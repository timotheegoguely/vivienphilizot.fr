---
title: "<cite>Qu’est-ce qu’une image dans l’espace public ?</cite>"
description: "Éditions Deux-cent-cinq, Lyon, 2022. 84 p. ISBN : 9782919380626
"
author: "Vivien Philizot"
date: 2022-01-01
years: "2022"
places: "Lyon"
objects: "livre"
themes: ["design graphique", "espace public", "affiche"]
persons: ""
---

# <cite>Qu’est-ce qu’une image dans l’espace public ?</cite>, Éditions Deux-cent-cinq, Lyon, 2022. 84 p. ISBN : 9782919380626


La conceptualisation de l’espace public au XX<sup>e</sup> siècle a permis de mettre en évidence, comme le rappelle Thierry Paquot, la richesse et la diversité de cette notion polysémique : au singulier, l’espace public est le lieu du débat politique, l’espace de la démocratie ; au pluriel, ce sont les espaces physiques et symboliques dans lesquels et par lesquels les publics circulent et communiquent. La question principale qui oriente les réflexions développées dans cet ouvrage est située précisément à l’intersection de ces deux définitions. Elle s’accompagne de l’hypothèse qu’à cette intersection se tiennent des images, qui jouent un rôle de premier plan dans les pratiques sociales et les situations visuelles dont est fait notre quotidien. Une œuvre d’art dans l’espace urbain, une manifestation parsemée d’affiches, une publicité, un graphe ou une affiche institutionnelle… quelles sont les conséquences sur l’espace public et sur le débat démocratique de ces formes visuelles adressées publiquement au regard ?

<figure>
<img src="/images/074_Philizot_Image-dans-espace-public_06a.jpg" alt="" width="900" height="900" loading="lazy">
<img src="/images/074_Philizot_Image-dans-espace-public_06b.jpg" alt="" width="900" height="900" loading="lazy">
<img src="/images/074_Philizot_Image-dans-espace-public_06c.jpg" alt="" width="900" height="900" loading="lazy">
<img src="/images/074_Philizot_Image-dans-espace-public_06d.jpg" alt="" width="900" height="900" loading="lazy">
<img src="/images/074_Philizot_Image-dans-espace-public_06e.jpg" alt="" width="900" height="900" loading="lazy">
</figure>

[Lien vers la version en ligne](https://www.editions205.fr/collections/milieux/products/qu-est-ce-qu-une-image-dans-l-espace-public)
