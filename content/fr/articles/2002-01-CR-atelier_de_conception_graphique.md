---
title: "Atelier de conception graphique"
description: "Années 3 et 4, options Didactique visuelle et Communication graphique, Haute école des arts du Rhin (Hear). Licence 2 et 3 Arts, parcours Design, Faculté des Arts de l’Université de Strasbourg, de 2002 à 2018"
author: "Vivien Philizot"
date: 2002-01-01
years: "2002"
places: ["Haute école des arts du Rhin", "Strasbourg"]
objects: "cours"
themes: ["design graphique", "projet"]
persons: ""
---

# Atelier de conception graphique, Années 3 et 4, options Didactique visuelle et Communication graphique, Haute école des arts du Rhin (Hear). Licence 2 et 3 Arts, parcours Design, Faculté des Arts de l’Université de Strasbourg, de 2002 à 2018

Ces ateliers sont organisés autour de la pratique du design graphique, de la communication visuelle, du design d’information et de l’édition. Il s’agit de se familiariser avec la mise en page, l’organisation de ses contenus, le rapport texte-image, la hiérarchisation de l’information. L’accent est mis sur la lisibilités des formes, leur capacité à traduire un ensemble d’idées ou de notions de manière visuelle. Sont examinés un grand nombre d’exemples imprimés ou numériques, qui sont autant de cas pratiques permettant de comprendre les enjeux posés par la conception graphique.
