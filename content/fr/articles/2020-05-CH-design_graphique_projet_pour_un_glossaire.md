---
title: "« Design graphique. Projet pour un glossaire »"
description: "in Vivien Philizot et Jérôme Saint-Loubert (dir.), <cite>Technique et design graphique. Outils, médias, savoirs</cite>, Éditions B42, Paris, 2020, p.252-272."
author: "Vivien Philizot"
date: 2020-01-01
years: "2020"
places: "Paris"
objects: "chapitre d’ouvrage"
themes: ["glossaire", "design graphique", "boîte noire", "définition", "matériel", "milieu", "montage", "surface", "visibilité"]
persons: ""
---

# « Design graphique. Projet pour un glossaire », in Vivien Philizot et Jérôme Saint-Loubert (dir.), <cite>Technique et design graphique. Outils, médias, savoirs</cite>, Éditions B42, Paris, 2020, p.252-272.

Quel langage pourrait nous servir à appréhender la technique en design graphique sans la réduire à des recettes, à des règles ou à des lois générales ? Un glossaire est un recueil de gloses, une collection d’explications de termes liés à un domaine spécifique. C’est un moyen technique qui passe par la langue, glossa en grec, pour donner à comprendre un vocabulaire spécialisé. À l’échelle – locale – du livre depuis lequel il est entrepris, le glossaire réorganise le monde en le donnant à lire de manière fragmentaire à son lecteur. Il est souvent pensé comme un outil, ou un ensemble d’outils, dont on parie sur la dimension opératoire (l’outil est destiné à être employé, manipulé, pour produire un certain travail – ici un travail intellectuel). Les termes qui suivent – boîte noire, définition, matériel, milieu, montage, surface, visibilité – ont été rassemblés selon ce principe. Ce glossaire est à l’état de projet, à la manière d’une boîte à outils qu’il est toujours possible de compléter, de réorganiser, au gré des travaux que l’on voudra bien entreprendre.
