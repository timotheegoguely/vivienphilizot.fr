---
title: "« Helmo, une géologie du projet »"
description: "in Thomas Couderc, Jochen Gerner, Vivien Philizot (dir.), Clément Vauchez (dir.). <cite>Helmo. Design graphique, ping-pong et géologie</cite>, Université de Strasbourg, 2013, (Cahiers Design ; 3), p.6-10. "
author: "Vivien Philizot"
date: 2013-01-01
years: "2013"
places: "Strasbourg"
objects: "chapitre d’ouvrage"
themes: ["Helmo", "contrainte", "identité visuelle", "affiche", "design graphique"]
persons: ["Thomas Couderc", "Jochen Gerner", "Clément Vauchez"]
---

# « Helmo, une géologie du projet », in Thomas Couderc, Jochen Gerner, Vivien Philizot (dir.), Clément Vauchez (dir.). <cite>Helmo. Design graphique, ping-pong et géologie</cite>, Université de Strasbourg, 2013, (Cahiers Design ; 3), p.6-10. 

Ce texte est l’introduction du troisième numéro des Cahiers design, qui porte sur le travail du collectif Helmo. Si l’on entend souvent que le design est une opération visant à transformer les contraintes en opportunités, le déterminisme graphique qui sous tend le travail d’Helmo, empruntant à Georges Perec ou à Jorge Luis Borges, semble en premier lieu viser à réduire la part d’arbitraire de l’acte de création, moins motivé par la subjectivité de ses auteurs que par la nécessité des contextes de vie du projet.

[Télécharger l'article](/pdf/009-PHILIZOT-helmo_une_geologie_du_projet-2013.pdf)
