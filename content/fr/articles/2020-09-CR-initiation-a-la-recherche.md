---
title: "Initiation à la recherche"
description: "Licence 3 Arts, parcours Design, Faculté des Arts de l’Université de Strasbourg, à partir de 2020"
author: "Vivien Philizot"
date: 2020-01-01
years: "2020"
places: "Université de Strasbourg"
objects: "cours"
themes: ["recherche", "théorie", "enquête", "méthode de recherche", "culture visuelle"]
persons: ""
---

# Initiation à la recherche, Licence 3 Arts, parcours Design, Faculté des Arts de l’Université de Strasbourg, à partir de 2020

Ce cours vise à acquérir les fondamentaux de la culture scientifique universitaire au niveau licence. Il s’agit de maîtriser les différents aspects du travail de recherche et de la production scientifique en sciences humaines et plus spécifiquement en design ou dans les champs de recherche liés au design. Ce cours porte sur la méthodologie (pratiques de recherche, organisation du travail, écriture, élaboration d’un article scientifique, travail du terrain, analyses et cadres théoriques). Sont également abordés certains aspects de l’épistémologie de la recherche en design. Dans ce sens, il s’agira notamment de situer la recherche en design, en arts, en architecture, dans le voisinage de l’histoire de l’art, des études visuelles et culturelles ou des sciences de l’information et de la communication.
