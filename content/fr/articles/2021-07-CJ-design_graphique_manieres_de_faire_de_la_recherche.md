---
title: "Design graphique. Manières de faire de la recherche"
description: "Journée d’étude, Centre Georges Pompidou, et Bibliothèque Kandinsky. Paris, 17 décembre 2021. Co-produit par l’Université de Strasbourg et le Centre national des arts plastiques. Co-organisation avec Nolwenn Maudet (UR 3402) et Véronique Marrier."
author: "Nolwenn Maudet, Vivien Philizot, Véronique Marrier"
date: 2021-01-01
years: "2021"
places: ["Centre Georges Pompidou", "Bibliothèque Kandinsky", "Paris"]
objects: "colloque et journée d’études"
themes: ["design graphique", "recherche", "réflexivité"]
persons: ["Véronique Marrier", "Nolwenn Maudet", "Yann Aucompte", "Julie Blanc", "Max Bonhomme", "Corentin Brulé", "Léonore Conte", "Jérémy De Barros", "Sébastien Dégeilh", "Catherine de Smet", "Pierre Fournier", "Catherine Geel", "Thomas Huot-Marchand", "Olivier Huz", "Annick Lantenois", "Vincent Maillard", "Nolwenn Maudet", "Manon Ménard", "Éloïsa Pérez", "Vivien Philizot", "Lorène Picard", "Gilles Rouffineau", "Fabrice Sabatier"]
---

# Design graphique. Manières de faire de la recherche, Journée d’étude, Centre Georges Pompidou, et Bibliothèque Kandinsky. Paris, 17 décembre 2021. Co-produit par l’Université de Strasbourg et le Centre national des arts plastiques. Co-organisation avec Nolwenn Maudet (UR 3402) et Véronique Marrier.

Le champ du design graphique est marqué depuis maintenant quelques années par un intérêt grandissant pour la recherche. Cet intérêt se traduit à plusieurs échelles : au niveau de l’enseignement dans les écoles d’art et les universités (qui associe de mieux en mieux la théorie et le projet), au niveau de la pratique (qui gagne en réflexivité et en recul critique) et au niveau de la recherche elle-même (qui s’enrichit de connaissances de  plus en plus variées). Cette journée d’études vise à interroger l’existence et la nature de « modes de connaissance propres », ou de manières spécifiques de faire de la recherche en design graphique. Comment celles-ci se sont- elles développées ces dernières années, tout en ménageant des points de passage et des ouvertures vers les disciplines voisines ? Organisation : Véronique Marrier, Nolwenn Maudet, Vivien Philizot, avec la collaboration de Mica Gherghescu et Victor Guégan. Intervenant·e·s : Yann Aucompte, Julie Blanc, Max Bonhomme, Corentin Brulé, Léonore Conte, Jérémy De Barros, Sébastien Dégeilh, Catherine de Smet, Pierre Fournier, Catherine Geel, Thomas Huot-Marchand, Olivier Huz, Annick Lantenois, Vincent Maillard, Nolwenn Maudet, Manon Ménard, Éloïsa Pérez, Vivien Philizot, Lorène Picard, Gilles Rouffineau, Fabrice Sabatier 

[Lien vers la version en ligne](https://www.culturesvisuelles.org/champs-de-recherche/epistemologie-des-images-et-du-design-graphique/design-graphique-manieres-de-faire-de-la-recherche)
