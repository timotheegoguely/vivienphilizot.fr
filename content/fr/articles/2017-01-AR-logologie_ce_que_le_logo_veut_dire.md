---
title: "« Logologie. Ce que le logo veut dire »"
description: "in <cite>Graphisme en France</cite> 2017, Centre national des Arts plastiques, 2017, p.15-23."
author: "Vivien Philizot"
date: 2017-01-01
years: "2017"
places: "Paris"
objects: "article"
themes: ["logotype", "identités visuelles", "discours", "langages graphiques", "idéologie", "Paul Rand"]
persons: ""
---

# « Logologie. Ce que le logo veut dire », in <cite>Graphisme en France</cite> 2017, Centre national des Arts plastiques, 2017, p.15-23.

Le logotype est un objet qui tire ses fonctions particulières de ses propriétés visuelles, de sa nature fondamentalement iconique. La place de ces signes dans notre histoire, leur rôle dans notre économie et notre culture, leur fonction politique, nous invitent à dénouer les deux fils du discours et de l’image, du <cite>logos</cite> et du <cite>tupos</cite>, pour comprendre comment leur enchevêtrement a pu tisser progressivement les formes que nous leur connaissons. Quel rôle nos systèmes visuels jouent-il réellement dans notre environnement immédiat ? De quoi les investissons nous au quotidien ? De quelle manière participent-il à la construction sociale et culturelle de notre environnement visuel ? La « logologie », littéralement « science du discours », doublant avec un peu d’ironie sa racine grecque, peut aussi s’entendre comme une science du logotype, si du moins l’on retient que la manière tout à fait visuelle dont ce dernier se présente à nous, reste fondamentalement redoublée, contrariée ou renforcée – selon les cas – par la logique propre du discours. Au double sens de logotype et de discours, qu’est ce que le <cite>logos</cite> peut bien vouloir dire ?

<figure>
<img src="/images/030_Vivien_Philizot_Logologie-1200x773.jpg" alt="" width="1200" height="773" loading="lazy">
<figcaption>Vivien Philizot, « Logologie. Ce que le logo veut dire » - « Logology. What a logo means »</figcaption>
</figure>

[Lien vers la version en ligne](https://www.cnap.fr/graphisme-en-france-n%C2%B023-logos-identites-visuelles-2017)
[Télécharger l'article](/pdf/030-PHILIZOT-logologie_ce_que_le_logo_veut_dire-2017.pdf)
