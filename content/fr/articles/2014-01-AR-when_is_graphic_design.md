---
title: "« When is Graphic design ? Quelques remarques nominalistes sur la définition d’une discipline »"
description: "in <cite>Tombolo</cite>, [en ligne] 2014. /www.t-o-m-b-o-l-o.eu/meta/when-is-grahic-design-quelques-remarques-nominalistes-sur-la-definition-dune-discipline-2/ republié dans Collectif, <cite>Graphisme et transmission</cite>, Alliance graphique internationale, 2015, p.28-31."
author: "Vivien Philizot"
date: 2014-01-01
years: "2014"
places: ""
objects: "article"
themes: ["design graphique", "définitions", "pragmatisme", "Nelson Goodman", "contexte"]
persons: ""
---

# « When is Graphic design ? Quelques remarques nominalistes sur la définition d’une discipline », in <cite>Tombolo</cite>, [en ligne] 2014. /www.t-o-m-b-o-l-o.eu/meta/when-is-grahic-design-quelques-remarques-nominalistes-sur-la-definition-dune-discipline-2/ republié dans Collectif, <cite>Graphisme et transmission</cite>, Alliance graphique internationale, 2015, p.28-31.

La définition du design graphique comme champ de pratique autonome est sujette à de nombreux débats, bien alimentés depuis qu’en 2010, le Festival international de graphisme de Chaumont proposait comme sujet de son concours annuel, la question « Le graphisme qu’est-ce que c’est ? ». Il s’agit ici d’étudier, pour le compte du design graphique, les conséquences du changement de paradigme connu dans le champ artistique au XX<sup>e</sup> siècle, et que Nelson Goodman avait conceptualisé dans un article très médiatisé, en proposant de substituer à « Qu’est ce que l’art ? » la question « Quand y a-t-il art ? » (<cite>When is Art?</cite>).

[Télécharger l'article](/pdf/011-PHILIZOT-when_is_graphic_design-2014.pdf)
