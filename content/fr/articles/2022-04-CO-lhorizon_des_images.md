---
title: "« L’horizon des images »"
description: "Communication dans le cadre du colloque « Horizons contemporains : ouverture, partition, profondeur d’espaces dans les arts de la scène et de l’écran », organisé par Aurélie Coulon et Benjamin Thomas, Université de Strasbourg, 28 juin 2022 "
author: "Vivien Philizot"
date: 2022-01-01
years: "2022"
places: "Strasbourg"
objects: "communication"
themes: ["Horizon", "images", "perception", "représentation", "remédiation", "critique de la représentation télévisuelle"]
persons: ["Aurélie Coulon", "Benjamin Thomas"]
---

# « L’horizon des images, Communication dans le cadre du colloque « Horizons contemporains : ouverture, partition, profondeur d’espaces dans les arts de la scène et de l’écran », organisé par Aurélie Coulon et Benjamin Thomas, Université de Strasbourg, 28 juin 2022 

Le film de Peter Weir, <cite>The Truman Show</cite>, sorti en salles en 1998, met en scène un horizon dystopique en livrant une critique de la représentation médiatique qui caractérise plusieurs productions cinématographiques américaines de cette fin des années 1990. Or cette fin de décennie est aussi marquée aux États-Unis par l’apogée du débat sur la visual culture, qui invite à observer l’horizon de ces écrans cinématographiques et télévisuels à la lumière des réflexions sur l’image développées dans les champs de la philosophie anglo-saxonne et des études visuelles. Si la remédiation télévisuelle au cinéma implique de faire une distinction entre l’image de l’horizon et l’horizon des images, il s’agira de mettre cette distinction au travail, en tentant de dégager ce que l’horizon, comme objet et outil théorique, peut apporter à notre compréhension des images.
