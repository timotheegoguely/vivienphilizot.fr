---
title: "Exposition « 48°34’52” – 7°45’33” – une cartographie illustrée de la didactique visuelle »"
description: "Co-commissariat avec Olivier Poncer et Guy Meyer, Galerie La Chaufferie, Haute école des arts du Rhin, Strasbourg, du 11 décembre 2009 au 11 janvier 2010."
author: "Guy Meyer, Vivien Philizot, Olivier Poncer"
date: 2010-01-01
years: "2010"
places: ["Haute école des arts du Rhin", "Strasbourg"]
objects: "exposition"
themes: ["didactique visuelle", "pédagogie", "sciences de l’éducation", "savoirs visuels"]
persons: ["Olivier Poncer", "Guy Meyer"]
---

# Exposition « 48°34’52” – 7°45’33” – une cartographie illustrée de la didactique visuelle », Co-commissariat avec Olivier Poncer et Guy Meyer, Galerie La Chaufferie, Haute école des arts du Rhin, Strasbourg, du 11 décembre 2009 au 11 janvier 2010.

Cette exposition vise à donner à comprendre l’étendue du champ de recherche et de pratiques couvert par la didactique visuelle. À la frontière du design graphique, des sciences cognitives, des sciences de l’éducation, de la médiation scientifique et culturelle et de l’illustration, la didactique visuelle procède d’une lecture vigilante du monde des signes visuels qui nous entoure et des processus de construction et transmission des connaissances par l’image.
