---
title: "« Quelle recherche en design graphique ? »"
description: "Conférence dans le cadre du cycle « Design Arts Médias », organisé par Catherine Chomarat-Ruiz, Paris I Panthéon Sorbonne, 20 janvier 2021."
author: "Vivien Philizot"
date: 2021-01-01
years: "2021"
places: ["Paris I Panthéon Sorbonne", "Paris"]
objects: "conférence"
themes: ["objets graphiques", "médiations visuelles", "environnements graphiques", "Bruno Latour"]
persons: ["Catherine Chomarat-Ruiz"]
---

# « Quelle recherche en design graphique ? », Conférence dans le cadre du cycle « Design Arts Médias », organisé par Catherine Chomarat-Ruiz, Paris I Panthéon Sorbonne, 20 janvier 2021.

Cette conférence porte sur les nouvelles manières d’envisager les objets de notre environnement visuel comme des médias graphiques, et donc comme des formes par lesquelles l’action est systématiquement déléguée, transférée, prolongée vers autre chose. Il s’agit d’enquêter sur les médiations, c’est-à-dire sur que font ces objets, plutôt que sur ce qu’ils sont, en se rendant attentif aux contextes, aux situations dans lesquels ils ont un rôle à tenir. Il s’agit aussi, en empruntant au programme de la sociologie pragmatique, de resserrer l’enquête sur les épreuves : ces situations où la médiation et la représentation sont en crise, où la délégation ne produit pas ce qui est attendu. À la suite des travaux de Bruno Latour sur les « inscriptions » (qui ne sont pas autre chose que des objets graphiques en contexte scientifique), une recherche de ce genre ouvre des perspectives fécondes sur le rôle social du design graphique.
