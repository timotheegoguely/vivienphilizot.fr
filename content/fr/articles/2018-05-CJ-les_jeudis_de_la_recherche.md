---
title: "Les jeudis de la recherche. Pratique, théorie, épistémologie de la recherche en Arts"
description: "Journée d’études. Faculté des Arts de l’Université de Strasbourg. Palais universitaire, 3 et 4 mai 2018. Co-organisée avec les membres de l’équipe Approches contemporaines de la création et de la réflexion artistiques (ACCRA - UR 3402)."
author: ""
date: 2018-01-01
years: "2018"
places: "Université de Strasbourg"
objects: "colloque et journée d’études"
themes: ["art", "pratique", "théorie", "épistémologie de la recherche"]
persons: ""
---

# Les jeudis de la recherche. Pratique, théorie, épistémologie de la recherche en Arts, Journée d’études. Faculté des Arts de l’Université de Strasbourg. Palais universitaire, 3 et 4 mai 2018. Co-organisée avec les membres de l’équipe Approches contemporaines de la création et de la réflexion artistiques (ACCRA - UR 3402).

L’objectif de ces journées est de croiser différentes recherches pluridisciplinaires et de faire un état des lieux de la recherche en Arts et en Design à l’Université de Strasbourg. Inscrit dans les événements scientifiques de l’Unité de recherche Approches contemporaines de la création et de la réflexion artistiques, plus précisément de l’Axe 1 « Politique des Arts », ces interventions programmés sur deux journées abordent des questions méthodologiques et épistémologiques. Qu’est-ce qui définit une recherche en Art, en Arts visuels ? Quelles méthodes et quels cadres perceptifs sont convoqués et utilisés ? Selon la diversité des recherches en Art quelles sont les influences des disciplines académiques historiques ? Intervenant·e·s : Grazia Giacco, Marie-Odile Biry-Fétique, Stephane Mroczkowski, Maxime Favard, Gwenaëlle Bertrand, Anais Bernard, Janig Begoc, Chiara Palermo, Denis Steinmetz, Corine Pencenat, Maxime Boidy, Vivien Philizot, Sophie Suma
