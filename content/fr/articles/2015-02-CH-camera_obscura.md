---
title: "« Camera obscura. Le design graphique, entre construction sociale du champ visuel et construction visuelle du champ social »"
description: "in Gwenaëlle Bertrand et Maxime Favard (dir.), <cite>Poïétiques du design - Conception et politique</cite>, L’Harmattan, Esthétiques série Ars, Paris, 2015, p.33-55."
author: "Vivien Philizot"
date: 2015-01-01
years: "2015"
places: "Paris"
objects: "chapitre d’ouvrage"
themes: ["transparence", "opacité", "idéologie", "graphisme et politique", "Beatrice Warde", "John Carpenter"]
persons: ""
---

# « Camera obscura. Le design graphique, entre construction sociale du champ visuel et construction visuelle du champ social », in Gwenaëlle Bertrand et Maxime Favard (dir.), <cite>Poïétiques du design - Conception et politique</cite>, L’Harmattan, Esthétiques série Ars, Paris, 2015, p.33-55.

Ce texte interroge la dimension politique du design graphique par la mise en rapport de la métaphore du verre en cristal imaginée par Beatrice Warde dans les années 1930 et du film de John Carpenter, <cite>They Live</cite> (1988). Il s’agit plus précisément d’aborder cette contradiction séminale propre à l’invention, à l’époque moderne, du design graphique comme dispositif de mise à distance du monde et des choses du monde, et simultanément, comme négation de cette mise à distance. Les effets politiques du design graphique sont alors moins à chercher dans les éclatantes manifestations de la propagande au XX<sup>e</sup> siècle, que dans la banalité de cet acte quotidien qui consiste à incorporer les conditions du voir au voir lui-même.
