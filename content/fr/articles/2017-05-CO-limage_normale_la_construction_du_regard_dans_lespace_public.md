---
title: "« L’image normale. La construction du regard dans l’espace public »"
description: "Communication dans le cadre de la journée d’étude « Regards sur le paysage urbain », organisée par Sophie Suma et Lise Lerichomme (ACCRA - UR 3402, AMUP - EA 7309), Maison Interuniversitaire des Sciences de l’Homme-Alsace Université de Strasbourg, 25 mai 2017."
author: "Vivien Philizot"
date: 2017-01-01
years: "2017"
places: "Université de Strasbourg"
objects: "communication"
themes: ["images", "norme", "standard", "design graphique", "espace public"]
persons: ["Sophie Suma", "Lise Lerichomme"]
---

# « L’image normale. La construction du regard dans l’espace public », Communication dans le cadre de la journée d’étude « Regards sur le paysage urbain », organisée par Sophie Suma et Lise Lerichomme (ACCRA - UR 3402, AMUP - EA 7309), Maison Interuniversitaire des Sciences de l’Homme-Alsace Université de Strasbourg, 25 mai 2017.

Quel est la fonction normative de l’image dans la construction du paysage urbain ? On pourrait dire dans un premier temps que l’image normale est une image morale au sens ou elle semble bien incarner cette pratique sociale moyenne dont parlait Henri Lefebvre, en lui donnant forme et en la rendant visible. Bien-sûr les notions de norme, de normalisation, de standard et de type, sont au centre des préoccupations des concepteurs d’images dans le champ du design et du design graphique depuis le début du XX<sup>e</sup> siècle. Il s’agit ici de se livrer à une exploration lacunaire de l’image normale dans le paysage urbain, en évoquant des exemples qui, des publicités des années 1960 au catalogue Ikea, sont bien devenus une composante essentielle de notre horizon visuel.
