---
title: "« Le signe typographique et le mythe de la neutralité »"
description: "in <cite>Textimage</cite>, revue d’étude du dialogue texte-image [en ligne], 3/2009. www.revue-textimage.com/04_a_la_lettre/philizot1.html"
author: "Vivien Philizot"
date: 2009-01-01
years: "2009"
places: ""
objects: "article"
themes: ["typographie", "histoire", "modernité", "avant-gardes", "Adrian Frutiger", "Max Miedinger"]
persons: ""
---

# « Le signe typographique et le mythe de la neutralité », in <cite>Textimage</cite>, revue d’étude du dialogue texte-image [en ligne], 3/2009. www.revue-textimage.com/04_a_la_lettre/philizot1.html

Il s’agit ici d’étudier l’idée de neutralité dans le champ de la typographie et d’identifier le mythe dont elle fait l’objet à travers le discours dominant dans la production actuelle. Le processus de rationalisation à l’œuvre des débuts de l’imprimerie jusqu’à l’apparition de certaines formes typographiques contemporaines peut se retracer au travers d’une tradition typographique qui semble s’être construite dans une progressive élimination des traces de sa propre histoire. Alors que la typographie des années 1920 se propose de formuler un prototype idéal nourri des utopies des avant-gardes, les recherches fonctionnalistes de l’après-guerre traduisent une autre idée de la neutralité, représentée entre autres par deux caractères très légitimes : l’Helvetica et l’Univers. Accompagné des crispations produites par le contexte socio-économique de la période moderniste, le mythe du neutre est aujourd’hui revisité par la création contemporaine.

[Lien vers la version en ligne](https://www.revue-textimage.com/04_a_la_lettre/philizot1.html)
