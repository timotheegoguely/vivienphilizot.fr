---
title: "« Quelques éclairages non techniques sur la technique »"
description: "in Vivien Philizot et Jérôme Saint-Loubert (dir.), <cite>Technique et design graphique. Outils, médias, savoirs</cite>, Éditions B42, Paris, 2020, p.23-31."
author: "Vivien Philizot"
date: 2020-01-01
years: "2020"
places: "Paris"
objects: "chapitre d’ouvrage"
themes: ["design graphique", "technique", "histoire des techniques", "médiation technique"]
persons: ""
---

# « Quelques éclairages non techniques sur la technique », in Vivien Philizot et Jérôme Saint-Loubert (dir.), <cite>Technique et design graphique. Outils, médias, savoirs</cite>, Éditions B42, Paris, 2020, p.23-31.

Notre environnement visuel est traversé de part en part par la technique. Pourtant, ces opérations sont bien souvent maintenues dans l’ombre de questions plus nobles portant sur les graphistes, leurs démarches et les formes qu’ils produisent. Cette introduction à l’ouvrage collectif <cite>Technique et design graphique</cite>, en présente les différentes contributions qui montrent selon plusieurs éclairages complémentaires, que la technique n’est pas réductible à des opérations quantifiées ou à des objets fonctionnels, mais qu’elle revêt plus largement une dimension anthropologique beaucoup plus ancienne et profonde que ce que nos environnements technologiques ne laissent imaginer. Cet ouvrage témoigne de la multiplicité des approches possibles sur le sujet ainsi que de la fertilité d’une thématique qui reste encore largement à défricher, au croisement du design, des études visuelles, et des humanités numériques. 
