---
title: "« Kodak’s Colorama and the Construction of the Gaze in Public Space »"
description: "in <cite>Articulo</cite>, Journal of Urban Research [en ligne], 19/2019. https://journals.openedition.org/articulo/3975"
author: "Vivien Philizot"
date: 2019-01-01
years: "2019"
places: "New York"
objects: "article"
themes: ["images", "études visuelles", "photographie", "paysage urbain", "représentation", "dispositif", "selfie", "idéologie", "Colorama", "gare de New York"]
persons: ""
---

# « Kodak’s Colorama and the Construction of the Gaze in Public Space », in <cite>Articulo</cite>, Journal of Urban Research [en ligne], 19/2019. https://journals.openedition.org/articulo/3975

Entre les années 1950 et 1989, la société Kodak occupe en continu un espace de près d’une centaine de mètres carrés dans la gare centrale de New York sous la forme de photographies panoramiques changées régulièrement. En étudiant la généalogie de ce projet, il s’agit de comprendre ce que la mise en scène du point de vue dans l’espace de la ville nous enseigne sur la construction sociale du regard. L’héritage critique des visual studies peut nous aider à reconsidérer cette discipline consacrée au travail de la représentation : le design graphique.

<figure>
<img src="/images/046_Colorama_New-York-Central-Station.jpg" alt="" width="1200" height="783" loading="lazy">
<figcaption>Colorama, New York Central Station, non datée © George Eastman House</figcaption>
</figure>

[Lien vers la version en ligne](https://journals.openedition.org/articulo/3975)
[Télécharger l'article](/pdf/046-PHILIZOT-kodaks_colorama_and_the_construction_of_the_gaze_in_public_space-2019.pdf)
