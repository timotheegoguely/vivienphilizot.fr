---
title: "« Formes du design et construction subjective du monde social »"
description: "in <cite>Figures de l’Art</cite>, Presses universitaires de Pau, 25/2013, p.365-381."
author: "Vivien Philizot"
date: 2013-01-01
years: "2013"
places: ""
objects: "article"
themes: ["design", "expérience", "pratique", "usages", "langage"]
persons: ""
---

# « Formes du design et construction subjective du monde social », in <cite>Figures de l’Art</cite>, Presses universitaires de Pau, 25/2013, p.365-381.

Fondé sur un aller-retour entre projet et expérience, le design est une activité fondamentalement contextuelle, semblant à ce titre partager certaines propriétés avec le langage. Il est alors possible de tracer des correspondances entre le milieu linguistique et notre relation au monde, en tant qu’elle passe, non pas par les objets eux-mêmes, mais par la pratique des objets que le design a en charge de faire exister. Car c’est bien cette notion de pratique, dans son acception linguistique et anthropologique qui semble déterminer la possibilité pour l’objet de faire sens, et donc son appartenance même à cette catégorie d’« objet de design » si difficile à circonscrire.

[Lien vers la version en ligne](https://www.persee.fr/doc/fdart_1265-0692_2013_num_25_1_1616)
[Télécharger l’article](/pdf/007-PHILIZOT-formes_du_design_et_construction_subjective_du_monde_social-2013.pdf)
