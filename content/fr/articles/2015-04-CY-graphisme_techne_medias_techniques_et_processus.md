---
title: "Graphisme - Technè. Médias, techniques et processus"
description: "ACCRA - UR3402, Université de Strasbourg, Haute école des arts du Rhin et Musées de Strasbourg. Novembre 2015 à avril 2016."
author: ""
date: 2015-01-01
years: "2015"
places: "Strasbourg"
objects: "cycle de conférences"
themes: ["design graphique", "technique", "médias"]
persons: ["Jérôme Saint-Loubert-Bié", "Philippe Delangle", "Loïc Horellou", "Yohanna Nguyen", "Jean-Noël Lafargue", "Indra Kupferschmid", "Sarah Garçin", "Kévin Donnot", "Bruno Bachimont", "Alaric Garnier", "Douglas Stanley", "Anthony Masure", "Nicolas Thély"]
---

# Graphisme - Technè. Médias, techniques et processus, ACCRA - UR3402, Université de Strasbourg, Haute école des arts du Rhin et Musées de Strasbourg. Novembre 2015 à avril 2016.

Si le design graphique a souvent été réduit à ses aspects formels et technologiques, il s’agit à l’occasion de ce cycle de conférences, de prendre ces notions à rebours, en les mettant en perspective avec les déterminants historiques et culturels qui les traversent. Il est alors possible de s’interroger sur des processus aussi différents que la démocratisation et la standardisation des outils, la mobilisation des savoirs dans les médias, la perte du monopole technique autrefois détenu par les designers, les métamorphoses économiques qui accompagnent les processus techniques initiés par le design graphique. Co-organisation Jérôme Saint-Loubert-Bié, Philippe Delangle, Loïc Horellou et Yohanna Nguyen. Intervenant·e·s Jean-Noël Lafargue, Indra Kupferschmid, Sarah Garçin, Kévin Donnot, Bruno Bachimont, Alaric Garnier, Douglas Stanley, Anthony Masure, Nicolas Thély.
