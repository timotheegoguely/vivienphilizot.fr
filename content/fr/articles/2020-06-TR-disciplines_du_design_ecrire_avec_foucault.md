---
title: "« Disciplines du design. Écrire avec Foucault »"
description: "Traduction de l’anglais de Lupton, Ellen. 1996. « Disciplines of Design. Writing with Foucault ». In <cite>Design Writing Research. Writing on Graphic Design</cite>, par Ellen Lupton et J. Abbott Miller, 66‑70. New York: Kiosk. "
author: "Vivien Philizot, Nolwenn Maudet"
date: 2020-01-01
years: "2020"
places: ""
objects: "traduction"
themes: ["Ellen Lupton", "Michel Foucault", "design", "disciplines", "épistémologie du design", "Archéologie du savoir"]
persons: ["Nolwenn Maudet"]
---

# « Disciplines du design. Écrire avec Foucault », Traduction de l’anglais de Lupton, Ellen. 1996. « Disciplines of Design. Writing with Foucault ». In <cite>Design Writing Research. Writing on Graphic Design</cite>, par Ellen Lupton et J. Abbott Miller, 66‑70. New York: Kiosk. 

Publié dans <cite>Graphisme en France</cite>, Centre national des Arts plastiques, 2020, p.32-36. Avec Nolwenn Maudet.
