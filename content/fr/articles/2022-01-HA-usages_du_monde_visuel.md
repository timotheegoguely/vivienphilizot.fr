---
title: "« Usages du monde visuel »"
description: "Habilitation à diriger des recherches. Garante : Catherine de Smet. Soutenue à l’Université Paris VIII Vincennes - Saint-Denis, le 4 février 2022. Jury : Catherine Chomarat-Ruiz, Yves Citton, Catherine de Smet, Nathalie Delbard, Valérie Devillard"
author: "Vivien Philizot"
date: 2022-01-01
years: "2022"
places: ["Université Paris 8", "Vincennes Saint-Denis"]
objects: "habilitation"
themes: ["design graphique", "épistémologie", "images", "études visuelles"]
persons: ["Catherine Chomarat-Ruiz", "Yves Citton", "Catherine de Smet", "Nathalie Delbard", "Valérie Devillard"]
---

# « Usages du monde visuel », Habilitation à diriger des recherches. Garante : Catherine de Smet. Soutenue à l’Université Paris VIII Vincennes - Saint-Denis, le 4 février 2022. Jury : Catherine Chomarat-Ruiz, Yves Citton, Catherine de Smet, Nathalie Delbard, Valérie Devillard

Cette habilitation à diriger des recherches se compose de trois volumes : un mémoire de synthèse (138 p.), un inédit, dont le titre est Images premières. Aux origines de la représentation visuelle (266 p.), et un recueil de travaux (522 p.). Au croisement des champs du design graphique et des études visuelles, ma démarche de recherche consiste à appréhender les objets graphiques dans la perspective ouverte par les média studies et au-delà, par des travaux sur les images, qui, de Louis Marin ou à Emmanuel Alloa, se sont attachés à mettre en évidence, dans ses diverses formes, le paradoxe de la médiation visuelle. Ce travail est abondamment nourri par les études visuelles et plus précisément par l’iconologie de WJT Mitchell, dont la reformulation critique passe par une confrontation avec l’idéologie. Cette habilitation trace par ailleurs les contours d’un cadre conceptuel plus vaste, en situant mes réflexions dans une tradition philosophique largement teintée par le pragmatisme et ses figures historiques, Charles Sanders Peirce, William James et John Dewey, jusqu’aux travaux plus récents de Nelson Goodman et de Richard Shusterman. Il s’agit, pour reprendre le titre général, d’étudier les usages du monde visuel. Qu’est-ce que produit cette opération particulière et cependant triviale, ordinaire, et quotidienne, qu’est la médiation visuelle ? Comment cette opération transforme-t-elle les rapports entre les sujets qu’elle engage ? Comment prend-elle forme dans des images ou des situations visuelles, qui ne sont jamais autre chose que des rapports sociaux incarnés ou embarqués par des choses ? Observer les usages du monde visuel, c’est étudier, de manière indirecte, quelque chose qui passe par les images, tout en recueillant au passage les bénéfices épistémologiques de ce détour. Cette habilitation vise à donner corps à de nouvelles manières d’envisager la recherche sur les images et la culture visuelle contemporaine, d’enrichir notre compréhension des pratiques graphiques en dépassant l’horizon polysémique du design, pour engager une réflexion élargie sur le monde visuel.

<figure>
<img src="/images/073_Philizot-Habilitation_1200x1200_a.jpg" alt="" width="1200" height="1200" loading="lazy">
<img src="/images/073_Philizot-Habilitation_1200x1200_b.jpg" alt="" width="1200" height="1200" loading="lazy">
<img src="/images/073_Philizot-Habilitation_1200x1200_c.jpg" alt="" width="1200" height="1200" loading="lazy">
<img src="/images/073_Philizot-Habilitation_1200x1200_d.jpg" alt="" width="1200" height="1200" loading="lazy">
<img src="/images/073_Philizot-Habilitation_1200x1200_e.jpg" alt="" width="1200" height="1200" loading="lazy">
<img src="/images/073_Philizot-Habilitation_1200x1200_f.jpg" alt="" width="1200" height="1200" loading="lazy">
<img src="/images/073_Philizot-Habilitation_1200x1200_g.jpg" alt="" width="1200" height="1200" loading="lazy">
<figcaption>Vivien Philizot, Habilitation à diriger des recherches, 2022</figcaption>
</figure>
