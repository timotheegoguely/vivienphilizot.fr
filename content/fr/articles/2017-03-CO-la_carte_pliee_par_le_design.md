---
title: "« La carte pliée par le design. Épistémologie de la carte, du diagramme et de la métaphore »"
description: "Communication dans le cadre du colloque « Places en relation », organisé par Ruedi et Vera Baur, Théâtre Saint-Gervais, Genève, 7 octobre 2017."
author: "Vivien Philizot"
date: 2017-01-01
years: "2017"
places: ["Théâtre Saint-Gervais", "Genève"]
objects: "communication"
themes: ["diagramme", "métaphore", "carte", "design graphique"]
persons: ["Ruedi Baur", "Vera Baur"]
---

# « La carte pliée par le design. Épistémologie de la carte, du diagramme et de la métaphore », Communication dans le cadre du colloque « Places en relation », organisé par Ruedi et Vera Baur, Théâtre Saint-Gervais, Genève, 7 octobre 2017.

Ces objets de construction des savoirs que sont la carte, le diagramme et la métaphore ont quelque chose en commun, si nous nous rappelons que Charles Sanders Peirce les envisageait comme des icônes, ces signes qui entretiennent avec leur objet un rapport d’analogie. Il s’agit ici d’étudier cette propriété singulière dans différentes occurrences de ces objets, tout en les rapportant aux champs du design et de la conception graphique.
