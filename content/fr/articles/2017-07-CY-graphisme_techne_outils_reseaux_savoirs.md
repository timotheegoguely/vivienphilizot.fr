---
title: "Graphisme - Technè. Outils, réseaux, savoirs"
description: "ACCRA - UR3402, Université de Strasbourg, Haute école des arts du Rhin et Musées de Strasbourg. Novembre 2017 à avril 2018."
author: ""
date: 2017-01-01
years: "2017"
places: "Strasbourg"
objects: "cycle de conférences"
themes: ["design graphique", "techniques", "outils"]
persons: ["Sandra Chamaret", "Jérôme Saint-Loubert-Bié", "Philippe Delangle", "Loïc Horellou", "Yohanna Nguyen", "Kristyan Sarkis", "Pauline Thomas", "Max Bonhomme", "Etienne Robial", "David Bennewith", "Julien Priez", "Fabrice Sabatier", "Pierre-Damien Huyghe", "Pierre Ponant"]
---

# Graphisme - Technè. Outils, réseaux, savoirs, ACCRA - UR3402, Université de Strasbourg, Haute école des arts du Rhin et Musées de Strasbourg. Novembre 2017 à avril 2018.

Les différentes contributions de ce cycle de conférences viennent prolonger les réflexion du premier cycle, en tentant de montrer que la technique n’est pas ce qui sert mécaniquement une activité, pas plus qu’elle ne se réduit à des outils, des recettes ou des programmes. Elle est bien plutôt ce qui croise des pratiques, des normes, des habitudes et des « manières de faire », c’est-à-dire tout une somme de choses qui font partie intégrante des processus de conception sans forcément y apparaître explicitement. Co-organisation Sandra Chamaret, Jérôme Saint-Loubert-Bié, Philippe Delangle, Loïc Horellou et Yohanna Nguyen. Intervenants Kristyan Sarkis, Pauline Thomas, Max Bonhomme, Etienne Robial, David Bennewith, Julien Priez, Fabrice Sabatier, Pierre-Damien Huyghe, Pierre Ponant.
