---
title: "Cultures visuelles - Plateforme numérique de diffusion de la recherche - culturesvisuelles.org"
description: "ACCRA - UR3402, Université de Strasbourg."
author: "Vivien Philizot, Sophie Suma"
date: 2021-01-01
years: "2021"
places: ""
objects: "projet de recherche"
themes: ["cultures visuelles", "recherche"]
persons: ["Sophie Suma", "Nolwenn Maudet", "Simon Zara", "Sarah Calba", "Timothée Goguely", "Adrien Payet"]
---

# Cultures visuelles - Plateforme numérique de diffusion de la recherche - culturesvisuelles.org, ACCRA - UR3402, Université de Strasbourg.

La plateforme numérique Cultures visuelles est un espace numérique d’échanges, de production et de diffusion du travaux de recherche liés aux savoirs sur l’image et à la culture visuelle. Cette plateforme associe une démarche scientifique collective à une réflexion sur la forme et sur les outils visuels et textuels, en nourrissant la réflexion grâce aux apports des humanités numériques, du design graphique et des études visuelles. La plateforme est envisagée comme un espace de réflexion et d’expérimentation sur les nouveaux outils de travail collaboratif et sur les modes de diffusion et de publication de la recherche. Elle est conçue comme un dispositif d’écriture singulier, qui permet à plusieurs chercheur·se·s éloigné·es dans l’espace de collaborer sur un même projet et de diffuser en ligne leurs contenus. L’élaboration de cette plateforme est aussi l’opportunité de proposer au public un objet culturel didactique, offrant, à quiconque s’y intéresse, une expérience exploratoire pour comprendre le rôle des images et des cultures visuelles dans notre environnement. Conception : Sophie Suma, Nolwenn Maudet, Simon Zara, Sarah Calba. Développement : Timothée Goguely et Adrien Payet.

<figure>
<img src="/images/069-Cultures-visuelles_1200x953.jpg" alt="" width="1200" height="953" loading="lazy">
</figure>

[Lien vers la plateforme](https://www.culturesvisuelles.org/)
