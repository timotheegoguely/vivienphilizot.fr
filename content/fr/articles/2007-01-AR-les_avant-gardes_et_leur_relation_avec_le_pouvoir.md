---
title: "« Les avant-gardes et leur relation avec le pouvoir dans le champ du graphisme  et de la typographie »"
description: "in <cite>Articulo</cite> - revue de sciences humaines [en ligne], 3/2007. http://journals.openedition.org/articulo/589"
author: "Vivien Philizot"
date: 2007-01-01
years: "2007"
places: ""
objects: "article"
themes: ["design graphique", "typographie", "avant-gardes", "engagement politique", "auteur", "modernité", "Jan Tschichold"]
persons: ["Jan Tschichold", "Beatrice Warde"]
---

# « Les avant-gardes et leur relation avec le pouvoir dans le champ du graphisme et de la typographie », in <cite>Articulo</cite> - revue de sciences humaines [en ligne], 3/2007. http://journals.openedition.org/articulo/589

Le graphisme, à la fois outil de communication et pratique artistique, est un objet d’étude dont les rapports avec le pouvoir sont aussi complexes qu’ambigus. Si l’avant-garde historique est marquée par une volonté de démarcation par rapport à une pratique dite traditionnelle, la naissance du graphisme moderne est surtout liée à l’avènement de la communication de masse. Les questions de l’engagement politique et du rapport au pouvoir du graphiste aujourd’hui sont à replacer dans cette perspective historique. Si la distinction entre graphiste « producteur » et graphiste-auteur se mesure justement dans le rapport à la commande et au pouvoir, la question conjoncturelle du « style » et de l’imposition de formes spécifiques par une élite rejoint, quant à elle, les problématiques générales de l’art contemporain.
