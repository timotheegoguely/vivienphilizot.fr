---
title: "« Droit à la ville, droit à l’information. L’espace médiatique et sa construction »"
description: "Communication dans le cadre de la journée d’études « Prendre place », organisée par Mickaël Labbé et Sophie Suma (CRePHAC - EA2326), Faculté de Philosophie de l’Université de Strasbourg, 18 avril 2018."
author: "Vivien Philizot"
date: 2018-01-01
years: "2018"
places: "Université de Strasbourg"
objects: "communication"
themes: ["Henri Lefebvre", "Le Droit à la ville", "espace urbain", "information", "communication"]
persons: ["Mickaël Labbé", "Sophie Suma"]
---

# « Droit à la ville, droit à l’information. L’espace médiatique et sa construction », Communication dans le cadre de la journée d’études « Prendre place », organisée par Mickaël Labbé et Sophie Suma (CRePHAC - EA2326), Faculté de Philosophie de l’Université de Strasbourg, 18 avril 2018.

Dans la perspective du <cite>Droit à la ville</cite> publié par Henri Lefebvre en 1967, il s’agit de s’interroger ici sur un « droit à l’information », en construisant l’analogie principale qui rend cette idée possible : à l’espace urbain, bâti, matériel, semble correspondre un espace médiatique, symbolique, immatériel. L’espace dans lequel nous évoluons, nous nous déplaçons et nous pouvons inscrire notre corps, semble bien se doubler d’un espace symbolique, qui n’a pas moins d’effet sur nos conditions d’existence et nos représentations sociales.
