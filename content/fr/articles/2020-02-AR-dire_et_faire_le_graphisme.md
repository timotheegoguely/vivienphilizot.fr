---
title: "« Dire et faire le graphisme. Où l’on apprend que personne ne s’entend sur les mots du graphisme et que les distinctions terminologiques ont des conséquences sur la pratique »"
description: "(avec Nolwenn Maudet) in <cite>Graphisme en France</cite> 2020, Centre national des Arts plastiques, 2020, p.38-54."
author: "Nolwenn Maudet, Vivien Philizot"
date: 2020-01-01
years: "2020"
places: ""
objects: "article"
themes: ["design graphique", "texte et image", "histoire", "métiers", "disciplines"]
persons: ["Nolwenn Maudet"]
---

# « Dire et faire le graphisme. Où l’on apprend que personne ne s’entend sur les mots du graphisme et que les distinctions terminologiques ont des conséquences sur la pratique », (avec Nolwenn Maudet) in <cite>Graphisme en France</cite> 2020, Centre national des Arts plastiques, 2020, p.38-54.

« Graphisme », « Design graphique ». Ce texte explore sous forme de dialogue une distinction terminologique épineuse et largement débattue. Comment ces dénominations se sont-elles progressivement imposées en France, quelles réalités différentes recouvrent-elles dans des contextes aussi spécifiques que les écoles d’art, les institutions publiques, les entreprises ou encore dans les manières dont les graphistes eux-mêmes se présentent ? Comment ces termes ont-ils été employés par des professions qui ont elles-mêmes leur propre histoire ? Comment ont-ils été investis de significations divergentes pour désigner toute la variété des rapports à la commande que nous observons dans les pratiques ? Comment des termes dont le sens est systématiquement déplacé et redéfini permettent-ils au public de comprendre ou de se méprendre sur ce qu’ils désignent comme activité, comme champ de pratiques, comme métiers ? 

<figure>
<img src="/images/055_Revue-BAT-N107_1000x1329.jpg" alt="" width="1000" height="1329" loading="lazy">
<figcaption>Revue BAT numéro 107</figcaption>
</figure>

[Lien vers la version en ligne](https://www.cnap.fr/graphisme-en-france/ecrire-le-design-graphique)
[Télécharger l'article](/pdf/055-MAUDET-PHILIZOT-dire_et_faire_le_graphisme-2020.pdf)
