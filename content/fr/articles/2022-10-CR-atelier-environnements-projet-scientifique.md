---
title: "Atelier Environnements - Organiser un événement scientifique"
description: "Master 1 et 2, parcours Design Environnements publics, Faculté des Arts de l’Université de Strasbourg, 2022-2024"
author: "Vivien Philizot"
date: 2022-01-01
years: "2022"
places: "Université de Strasbourg"
objects: "cours"
themes: ["environnements", "projet scientifique", "recherche"]
persons: ""
---

# Atelier Environnements - Organiser un événement scientifique, Master 1 et 2, parcours Design Environnements publics, Faculté des Arts de l’Université de Strasbourg, 2022-2024

L’atelier « Organiser un événement scientifique » complète l’atelier collectif par un travail autour d’un projet scientifique réalisé sur les deux semestres en réinvestissant les différentes connaissances et compétences acquises dans les modules de l’UE (CM environnements et atelier collectif). Un travail original est mené de A à Z par les étudiants en partenariat avec une ou plusieurs institutions ainsi que des acteurs·trices de la recherche. Pour l’année 2023-2024, il s’agira d’organiser une exposition et un livret autour du projet collectif en partenariat avec l’Université des Sciences appliquées de Potsdam (Fachhochschule University of Applied Sciences - FHP).
