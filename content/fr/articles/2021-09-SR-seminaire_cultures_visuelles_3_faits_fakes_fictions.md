---
title: "Séminaire Cultures visuelles #3 : Faits, fakes, fictions. Épistémologie visuelle du vrai et du faux"
description: "Master 1 et 2, ACCRA - UR3402, Faculté des Arts, Université de Strasbourg. Co-organisation avec Sophie Suma et Sarah Calba - 9 séances entre février et avril 2021."
author: "Sarah Calba, Vivien Philizot, Sophie Suma"
date: 2021-01-01
years: "2021"
places: ""
objects: "séminaire de recherche"
themes: ["épistémologie", "fake", "science", "fiction", "vrai et faux"]
persons: ["Sophie Suma", "Sarah Calba", "Cyrille Bodin", "André Gunthert", "Manuel Cervera-Marzal", "Mikaël Chambru"]
---

# Séminaire Cultures visuelles #3 : Faits, fakes, fictions. Épistémologie visuelle du vrai et du faux, Master 1 et 2, ACCRA - UR3402, Faculté des Arts, Université de Strasbourg. Co-organisation avec Sophie Suma et Sarah Calba - 9 séances entre février et avril 2021.

Ce séminaire interdisciplinaire est porté par le groupe de recherche Cultures visuelles (ACCRA (UR 3402), AMUP (UR 7309), AHP PREST (UMR 7117) et LETHICA (Institut thématique interdisciplinaire sur les rapports entre éthique, littérature et arts). Il s’agit d’étudier la manière dont les médias visuels peuvent être rapportés aux régimes des faits, des fakes ou des fictions, en interrogeant dans leur environnement conceptuel immédiat le vrai, le faux, la transparence, l’opacité, ou encore le fantasme et la spéculation. Les objets d’études sont empruntés aux champs de l’architecture, du design, de la création numérique, télévisuelle, cinématographique, mais aussi au domaine du journalisme et de la communication par les réseaux sociaux. Comment la médiatisation (mettre en image, organiser l’information, mais aussi faire une métaphore ou communiquer visuellement de bien des manières) contribue-t-elle à renforcer ou affaiblir un discours ? Comment les concepts de fait, de fake et de fiction sont-ils communément employés dans la construction visuelle des connaissances et des opinions ? Comment le vrai et le faux, comme catégories épistémiques se traduisent-ils dans des objets visuels ? Invité·e·s : Cyrille Bodin (LISEC EA 2310), André Gunthert (EHESS), Manuel Cervera-Marzal (FNRS, Université de Liège), Mikaël Chambru (Labex ITTEM).
