---
title: "Cultures visuelles - Programme de recherche"
description: "ACCRA - UR3402, Université de Strasbourg. Cofondateur et coordinateur du groupe de recherche avec Sophie Suma, depuis 2018."
author: "Vivien Philizot, Sophie Suma"
date: 2018-01-01
years: "2018"
places: ""
objects: "projet de recherche"
themes: ["cultures visuelles", "images", "iconologie", "épistémologie", "recherche"]
persons: ["Sophie Suma", "Simon Zara", "Nolwenn Maudet", "Sarah Calba"]
---

# Cultures visuelles - Programme de recherche, ACCRA - UR3402, Université de Strasbourg. Cofondateur et coordinateur du groupe de recherche avec Sophie Suma, depuis 2018.

Le groupe de recherche Cultures visuelles a été créé en 2018 au sein du laboratoire Approches contemporaines de la création et de la réflexion artistiques (ACCRA - UR3402), et en lien avec le laboratoire Architecture, Morphologie Urbaine, Projets (AMUP - UR 7309) et de l’Institut interdisciplinaire Lethica - Littérature Étique et Arts. Le groupe inscrit ses travaux dans les perspectives interdisciplinaires ouvertes depuis quelques dizaines années à l’étranger (et un peu plus récemment en France) par les études culturelles et visuelles, qui ont permis de renouveler en profondeur les manières d’appréhender les images et leurs usages. L’image n’est pas entendue au seul sens d’une représentation, avec toute l’histoire scientifique que cette notion emporte avec elle, mais comme un processus dynamique, une relation, un rapport social embarqué ou incarné par des objets visuels.
