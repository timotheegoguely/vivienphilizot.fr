---
title: "Exposition « Eames Studio Crit. Image, science et complexité »"
description: "Co-commissariat avec Sophie Suma. Exposition présentée au Shadok, Fabrique du numérique, Strasbourg, du 30 janvier au 3 février 2019."
author: "Vivien Philizot, Sophie Suma"
date: 2019-01-01
years: "2019"
places: ["Shadok", "Strasbourg"]
objects: "exposition"
themes: ["Ray et Charles Eames", "pédagogie par l’image", "films", "médiation scientifique"]
persons: ["Sophie Suma"]
---

# Exposition « Eames Studio Crit. Image, science et complexité », Co-commissariat avec Sophie Suma. Exposition présentée au Shadok, Fabrique du numérique, Strasbourg, du 30 janvier au 3 février 2019.

Entre image et science, cette exposition présente une exploration du travail filmique des designers Ray et Charles Eames, revu par les étudiants du Master design de l’Université de Strasbourg et de la Haute école des arts du Rhin. En écho aux films des Eames, six essais visuels abordent différents thèmes par des moyens graphiques : les flux migratoires, le big data, les sexbots, les émotions, la photographie numérique, ou encore, les stéréotypes dans le cinéma américain.
