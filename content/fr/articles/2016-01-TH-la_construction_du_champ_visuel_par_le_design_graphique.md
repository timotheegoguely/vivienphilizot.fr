---
title: "« La construction du champ visuel par le design graphique. Une épistémologie du regard »"
description: "Thèse en Arts visuels sous la direction de Pierre Litzler. Soutenue à l’Université de Strasbourg, le 29 novembre 2016. Jury composé de Frédéric Lambert, Nicolas Thély, Catherine de Smet, Daniel Payot, Pierre Litzler"
author: "Vivien Philizot"
date: 2016-01-01
years: "2016"
places: ""
objects: "thèse"
themes: ["WJT Mitchell", "Modernité", "Bruno Latour", "Ludwig Wittgenstein", "Nelson Goodman", "Pierre Bourdieu"]
persons: ["Pierre Litzler", "Frédéric Lambert", "Nicolas Thély", "Catherine de Smet", "Daniel Payot"]
---

# « La construction du champ visuel par le design graphique. Une épistémologie du regard », Thèse en Arts visuels sous la direction de Pierre Litzler. Soutenue à l’Université de Strasbourg, le 29 novembre 2016. Jury composé de Frédéric Lambert, Nicolas Thély, Catherine de Smet, Daniel Payot, Pierre Litzler

Cette thèse porte sur la manière dont le design graphique entre dans la construction de notre expérience visuelle. La première partie se développe autour de différents objets de l’histoire du graphisme moderne saisis au travers de grandes notions qui ont forgé la discipline : l’opacité et la transparence, le fond et la forme, l’objectivité et la subjectivité, visible et lisible, etc. Ces notions sont replacées dans la perspective élargie d’une culture visuelle, qui emprunte ses cadres théoriques à l’anthropologie et aux études visuelles, plus spécifiquement à l’iconologie critique de W.J.T. Mitchell. Dans la seconde partie de cette thèse se trouve véritablement engagée la dimension épistémologique de ce travail, dans deux sens du terme. D’une part c’est une épistémologie des connaissances au sujet du regard, de la vision et de ses implications sociales. D’autre part c’est une épistémologie des moyens de produire de la connaissance par le regard – entendu cette fois comme point de vue, comme subjectivité nécessaire dans la construction d’un objet scientifique.
