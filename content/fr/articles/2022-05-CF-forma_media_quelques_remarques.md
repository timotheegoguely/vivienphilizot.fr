---
title: "« Forma, Media. Quelques remarques sur les modes d’existence du design graphique »"
description: "Conférence en ouverture du festival de design graphique « Format(s) », Haute École des Arts du Rhin (HEAR), 6 octobre 2022"
author: "Vivien Philizot"
date: 2022-01-01
years: "2022"
places: ["Haute École des Arts du Rhin", "Strasbourg"]
objects: "conférence"
themes: ["Format", "design graphique", "médiation"]
persons: ""
---

# « Forma, Media. Quelques remarques sur les modes d’existence du design graphique », Conférence en ouverture du festival de design graphique « Format(s) », Haute École des Arts du Rhin (HEAR), 6 octobre 2022

La notion de forme et ses dérivés occupent une place privilégiée dans l’histoire et la culture du design, et plus précisément du design graphique. Après tout, les graphistes créent des formes, ou donnent forment à des idées. Ils et elles manipulent quotidiennement des formats, de fichiers ou de papier, tout en faisant l’expérience de cette tension si souvent discutée entre « mettre en forme » et « mettre les formes ». La « forme » est l’un des lieux communs du design graphique. Pour autant elle ne permet pas toujours de comprendre la manière dont fonctionnent les objets graphiques, qui s’apparentent peut-être moins à des représentations qu’à des médiations. Il s’agira d’appréhender ce problème à partir de deux notions fondamentales : forma et media.

<figure>
<img src="/images/077_John_Berger-Voir_le_voir_4e-1972_1200x1628.jpg" alt="" width="1200" height="1628" loading="lazy">
<figcaption>Seing John Berger's <cite>Ways of Seeing</cite> (1972 - 2014 - 2022)</figcaption>
</figure>
