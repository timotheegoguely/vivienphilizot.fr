---
title: "« Principes de vision, principes de division »"
description: "Communication dans le cadre de la journée d’études « Le commanditaire, le graphiste et l’imprimeur », organisée par Jocelyn Cottencin et Yann Sérandour (PTAC - EA 3208), Université Rennes 2, Auditorium des archives départementales d’Ille-et-Vilaine, 19 octobre 2016."
author: "Vivien Philizot"
date: 2016-01-01
years: "2016"
places: ["Université Rennes 2", "Rennes"]
objects: "communication"
themes: ["design graphique", "pouvoir", "Pierre Bourdieu"]
persons: ""
---

# « Principes de vision, principes de division », Communication dans le cadre de la journée d’études « Le commanditaire, le graphiste et l’imprimeur », organisée par Jocelyn Cottencin et Yann Sérandour (PTAC - EA 3208), Université Rennes 2, Auditorium des archives départementales d’Ille-et-Vilaine, 19 octobre 2016.

Cette communication interroge les rapports entre le design graphique et le pouvoir, non pas dans sa formulation la plus visible – on pense souvent à la propagande – mais, pour reprendre une idée chère à Pierre Bourdieu, dans les formes douces de ses manifestations symboliques. Ainsi le design graphique rencontre le pouvoir au quotidien sur un plan qui n’est pas celui de l’injonction, de l’autorité ou de la coercition, mais bien plutôt celui des habitudes instaurées par les processus communs de signification qui balisent notre environnement visuel et textuel. Quelle place le design graphique occupe-t-il dans les processus de socialisation ? Comment participe-t-il de la production et de la reproduction d’un monde de sens commun ? Au-delà des formes du pouvoir et de la mise en forme du pouvoir, souvent confiée à la figure trop parfaite du designer en « <cite>problem-solver</cite> », le design graphique dans l’espace de la démocratie peut-il retrouver une fonction critique et sociale ? 
