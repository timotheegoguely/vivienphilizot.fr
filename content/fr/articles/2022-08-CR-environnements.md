---
title: "Environnements"
description: "Cours magistral. Master 1 et 2 Design, parcours Design Environnements publics, Faculté des Arts de l’Université de Strasbourg, 2022-2024"
author: "Vivien Philizot"
date: 2022-01-01
years: "2022"
places: "Université de Strasbourg"
objects: "cours"
themes: ["environnements", "design", "media studies"]
persons: ""
---

# Environnements, Cours magistral. Master 1 et 2 Design, parcours Design Environnements publics, Faculté des Arts de l’Université de Strasbourg, 2022-2024

Au croisement du design, de l’anthropologie, des études urbaines et de paysage, de la philosophie et de l’esthétique, des études visuelles et des media studies, la notion d’« environnement » représente un terrain de recherche qui s’impose aujourd’hui avec une certaine urgence. Ce cours vise à examiner conjointement l’environnement au prisme de l’écologie et des théories environnementalistes aussi bien que des media studies. Les environnements sont des médias (Peters 2015), des milieux qui présupposent de ne pas les dissocier des entités – humains et non-humains – qui en font l’expérience. Ils attirent notre attention sur les contextes, les cadres de vie, les situations, les ambiances, qui ont toutes les propriétés de ces « milieux souples, qui s’étendent jusqu’à refermer ce que nous pensions de prime abord leur être étranger » (Mitchell [2005] 2014, 214). Il s’agira d’explorer les potentiels heuristique et épistémologique des environnements et des médias, en étudiant différentes approches théoriques et critiques associées à ces notions complexes. Comment réinvestir aujourd’hui cet ensemble de réflexions à l’aune des pratiques actuelles et des enjeux posés par le monde contemporain ? Le cours est organisé en douze séances, chacune d’entre elles étant centrée sur un·e auteur·trice spécifique.
