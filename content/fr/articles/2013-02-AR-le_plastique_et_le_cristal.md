---
title: "« Le plastique et le cristal. L’identité visuelle du Stedelijk Museum Amsterdam »"
description: "in <cite>Après/Avant</cite>, Revue annuelle de culture graphique, Rencontres de Lure, 1-2013, p.59-63."
author: "Vivien Philizot"
date: 2013-01-01
years: "2013"
places: ""
objects: "article"
themes: ["design graphique", "Stedelijk Museum Amsterdam", "identité visuelle", "transgression", "Armand Mevis", "Linda van Deursen"]
persons: ""
---

# « Le plastique et le cristal. L’identité visuelle du Stedelijk Museum Amsterdam », in <cite>Après/Avant</cite>, Revue annuelle de culture graphique, Rencontres de Lure, 1/2013, p.59-63.

L’histoire de l’identité visuelle du Stedelijk Museum à Amsterdam, initiée par Sandberg, graphiste et directeur de 1945 à 1963, est certainement l’une des plus riches qu’un musée ait pu connaître. Cet article examine l’identité conçue par Armand Mevis et Linda van Deursen en 2011. L’apparente candeur enfantine de ce projet ébranle les notions d’harmonie, d’équilibre et de proportions, en cristallisant des questions inscrites au cœur de la discipline.

<figure>
<img src="/images/008_Stedelijk-Museum_1000x1500.jpg" alt="" width="1000" height="1500" loading="lazy">
<figcaption>Stedelijk Museum Amsterdam, Armand Mevis, Linda Van Deursen, 2012</figcaption>
</figure>

[Lien vers la version en ligne](https://www.delure.org/les-a-cotes/bibliotheque/revue-apres-avant/apres-avant-1)
[Télécharger l'article](/pdf/008-PHILIZOT-le_plastique_et_le_cristal-2013.pdf)
