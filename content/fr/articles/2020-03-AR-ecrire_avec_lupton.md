---
title: "« Écrire avec Lupton »"
description: "(avec Nolwenn Maudet) in <cite>Graphisme en France</cite> 2020, Centre national des Arts plastiques, 2020, p.28-31."
author: "Nolwenn Maudet, Vivien Philizot"
date: 2020-01-01
years: "2020"
places: ""
objects: "article"
themes: ["Ellen Lupton", "Michel Foucault", "design", "disciplines", "épistémologie du design", "Archéologie du savoir"]
persons: ["Nolwenn Maudet"]
---

# « Écrire avec Lupton », (avec Nolwenn Maudet) in <cite>Graphisme en France</cite> 2020, Centre national des Arts plastiques, 2020, p.28-31.

Ce texte est une introduction à notre traduction en français de « Disciplines of Design. Writing with Foucault », un texte publié par la chercheuse Ellen Lupton en 1996 dans <cite>Design Writing Research</cite>. Le dispositif imaginé par l’auteure est surprenant et audacieux : il disperse en deux colonnes des extraits de <cite>L’Archéologie du savoir</cite> de Michel Foucault, dans lesquels Lupton a pris soin de remplacer les références de l’auteur à la folie et à la médecine par le terme « design » et d’autres notions afférentes (production visuelle, journaux, imprimeurs, etc.). Le propos est d’emblée annoncé par le titre et un texte liminaire : il s’agit d’examiner la cohérence du design comme discipline, en empruntant à Foucault sa méthode « archéologique ». 

<figure>
<img src="/images/057_Maudet-Philizot-Lupton_1200x864_a.jpg" alt="" width="1200" height=« 864" loading="lazy">
<img src="/images/057_Maudet-Philizot-Lupton_1200x864_b.jpg" alt="" width="1200" height=« 864" loading="lazy">
<img src="/images/057_Maudet-Philizot-Lupton_1200x864_c.jpg" alt="" width="1200" height=« 864" loading="lazy">
<img src="/images/057_Maudet-Philizot-Lupton_1200x864_d.jpg" alt="" width="1200" height=« 864" loading="lazy">
<figcaption>Nolwenn Maudet, Vivien Philizot, « Écrire avec Lupton », <cite>Graphisme en France</cite>, 2020</figcaption>
</figure>

[Lien vers la version en ligne](https://www.cnap.fr/graphisme-en-france/ecrire-le-design-graphique)
[Télécharger l'article](/pdf/057-MAUDET-PHILIZOT-ecrire_avec_lupton-2020.pdf)
