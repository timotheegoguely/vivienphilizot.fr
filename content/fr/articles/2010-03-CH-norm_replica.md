---
title: "« Norm, Replica »"
description: "in <cite>Collectif, 48°34’52” – 7°45’33” – une cartographie illustrée de la didactique visuelle</cite>, catalogue d’exposition, Haute école des arts du Rhin, Strasbourg, 2010, p.18."
author: "Vivien Philizot"
date: 2010-01-01
years: "2010"
places: ["Haute école des arts du Rhin", "Strasbourg"]
objects: "chapitre d’ouvrage"
themes: ["Norm", "typographie", "normalisation", "dessin de lettre", "Dimitri Bruni", "Manuel Krebs"]
persons: ""
---

# « Norm, Replica », in <cite>Collectif, 48°34’52” – 7°45’33” – une cartographie illustrée de la didactique visuelle</cite>, catalogue d’exposition, Haute école des arts du Rhin, Strasbourg, 2010, p.18.

Ce texte présente le travail de Norm, studio de création graphique formé à Zurich par Dimitri Bruni et Manuel Krebs. Le caractère typographique Replica prolonge une histoire qui commence avec le Romain du Roi de Grandjean, premier alphabet, construit plutôt que dessiné, qui, aux alentours de 1700, traduisait déjà, dans une série de gravures sur cuivre, une forme d’exposition de la structure d’un signe inscrit dans une trame orthogonale.
