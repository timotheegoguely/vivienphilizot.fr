---
title: "« Qu’est-ce qu’une image dans l’espace public ?"
description: "Conférence dans le cadre de la journée « Revoir le voir: comment créer, lire et consommer l’image aujourd’hui?», organisée par Stéphanie Gygax, Anette Lenz, Rob van Leijsen, Haute école d’art et de design de Genève (HEAD), 30 janvier 2023"
author: "Vivien Philizot"
date: 2023-01-01
years: "2023"
places: ["Haute école d’art et de design de Genève", "Genève"]
objects: "conférence"
themes: ["images", "espace public", "graphisme d’utilité publique"]
persons: ["Stéphanie Gygax", "Anette Lenz", "Rob van Leijsen"]
---

# « Qu’est-ce qu’une image dans l’espace public ?, Conférence dans le cadre de la journée « Revoir le voir: comment créer, lire et consommer l’image aujourd’hui?», organisée par Stéphanie Gygax, Anette Lenz, Rob van Leijsen, Haute école d’art et de design de Genève (HEAD), 30 janvier 2023

La question principale qui oriente les réflexions développées dans cette conférence est située précisément à l’intersection de ces deux définitions classiques de l’espace public (Thierry Paquot) : au singulier, l’espace public est le lieu du débat politique, l’espace de la démocratie ; au pluriel, ce sont les espaces physiques et symboliques dans lesquels et par lesquels les publics circulent et communiquent. Elle s’accompagne de l’hypothèse qu’à cette intersection se tiennent des images, qui jouent un rôle de premier plan dans les pratiques sociales et les situations visuelles dont est fait notre quotidien.

<figure>
<img src="/images/082_They-live-carpenter_1200x1789.jpg" alt="" width="1200" height="1789" loading="lazy">
<figcaption>John Carpenter, <cite>They Live</cite>, 1988</figcaption>
</figure>
