---
title: "« Comment regarder les objets graphiques ? »"
description: "Communication dans le cadre de la journée d’études « Design graphique. Manières de faire de la recherche », Centre Georges Pompidou, Paris. Organisée par Véronique Marrier, Nolwenn Maudet, Vivien Philizot. Co-produit par l’Université de Strasbourg et le Cnap, 17 décembre 2021."
author: "Vivien Philizot"
date: 2021-01-01
years: "2021"
places: ["Centre Georges Pompidou", "Paris"]
objects: "communication"
themes: ["recherche", "médiations", "design graphique", "épistémologie du design graphique"]
persons: ""
---

# « Comment regarder les objets graphiques ? », Communication dans le cadre de la journée d’études « Design graphique. Manières de faire de la recherche », Centre Georges Pompidou, Paris. Organisée par Véronique Marrier, Nolwenn Maudet, Vivien Philizot. Co-produit par l’Université de Strasbourg et le Cnap, 17 décembre 2021.

Dans le voisinage immédiat de l’art, du design ou de l’architecture, le design graphique s’accompagne depuis plusieurs années d’un intérêt grandissant pour ce que l’on appelle communément la « recherche ». Mais les objets d’une recherche de ce genre, qu’on les appréhende comme des médias, des signes ou des mélanges hétérogènes de textes et d’images, posent d’emblée des problèmes de définition et de délimitation, qui conduisent les chercheurs et chercheuses « en » design graphique, à traverser certaines régions du vaste territoire des sciences humaines sociales. Quels montages épistémiques ces objets aussi particuliers peuvent-ils conduire à imaginer ? Comment regarder (scientifiquement) des objets que l’on a si souvent envisagés dans leur rapport au visible et à ses conditions de possibilité ?

[Lien vers la version en ligne](https://www.culturesvisuelles.org/champs-de-recherche/epistemologie-des-images-et-du-design-graphique/design-graphique-manieres-de-faire-de-la-recherche/comment-regarder-les-objets-graphiques)
