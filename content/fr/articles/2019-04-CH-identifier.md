---
title: "« Identifier »"
description: "in <cite>Le Ludographe, connaître et pratiquer le design graphique à l’école élémentaire</cite>, Collection Série graphique, Centre national des arts plastiques, Paris, 2019, p.10-15."
author: "Vivien Philizot"
date: 2019-01-01
years: "2019"
places: "Paris"
objects: "chapitre d’ouvrage"
themes: ["identité visuelle", "mots et images", "logotypes", "langages graphiques", "systèmes visuels"]
persons: ""
---

# « Identifier », in <cite>Le Ludographe, connaître et pratiquer le design graphique à l’école élémentaire</cite>, Collection Série graphique, Centre national des arts plastiques, Paris, 2019, p.10-15.

Lorsqu’il s’agit d’identifier, le design graphique procède d’un certain nombre de choix contextuels, qui mobilisent ces outils visuels que sont les mots, les images, les mot-images. Ce texte présente le design graphique dans cette perspective, en passant par plusieurs exemples de différentes échelles. Du pictogramme à l’identité visuelle d’une entreprise, c’est l’ensemble de notre univers de signes qui opère selon le régime de l’identification.

[Télécharger l'article](/pdf/049-PHILIZOT-identifier-2019.pdf)
