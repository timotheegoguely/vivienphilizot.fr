---
title: "« Design graphique et pouvoir symbolique »"
description: "Conférence dans le cadre du cycle de conférences « Design graphique : Pratique + Critique », organisé par Catherine de Smet (TEAMeD, Université Paris 8) et Philippe Millot (ENSAD), École nationale supérieure des Arts décoratifs, 12 avril 2014."
author: "Vivien Philizot"
date: 2014-01-01
years: "2014"
places: ["École nationale supérieure des Arts décoratifs", "Paris"]
objects: "conférence"
themes: ["design graphique", "publicité", "marques", "signes"]
persons: ["Catherine de Smet", "Philippe Millot"]
---

# « Design graphique et pouvoir symbolique », Conférence dans le cadre du cycle de conférences « Design graphique : Pratique + Critique », organisé par Catherine de Smet (TEAMeD, Université Paris 8) et Philippe Millot (ENSAD), École nationale supérieure des Arts décoratifs, 12 avril 2014.

Dans la manière dont il est pensé, conçu, présenté et représenté, le design graphique a toutes les chances de cristalliser des intérêts particuliers engageant des rapports de pouvoir. Un tel pouvoir s’exerce par exemple dans la façon dont la mode impose ses standards, dans le caractère performatif des signes destinés à faire autorité (comme les labels, les marques, qui sont autant d’institutions miniatures, de manifestations visuelles d’un pouvoir délégué), dans la rhétorique institutionnelle et commerciale de la vertu (on pense au discours écologique, durable, responsable), dans l’usage de la culture comme instrument de légitimation, dans l’usage publicitaire de lieux communs (on pense aux effets d’impositions produits par les stéréotypes publicitaires). Au delà de ces observations un peu rapides, il s’agit ici de revenir sur la nature même du design graphique, sa capacité à faire signe et à faire sens.
