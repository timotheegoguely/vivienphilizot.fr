---
title: "<cite>Langages visuels et systèmes complexes</cite>"
description: "Presses universitaires de Strasbourg, 2016. 248p. ISBN : 9782916058627"
author: "Ruedi Baur, Pierre Litzler, Vivien Philizot (dir.)"
date: 2016-01-01
years: "2016"
places: ""
objects: "livre"
themes: ["identités visuelles", "design graphique"]
persons: ""
---

# <cite>Langages visuels et systèmes complexes</cite>, Presses universitaires de Strasbourg, 2016. 248p. ISBN : 9782916058627

Cet ouvrage rend compte du projet de recherche-action « Identités complexes : lisibilité et intelligibilité de l’Université de Strasbourg », qui s’est développé à partir d’une réflexion sur les potentialités du design graphique à contribuer à une meilleure représentation des savoirs. Le projet a conjugué pendant trois ans une approche spéculative de recherche – mêlant état des lieux, réflexion théorique, élaboration de modèles – à une démarche d’ordre pratique, qui a consisté à inventer et concevoir un nouveau système d’identification et de représentation pour l’Université de Strasbourg.
