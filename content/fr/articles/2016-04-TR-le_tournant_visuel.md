---
title: "« Le Tournant visuel »"
description: "Traduction de l’anglais de Mitchell, William John Thomas. 1994. « The Pictorial Turn ». In <cite>Picture Theory. Essays on Verbal and Visual Representation</cite>. Chicago: The University of Chicago Press. 11-34."
author: "Vivien Philizot"
date: 2016-01-01
years: "2016"
places: ""
objects: "traduction"
themes: ["WJT Mitchell", "Iconologie", "Erwin Panofsky", "Louis Althusser"]
persons: ["Charlotte Bomy"]
---

# « Le Tournant visuel », Traduction de l’anglais de Mitchell, William John Thomas. 1994. « The Pictorial Turn ». In <cite>Picture Theory. Essays on Verbal and Visual Representation</cite>. Chicago: The University of Chicago Press. 11-34.

Publié dans le cadre de ma thèse de doctorat, « La construction du champ visuel par le design graphique. Une épistémologie du regard » (2016). Avec Charlotte Bomy.
