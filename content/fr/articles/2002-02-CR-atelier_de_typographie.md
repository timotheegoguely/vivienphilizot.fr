---
title: "Atelier de typographie"
description: "Années 3 et 4, options Didactique visuelle et Communication graphique, Haute école des arts du Rhin (Hear). Licence 2 et 3 Arts, parcours Design, Faculté des Arts de l’Université de Strasbourg, de 2002 à 2018"
author: "Vivien Philizot"
date: 2002-01-01
years: "2002"
places: "Université de Strasbourg"
objects: "cours"
themes: ["typographie", "projet"]
persons: ""
---

# Atelier de typographie, Années 3 et 4, options Didactique visuelle et Communication graphique, Haute école des arts du Rhin (Hear). Licence 2 et 3 Arts, parcours Design, Faculté des Arts de l’Université de Strasbourg, de 2002 à 2018

Ces TD portent sur la place de la typographie dans la conception éditoriale et la conception d’identités visuelles. Les exemples étudiés ressortissent aux différents domaines de la communication visuelle et permettent de prendre la mesure de l’expressivité de la lettre, de sa capacité à incarner des contenus textuels, des rapports qu’elle entretient avec l’ensemble des autres éléments graphiques qui constituent ces documents : photographies, diagrammes, schémas, etc. Le texte est ici envisagé comme un élément complexe dont le traitement graphique entre dans la production du sens.
