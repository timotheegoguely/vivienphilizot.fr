---
title: "« Traduire “design graphique”. Réflexions sur l’épistémologie d’une discipline »"
description: "in <cite>Appareil</cite>, [en ligne], 24/2022. https://doi-org.scd-rproxy.u-strasbg.fr/10.4000/appareil.4380"
author: "Vivien Philizot"
date: 2022-01-01
years: "2022"
places: ""
objects: "article"
themes: ["design graphique", "sémiotique", "théorie des médias", "études visuelles", "épistémologie", "traduction"]
persons: ""
---

# « Traduire “design graphique”. Réflexions sur l’épistémologie d’une discipline », in <cite>Appareil</cite>, [en ligne], 24/2022. https://doi-org.scd-rproxy.u-strasbg.fr/10.4000/appareil.4380

À certains égards, « design graphique » est une notion incompréhensible, recouvrant un ensemble diffus de pratiques, de situations, et d’objets hétérogènes, qui semblent dissuader toute tentative d’en circonscrire les contours sans cesse changeants. Pour saisir au mieux la forme problématique que découpe cette expression à la surface du monde, cet article se propose de suivre au plus près ses multiples traductions. Depuis quels paradigmes et sous quels autres noms, ce que nous nous entendons pour appeler « design graphique » a-t-il été construit, envisagé, théorisé, dans l’histoire récente ? À l’heure où l’on cherche partout sans les trouver les modèles scientifiques de la recherche en design et en design graphique, il est bien utile de remarquer que les objets qui entrent communément dans ces catégories sont appréhendés depuis des années par des champs du savoir voisins. En abordant trois traductions de « design graphique », cet article vise à donner un peu de netteté à cet ensemble de choses aux contours si diffus et d’autre part, à déterminer avec un peu plus de précision les perspectives scientifiques par lesquelles une recherche en design graphique est aujourd’hui envisageable.

<figure>
<img src="/images/075-Georges-Nelson_1977_1200x684.jpg" alt="" width="1200" height=« 684" loading="lazy">
<figcaption>Georges Nelson, <cite>How to See</cite>, 1977</figcaption>
</figure>

[Lien vers la version en ligne](https://doi-org.scd-rproxy.u-strasbg.fr/10.4000/appareil.4380)
[Télécharger l'article](/pdf/075-PHILIZOT-traduire_design_graphique_reflexions_sur_lepistemologie_dune_discipline-2022.pdf)
