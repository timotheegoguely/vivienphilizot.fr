---
title: "Écologie des médias graphiques"
description: "Cours magistral. Master 2 Design, parcours Design Environnements publics, Faculté des Arts de l’Université de Strasbourg, à partir de 2024"
author: "Vivien Philizot"
date: 2024-01-01
years: "2024"
places: "Université de Strasbourg"
objects: "cours"
themes: ["design graphique", "médias", "textes et images", "culture visuelle"]
persons: ""
---

# Écologie des médias graphiques, Cours magistral. Master 2 Design, parcours Design Environnements publics, Faculté des Arts de l’Université de Strasbourg, à partir de 2024

Ce CM pose les bases méthodologiques et épistémologiques d’une étude des objets graphiques, envisagés comme des images, des composés textes-images, et plus largement des médias. Il vise à construire la culture épistémologique d’étudiant·e·s en design graphique, non pas autour d’une science du design, mais d’un ensemble d’approches interdisciplinaires qui convergent vers une approche écologique des médias graphiques. Il s’agit d'apporter des clés de compréhension de différents systèmes théoriques ou paradigmes qui se sont élaborés depuis le début du XX<sup>e</sup> siècle, dans des champs disciplinaires intéressés par les images et les médias : sémiotique, histoire de l’art, théorie des médias, études visuelles, écologie et archéologie des médias, iconologie…
