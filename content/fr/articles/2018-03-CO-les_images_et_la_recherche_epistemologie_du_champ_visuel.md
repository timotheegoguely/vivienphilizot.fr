---
title: "« Les images et la recherche. Épistémologie du champ visuel »"
description: "Communication dans le cadre de la journée d’étude « Les jeudis de la recherche. Pratique, théorie, épistémologie de la recherche en Arts » organisée par les membres de l’équipe Approches contemporaines de la création et de la réflexion artistiques (ACCRA - UR 3402), Faculté des Arts de l’Université de Strasbourg, 4 mai 2018."
author: "Vivien Philizot"
date: 2018-01-01
years: "2018"
places: "Université de Strasbourg"
objects: "communication"
themes: ["images", "iconologie", "sciences", "WJT Mitchell", "études visuelles", "Aby Warburg", "Erwin Panofsky"]
persons: ""
---

# « Les images et la recherche. Épistémologie du champ visuel », Communication dans le cadre de la journée d’étude « Les jeudis de la recherche. Pratique, théorie, épistémologie de la recherche en Arts » organisée par les membres de l’équipe Approches contemporaines de la création et de la réflexion artistiques (ACCRA - UR 3402), Faculté des Arts de l’Université de Strasbourg, 4 mai 2018.

Aussi courante soit-elle dans notre environnement quotidien, l’image n’est cependant pas assurée de faire l’objet d’une définition stable, ni même de pouvoir être rapportée à un registre de savoir, un type de discours ou un champ de la connaissance particulier. Si la définition de ce qu’est une image soulève déjà de nombreuses difficultés, celle d’une science des images n’est pas moins complexe. D’Aby Warburg à Erwin Panofsky, auteur des <cite>Essais d’iconologie</cite>, publiés en anglais en 1939, puis à W.J.T. Mitchell, figure majeure des études visuelles américaines, il s’agit de comprendre ici comment la question d’une science des images est régulièrement posée mais systématiquement déplacée.
