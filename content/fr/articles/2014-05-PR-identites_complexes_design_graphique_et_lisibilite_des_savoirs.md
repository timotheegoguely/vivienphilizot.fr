---
title: "Identités complexes. Design graphique et lisibilité des savoirs"
description: "ACCRA - UR3402, Université de Strasbourg. Octobre 2014 à décembre 2017. Avec Ruedi Baur et Pierre Litzler. Financement IdEX 2014 Projet d’excellence."
author: ""
date: 2014-01-01
years: "2014"
places: ""
objects: "projet de recherche"
themes: ["identités visuelles", "design graphique"]
persons: ["Ruedi Baur", "Christina Poth", "Faustine Najman", "Laurie Chapotte", "Pierre Litzler", "Olivier Kohtz", "Irène Nanni", "Manon Cuccu", "Armelle Tanvez", "Manon Weber", "Chloé Ceschin", "Quynh Thi Nguyen"]
---

# Identités complexes. Design graphique et lisibilité des savoirs, ACCRA - UR3402, Université de Strasbourg. Octobre 2014 à décembre 2017. Avec Ruedi Baur et Pierre Litzler. Financement IdEX 2014 Projet d’excellence.

Dans le domaine des institutions publiques ou privées, la culture du logotype domine la production des signes et des systèmes de représentation, se soutenant de la logique des marques dont la fonction première est la visibilité. Si l’Université de Strasbourg hérite de cette tendance, un certain nombre de circonstances ont favorisé la possibilité d’une réflexion critique, et conduit à la mise en œuvre d’une approche alternative au sein d’un laboratoire dédié à cet effet. Pendant trois années, ce projet a conjugué une approche spéculative de recherche – mêlant état des lieux, réflexion théorique, élaboration de modèles – à une démarche d’ordre pratique, qui a consisté à inventer et concevoir un nouveau système d’identification et de représentation pour l’université. Participant·e·s : Ruedi Baur, Christina Poth, Faustine Najman, Laurie Chapotte, Pierre Litzler, Olivier Kohtz, Irène Nanni, Manon Cuccu, Armelle Tanvez Manon Weber, Chloé Ceschin, Quynh Thi Nguyen. 
