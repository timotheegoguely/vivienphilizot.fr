---
title: "« Métadiscours et typographie »"
description: "Communication dans le cadre de la journée d’études « Notes de bas de page », organisée par Germain Roesz (ACCRA - UR 3402), Maison Interuniversitaire des Sciences de l’Homme-Alsace Université, 3 et 4 avril 2008."
author: "Vivien Philizot"
date: 2008-01-01
years: "2008"
places: "Université de Strasbourg"
objects: "communication"
themes: ["typographie", "metadiscours", "mots et images"]
persons: ["Germain Roesz"]
---

# « Métadiscours et typographie », Communication dans le cadre de la journée d’études « Notes de bas de page », organisée par Germain Roesz (ACCRA - UR 3402), Maison Interuniversitaire des Sciences de l’Homme-Alsace Université, 3 et 4 avril 2008.

Un mot est toujours écrit à l’aide d’un caractère qui en est le support physique, ce à travers quoi il existe. Indispensable à la lecture physique et à l’accès à la signification du mot, il est aussi le vecteur d’une signification concurrente qu’il est impossible de négliger. Ainsi, le caractère, indispensable à la lecture, trahi le sens du mot, générant des contenus qui viennent se superposer à l‘original, il se transforme en vecteur d’un métadiscours. Au delà de la fonction informative de ce que nous lisons, il s’agit alors de prendre en compte l’expression de la mise en forme typographique, c’est à dire le commentaire, la référence, la citation, tout ce métadiscours, que l’on peut alors considérer comme une ou des notes de bas de page.
