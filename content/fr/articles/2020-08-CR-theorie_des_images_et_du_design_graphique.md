---
title: "Graphisme, images, médias"
description: "Cours magistral. Licence 3 Arts, parcours Design, Faculté des Arts de l’Université de Strasbourg, à partir de 2020"
author: "Vivien Philizot"
date: 2020-01-01
years: "2020"
places: "Université de Strasbourg"
objects: "cours"
themes: ["design graphique", "images", "médias", "agentivité", "histoire", "écriture", "communication visuelle"]
persons: ""
---

# Graphisme, images, médias, Cours magistral. Licence 3 Arts, parcours Design, Faculté des Arts de l’Université de Strasbourg, à partir de 2020

Ce cours vise à examiner les enjeux posés par le graphisme, les images et les médias dans notre environnement quotidien, à partir de deux grandes traditions théoriques : les études visuelles et les media studies. Notre environnement visuel a une histoire longue qui voit se côtoyer des livres, des cartes, des plans, des registres, des pièces de monnaie et des billets, des timbres et des diagrammes, des interfaces numériques et autres affichages digitaux… autant d’objets qui ressortissent en partie à ce que Bruno Latour appelle des « mobiles immuables », et que l’on peut qualifier au sens large d’« objets graphiques ». Le cours consiste à appréhender ces objets comme des médiations, en les rapportant aux acteurs, aux usages et aux milieux qui leur donnent du sens. Une affiche, une identité visuelle, une signalétique, une campagne de communication, un plan de métro… que font ces objets dans notre environnement quotidien ? Comment entrent-ils dans des pratiques ordinaires au sein desquelles les questions de forme, d’image ou de typographie peuvent souvent passer inaperçues ? Il s’agira d’étudier la diversité des formes visuelles au sein des pratiques commerciales, artistiques, militantes, politiques, etc. à partir des travaux d’auteurs et d’autrices qui ont ouvert des perspectives fécondes sur les images et les médias (W.J.T. Mitchell, N. Mirzoeff, F.A. Kittler, B. Siegert, S. Krämer, J.D. Peters…).
