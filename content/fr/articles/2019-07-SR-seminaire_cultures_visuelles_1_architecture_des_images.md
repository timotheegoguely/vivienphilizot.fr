---
title: "Séminaire Cultures visuelles #1 : Architecture des images. Formes visuelles de la construction et de la transmission des savoirs."
description: "Master 2, ACCRA - UR3402, Faculté des Arts, Université de Strasbourg. Co-organisation avec Sophie Suma - 9 séances entre janvier et mars 2019."
author: "Vivien Philizot, Sophie Suma"
date: 2019-01-01
years: "2019"
places: ""
objects: "séminaire de recherche"
themes: ["didactique visuelle", "pédagogie par l’image", "information et communication"]
persons: ["Sophie Suma", "Vincent Chevillon", "Sandra Chamaret", "Anne-Lyse Renon", "David Malaud", "Monique Sicard", "Catherine Allamel-Raffin", "Jean-Luc Gangloff"]
---

# Séminaire Cultures visuelles #1 : Architecture des images. Formes visuelles de la construction et de la transmission des savoirs., Master 2, ACCRA - UR3402, Faculté des Arts, Université de Strasbourg. Co-organisation avec Sophie Suma - 9 séances entre janvier et mars 2019.

Si l’image est un moyen de produire de la connaissance, quelles connaissances avons nous vraiment des images ? Ce séminaire vise à faire converger, à l’Université de Strasbourg, différentes expertises et recherches liées à la construction et à la transmission des savoirs par l’image. En s’appliquant aux images, l’architecture comme métaphore vise à faire entrevoir les multiples dimensions du champ visuel, mais aussi à réaffirmer la « construction », au sens épistémologique, dont celui-ci fait l’objet. Sont plus spécifiquement abordées les procédures visuelles et le statut épistémique des images en contexte de transmission : conférence, cours, exposé, communication, publication, enquête, exposition... toutes ces situations où la connaissance peut se donner à lire et à voir en conjuguant plusieurs médias. Il s’agit de développer une réflexion attentive aux articulations, au montage, à la spatialisation, à la scénographie, à la construction des images et la complexité visuelle, ainsi qu’à la didactique et à la pédagogie. Invité·e·s : Vincent Chevillon (HEAR Strasbourg), Sandra Chamaret (HEAR Strasbourg), Anne-Lyse Renon (LIAS, EHESS (UMR 8178)), David Malaud (LéaV Université Paris-Saclay), Monique Sicard (ITEM (UMR 8132 CNRS / ENS)), Catherine Allamel-Raffin, Jean-Luc Gangloff (AHPPReST (UMR 7117))
