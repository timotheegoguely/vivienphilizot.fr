---
title: "« Situations visuelles. Le designer en cartographe »"
description: "Communication dans le cadre de la journée d’études « Situations graphiques », organisée par Léonore Conte et Pauline Escot, Université Paris 8 Vincennes Saint-Denis (AIAC - EA 4010), Les Grands Voisins, Paris, 2 décembre 2017."
author: "Vivien Philizot"
date: 2017-01-01
years: "2017"
places: "Les Grands Voisins, Paris"
objects: "communication"
themes: ["design graphique", "cartographie", "métaphore"]
persons: ["Léonore Conte", "Pauline Escot"]
---

# « Situations visuelles. Le designer en cartographe », Communication dans le cadre de la journée d’études « Situations graphiques », organisée par Léonore Conte et Pauline Escot, Université Paris 8 Vincennes Saint-Denis (AIAC - EA 4010), Les Grands Voisins, Paris, 2 décembre 2017.

En quoi l’expression « situations visuelles » peut-elle décrire quelque chose de notre expérience contemporaine des images et des signes ? En quoi permet-elle de comprendre notre rapport au design graphique ? La figure du designer en cartographe peut bien nous apprendre quelque chose à ce sujet, encore faut-il entendre la carte comme une métaphore, moins rapportée à un espace topographique qu’à un espace symbolique.
