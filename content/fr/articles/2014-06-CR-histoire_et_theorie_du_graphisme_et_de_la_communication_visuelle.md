---
title: "Histoire et théorie du graphisme et de la communication visuelle"
description: "Cours magistral. Master 2 Design, parcours Design Projet, Faculté des Arts de l’Université de Strasbourg, 2014-2019"
author: "Vivien Philizot"
date: 2014-01-01
years: "2014"
places: "Université de Strasbourg"
objects: "cours"
themes: ["histoire", "théorie", "design graphique"]
persons: ""
---

# Histoire et théorie du graphisme et de la communication visuelle, Cours magistral. Master 2 Design, parcours Design Projet, Faculté des Arts de l’Université de Strasbourg, 2014-2019

Ce cours porte sur le design graphique et ses liens avec la culture visuelle. Il s’agit d’aborder les nombreuses manières dont le design graphique peut avoir un rôle à jouer dans notre environnement quotidien, en commençant par l’envisager comme une pratique située, contextuelle, systématiquement attachée à engager les lieux et les acteurs auxquels elle est attachée dans un processus de transformation. Nous connaissons ce processus sous de multiples noms : il s’agit, pour le design graphique, de représenter, d’identifier, d’informer, de visualiser, d’orienter, etc. Voilà autant de points d’entrée qui peuvent permettre de comprendre ce qu’est le design graphique tout en évitant de le réduire à une pratique de conception de “beaux” objets par lesquels il est trop souvent médiatisé.
