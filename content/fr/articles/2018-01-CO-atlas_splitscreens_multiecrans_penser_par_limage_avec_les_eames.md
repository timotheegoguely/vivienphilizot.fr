---
title: "« Atlas, split-screens, multi-écrans. Penser par l’image avec les Eames »"
description: "Communication dans le cadre du colloque « Objectivité et Design graphique », organisée par Catherine Allamel-Raffin et Anne-Lyse Renon (AHP-PReST UMR 7117 et LIAS, Institut Marcel Mauss - UMR 8178). Maison Interuniversitaire des Sciences de l’Homme-Alsace Université de Strasbourg, 17 et 18 décembre 2018. Avec Sophie Suma."
author: "Vivien Philizot, Sophie Suma"
date: 2018-01-01
years: "2018"
places: "Université de Strasbourg"
objects: "communication"
themes: ["Ray Eames", "Charles Eames", "split-screen", "images", "montage", "atlas"]
persons: ["Catherine Allamel-Raffin", "Anne-Lyse Renon", "Sophie Suma"]
---

# « Atlas, split-screens, multi-écrans. Penser par l’image avec les Eames », Communication dans le cadre du colloque « Objectivité et Design graphique », organisée par Catherine Allamel-Raffin et Anne-Lyse Renon (AHP-PReST UMR 7117 et LIAS, Institut Marcel Mauss - UMR 8178). Maison Interuniversitaire des Sciences de l’Homme-Alsace Université de Strasbourg, 17 et 18 décembre 2018. Avec Sophie Suma.

De la forme atlas au split-screen, une même organisation épistémique semble être à l’œuvre, qui nous invite à reconsidérer les dispositifs multi-écrans des Eames dans le contexte élargi de l’histoire des sciences et du cinéma des années 1960 et 1970. Employés par des scientifiques autant que les designers, des artistes ou des cinéastes, les systèmes visuels étudiés ici relèvent bien du montage, cette « forme visuelle du savoir », évoquée par Georges Didi-Huberman au sujet de la forme Atlas. Ces objets témoignent chacun d’une capacité à mobiliser des ensembles d’images non pas de manière illustrative, mais bien pour pour faire de l’architecture des images un véritable moyen visuel de produire de la pensée. 
