---
title: "« Les mots, les choses et les images. Apprendre à voir à une machine »"
description: "in <cite>Revue Radar</cite> [en ligne], 4/2019. https://revue-radar.fr/livraison/4/article/28/les-mots-les-choses-et-les-images-apprendre-a-voir-a-une-machine"
author: "Vivien Philizot"
date: 2019-01-01
years: "2019"
places: ""
objects: "article"
themes: ["intelligence artificielle", "vision", "réseaux de neurones", "images", "interaction humain-machine"]
persons: ""
---

# « Les mots, les choses et les images. Apprendre à voir à une machine », in <cite>Revue Radar</cite> [en ligne], 4/2019. https://revue-radar.fr/livraison/4/article/28/les-mots-les-choses-et-les-images-apprendre-a-voir-a-une-machine

Ces dernières années, les recherches sur l’intelligence artificielle ont fait des avancées spectaculaires dans le domaine de la vision. Des réseaux profonds de neurones semblent désormais capables de voir à notre place et de prendre des décisions sur le produit de leurs observations. Or les résistances que nous opposent depuis des siècles, à nous humains, les images et leur interprétation, ne semblent pas tomber pour autant. Apprendre à voir et à dessiner à une machine nous impose de redéfinir ce que le « voir », comme processus nécessairement imprégné par un savoir, peut bien vouloir dire. Irréductible à un champ du savoir spécifique, le problème de la vision des machines est fondamentalement un problème de culture visuelle. Pour tenter de comprendre ce que « voir » peut bien vouloir dire pour une machine, ce texte interroge la manière dont les réseaux profonds de neurones apprennent à lier ensemble le langage, le monde et la pensée, en examinant les détails à partir desquels la vision machinique semble s’établir.

[Lien vers la version en ligne](https://www.ouvroir.fr/radar/index.php?id=212)
[Télécharger l'article](/pdf/048-PHILIZOT-les_mots_les_choses_et_les_images_apprendre_a_voir_a_une_machine-2019.pdf)
