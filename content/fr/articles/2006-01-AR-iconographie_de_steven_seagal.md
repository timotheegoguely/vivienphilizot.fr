---
title: "« Iconographie de Steven Seagal »"
description: "in <cite>Livraison</cite> – revue d’art contemporain, 7/2006, p.84-93."
author: "Vivien Philizot"
date: 2006-01-01
years: "2006"
places: ""
objects: "article"
themes: ["sémiotique", "études visuelles", "Steven Seagal"]
persons: ""
---

# « Iconographie de Steven Seagal », in <cite>Livraison</cite> – revue d’art contemporain, 7/2006, p.84-93.

Steven Seagal, acteur américain, est ici étudié dans une perspective sémiologique volontairement déformante, qui pousse dans leur retranchements tout à la fois les efforts de représentation de soi déployés par l’acteur et la méthode sémiologique elle-même. Il s’agit de donner à voir un travail de mise en scène que peuvent avoir en commun le cinéma et cette activité particulière d’interprétation des signes visuels qui consiste bien souvent à faire parler des formes et des images, à leur attribuer un rôle, ou à les faire entrer en dialogue.
