---
title: "<cite>Images premières. Aux origines de la représentation visuelle</cite>"
description: "Mētis Presses, Genève, 2023. 275 p. ISBN : 9782940711222"
author: "Vivien Philizot"
date: 2023-01-01
years: "2023"
places: "Genève"
objects: "livre"
themes: ["images", "études visuelles", "origines", "représentation", "sciences", "épistémologie", "Ludwig Wittgenstein", "Nelson Goodman", "Ernst Gombrich", "Susan Sontag", "WJT Mitchell", "Emmanuel Alloa"]
persons: ""
---

# <cite>Images premières. Aux origines de la représentation visuelle</cite>, Mētis Presses, Genève, 2023. 275 p. ISBN : 9782940711222

À rebours de la perfection achevée des images promises par la surenchère technologique actuelle, ces images premières sont « tout juste des images », qui tirent leur pouvoir de fascination de leur capacité à nous placer aux frontières et aux origines mêmes de la représentation visuelle. L’essai aborde à ce titre ces sites originels de production des images que sont les parois souterraines du paléolithique, la Caverne de Platon, ou les murs décrépis décrits par Léonard de Vinci, avant de s’attarder sur d’autres récits de commencement, d’autres images inaugurales, comme la première photographie, les premiers monochromes, les premières images produites par des réseaux de neurones artificiels, ou certaines visualisations scientifiques, qui sont autant d’occasions de réinventer la notion même d’« image », aussi familière qu’insaisissable. Le propos entretient un dialogue nourri avec la philosophie de Ludwig Wittgenstein et de Nelson Goodman tout en interrogeant les conceptions de la représentation développées par Ernst Gombrich, Susan Sontag, ou plus récemment WJT Mitchell et Emmanuel Alloa.

<figure>
<img src="/images/081_Philizot-Images-premieres_1045x1200_a.jpg" alt="" width="1045" height="1200" loading="lazy">
<img src="/images/081_Philizot-Images-premieres_1024x621_b.jpg" alt="" width="1024" height="621" loading="lazy">
<img src="/images/081_Philizot-Images-premieres_1024x621_c.jpg" alt="" width="1024" height="621" loading="lazy">
<img src="/images/081_Philizot-Images-premieres_1024x621_d.jpg" alt="" width="1024" height="621" loading="lazy">
<figcaption>Vivien Philizot, <cite>Images premières. Aux origines de la représentation visuelle</cite></figcaption>
</figure>

[Lien vers la version en ligne](https://www.metispresses.ch/fr/images-premieres)
