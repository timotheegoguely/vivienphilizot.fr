---
title: "Épistémologie et méthodologie de la recherche"
description: "Master 1 et 2, parcours Design Projet, Faculté des Arts de l’Université de Strasbourg, depuis 2014"
author: "Vivien Philizot"
date: 2014-01-01
years: "2014"
places: "Université de Strasbourg"
objects: "cours"
themes: ["épistémologie", "méthodologie", "design"]
persons: ""
---

# Épistémologie et méthodologie de la recherche, Master 1 et 2, parcours Design Projet, Faculté des Arts de l’Université de Strasbourg, depuis 2014

Ce séminaire est un cours d’épistémologie du design, plus précisément du design graphique, qui vise à donner à comprendre les différents moyens dont dispose le chercheur pour conduire une démarche scientifique et produire de la connaissance dans le champ du design graphique et de la communication visuelle. Il porte plus précisément sur le statut épistémologique des images au sein d’une recherche. Comment les documents iconographiques peuvent-ils servir à construire une problématique ? Comment les organiser, les envisager comme des outils scientifiques de construction du savoir ?
