---
title: "Enseigner le design graphique à l’université. Qu’est-ce que la recherche fait à la pédagogie ?"
description: "Journée d’étude, Maison Interuniverstaire des Sciences de l'Homme – Alsace (Misha).
Strasbourg, 23 et 24 janvier 2024."
author: "Vivien Philizot"
date: 2024-01-01
years: "2024"
places: "Université de Strasbourg"
objects: "colloque et journée d’études"
themes: ["design graphique", "recherche", "enseignement", "pédagogie"]
persons: ["Yann Aucompte", "Ruedi Baur", "Karen Brunel", "Éloïse Cariou", "Boris du Boullay", "Catherine de Smet", "Damien Laverdunt", "Ron Fillon-Mallette", "Manon Ménard", "Éloïsa Pérez", "Christina Poth", "Magali Roumy Akue", "Silvia Sfligiotti", "Alice Twemlow", "Stéphane Vial", "Alix Gesnel", "Julia Coffre", "Marie Secher", "Louise Wambergue"]
---

# Enseigner le design graphique à l’université. Qu’est-ce que la recherche fait à la pédagogie ?, Journée d’étude, Maison Interuniverstaire des Sciences de l'Homme – Alsace (Misha).
Strasbourg, 23 et 24 janvier 2024.

Qu’est-ce qu’introduit, à l’endroit d’une formation en design graphique, l’adossement à la recherche ? Cette journée d’études vise à prolonger sur le terrain de l’enseignement les questions posées lors de l’exposition « Une cartographie de la recherche en design graphique », présentée au Signe, centre national du graphisme, en 2017 et de la journée d’études « Design graphique, manières de faire de la recherche » organisée au Centre Pompidou (CNAP, Université de Strasbourg, Bibliothèque Kandinsky) en 2021. Il s’agit d’interroger la manière dont la recherche déplace et transforme la pédagogie et l’enseignement du design graphique, en suivant trois axes principaux : Quelles compétences et quels savoirs la recherche, l’histoire et la théorie peuvent-elles apporter dans les formations en design graphique ? Quels montages théoriques et croisements disciplinaires pour une formation universitaire en design graphique ? Comment la formation en design graphique, au prisme de l’histoire et de la théorie, peut-elle favoriser l’exercice d’un regard critique sur le monde visuel ?

<figure>
<img src="/images/085-JE-Enseigner-le-design-graphique_1200x1697.jpg" alt="" width="1200" height="1697" loading="lazy">
<figcaption>Journée d’études « Enseigner le design graphique à l’université. Qu’est-ce que la recherche fait à la pédagogie ? », 2024</figcaption>
</figure>

[Lien vers la version en ligne](https://www.culturesvisuelles.org/champs-de-recherche/epistemologie-des-images-et-du-design-graphique/enseigner-le-design-graphique-a-l-universite)
[Télécharger le programme](/pdf/085-PHILIZOT-enseigner_le_design_graphique_a_luniversite-2024.pdf)
