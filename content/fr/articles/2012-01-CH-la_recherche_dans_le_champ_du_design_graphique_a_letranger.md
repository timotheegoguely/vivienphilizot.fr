---
title: "« La recherche dans le champ du design graphique à l’étranger »"
description: "in Jean-Pierre Durand, Joyce Sebag, <cite>Métiers du graphisme</cite>, Ministère de la culture et de la communication, Paris, 2012, p.197-198."
author: "Vivien Philizot"
date: 2012-01-01
years: "2012"
places: "Paris"
objects: "chapitre d’ouvrage"
themes: ["recherche en design graphique", "théorie", "États-Unis"]
persons: ["Jean-Pierre Durand", "Joyce Sebag"]
---

# « La recherche dans le champ du design graphique à l’étranger », in Jean-Pierre Durand, Joyce Sebag, <cite>Métiers du graphisme</cite>, Ministère de la culture et de la communication, Paris, 2012, p.197-198.

Ce texte court offre un point de vue sur la structure et l’organisation du champ de la recherche en design graphique à l’étranger, en évoquant l’histoire de la constitution d’approches théoriques qui se sont élaborées à partir des années 1970 aux États-Unis.
