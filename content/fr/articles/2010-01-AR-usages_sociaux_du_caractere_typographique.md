---
title: "« Usages sociaux du caractère typographique »"
description: "in <cite>Livraison</cite> – revue d’art contemporain, 13/2010, p.102-113."
author: "Vivien Philizot"
date: 2010-01-01
years: "2010"
places: ""
objects: "article"
themes: ["typographie", "usages", "lecture", "contexte social", "affiches de théâtre"]
persons: ""
---

# « Usages sociaux du caractère typographique », in <cite>Livraison</cite> – revue d’art contemporain, 13/2010, p.102-113.

Dans quelle mesure la situation et le contexte de lecture ont-il une influence sur ce qui est lu ? Cette réflexion interroge la forme visuelle des caractères typographique en la rapportant aux différents sens qu’elle peut avoir pour le lecteur. Les usages sociaux du caractère sont étroitement à ce que la tradition sémiologique avait appelé les « connotations » de la lettre, et qu’il s’agit ici de repenser dans une perspective pragmatique, libérée de toute forme d’essentialisme.

[Télécharger l'article](/pdf/086-PHILIZOT-usages_sociaux_du_caractere_typographique-2010.pdf)
