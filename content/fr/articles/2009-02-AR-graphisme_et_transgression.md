---
title: "« Graphisme et transgression : Citation et détournement dans les codes visuels du design graphique contemporain »"
description: "in <cite>Signes, Discours et Sociétés</cite>, sciences humaines et sociales, analyse des discours [en ligne], 2/2009. http://revue-signes.gsu.edu.tr/article/-LXz849CXEDoVZzqRdaE republié dans Martine Moureu, <cite>Graphisme aujourd’hui</cite>, École supérieure d’art des Pyrénées - Pau Tarbes, 2012, p.24-68."
author: "Vivien Philizot"
date: 2009-01-01
years: "2009"
places: ""
objects: "article"
themes: ["design graphique", "transgression", "détournement", "postmodernisme", "pratiques amateur", "vernaculaire"]
persons: ""
---

# « Graphisme et transgression : Citation et détournement dans les codes visuels du design graphique contemporain », in <cite>Signes, Discours et Sociétés</cite>, sciences humaines et sociales, analyse des discours [en ligne], 2/2009. http://revue-signes.gsu.edu.tr/article/-LXz849CXEDoVZzqRdaE republié dans Martine Moureu, <cite>Graphisme aujourd’hui</cite>, École supérieure d’art des Pyrénées - Pau Tarbes, 2012, p.24-68.

Un certain nombre de productions graphiques contemporaines font aujourd’hui l’objet de déplacements, de transgressions, de citations et de détournements motivés par un besoin permanent de re-questionner et de redéfinir les frontières qui circonscrivent la discipline. Les effets de non-maîtrise, de parodie et de citation, loin de représenter de simples jeux graphiques, ou de traduire un « style postmoderne » souvent décrit de manière caricaturale, traduisent les rapports ambigus qu’entretient le graphisme d’auteur avec la pratique amateur et la culture populaire. Il s’agit ici de s’interroger sur ces rapports, et sur les signes et les productions graphiques qui en traduisent la richesse et la complexité, fruit d’une tension entre auteur, œuvre et public.
