# Vivien Philizot website

## Setup

### Install Hugo

This website is build with [Hugo](https://gohugo.io), one of the most popular open-source static site generators. 
To install it on macOS, the simplest way is to use the Homebrew package manager. 
Open your terminal and type the following command:

```shell
brew install hugo
```

To check if Hugo is installed, type `which hugo`
It should retrun something like `/usr/local/bin/hugo`

To check which version og Hugo is install, type `hugo version`
It should return something like `hugo v0.143.1+extended+withdeploy darwin/amd64 BuildDate=2025-02-04T08:57:38Z VendorInfo=brew`

### Local Development

#### Development build, server, and watch

```shell
hugo server -e development --buildDrafts --buildFuture --disableFastRender
```

#### Development build

```shell
hugo -e development --gc --cleanDestinationDir --buildDrafts --buildFuture
```

#### Production build

```shell
hugo -e production --gc --cleanDestinationDir
```

### Deploy

### SFTP

To deploy your website via SFTP: 

1. run a production build
2. open your FTP client
3. connect to `bagage.deuxfleurs.fr:2222` using SFTP, your username and your password
4. copy all the files from your local `public` folder to the remote `vivienphilizot` root folder

### CLI

Créez un fichier nommé `.deployment.secrets` (ne commitez pas ce fichier dans votre dépôt !) :

```
export AWS_ACCESS_KEY_ID=GKa522f24269fe64778252aee7
export AWS_SECRET_ACCESS_KEY=[Clé secrète]
```

Pour déployer, sourcez le fichier de configuration et laissez Hugo faire :

```
source .deployment.secrets
hugo deploy --maxDeletes 1000
```

## Styles

### Colors

### Variables

The following color variables are declared at the begining of `assets/css/styles.css` line 8:

```css
--color-white: #FFF;
--color-black: #000;
--color-gray: #767676; /* 4.5:1 */
--color-blue: #007AB8; /* 4.5:1 */
--color-pink: #B946B9; /* 4.5:1 */
--color-primary: var(--color-black);
--color-secondary: var(--color-gray);
--color-link: var(--color-blue);
--color-selection: var(--color-black);
--color-footnotes: var(--color-blue);
```

#### Object tags

To modify or add a object tag color, open `assets/css/styles.css` and go to line 422.
Each color is linked to 3 selectors, and the `object-tag` need to be urlized (eg. Chapitre d’ouvrage → `chapitre-douvrage`).

Example:

```css
.articles .object-tag h2 > a { 
  color: #A754BA
}
```

To be accessible, each color must have a **contrast ratio** greater than or equal to `3:1`, ideally `4.5:1`.
You can use this tool to check your contrasts: https://coolors.co/contrast-checker/fcf8cd-ffffff

### Images

You can use the following classes on `<figure>` to create different image layouts:

* `max-h-screen`: prevents image from being higher than the viewport height
* `align-left`: aligns image to text (left)
* `align-center`: center image 
* `align-right`: aligns image to text (right)

Example:

```html
<figure class="max-h-screen align-center">
  <img src="/images/your-image.jpg" alt="" width="1200" height="1000" loading="lazy">
  <figcaption>Image caption</figcaption>
</figure>
```
